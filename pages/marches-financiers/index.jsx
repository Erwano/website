import React from 'react'
import BestManager from '../../components/Marches-Financier/BestManager'
import Hero from '../../components/Marches-Financier/Hero'
import Processus from '../../components/Marches-Financier/Processus'
import StayInControl from '../../components/Marches-Financier/StayInControl'
import Wallet from '../../components/Marches-Financier/Wallet'

const index = () => {
    return (
        <section>
            <Hero />
            <Wallet />
            <Processus />
            <BestManager />
            <StayInControl />
        </section>
    )
}

export default index