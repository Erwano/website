import axios from "axios";


const url =
    process.env.NODE_ENV === "production"
        ? process.env.NEXT_PUBLIC_PRODUCTION_ENV_VARIABLE_API
        : process.env.NEXT_PUBLIC_DEVELOPMENT_ENV_VARIABLE_API;

export const createUser = async (body) => {
    const newUser = await axios
        .post(url + "/users", body)
        .then(function (result) {
            return result.data;
        })
        .catch(function (error) {
            const status = error.toJSON().status;
            if (status && status === 409) {
                let errors = {
                    status: status,
                    data: error.response.data.message,
                };
                return errors;
            }
        });
    return newUser;
};

export const addField = async (id, fields) => {
    const newFields = await axios.post(
        url + "/fields/" + id,
        fields
    );
    return newFields.status;
};

export const updatedUser = async (id, body) => {
    const user = await axios.patch(
        url + "/users/" + id,
        body
    );
    return user;
};

export const generateO2s = async (id) => {
    const pdf = await axios.get(
        url + "/users/generatePdf/" + id
    );
    return pdf;
};
