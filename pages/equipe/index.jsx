import Board from "../../components/Qui-sommes-nous/Board"
import Founders from "../../components/Qui-sommes-nous/Founders"
import Hero from "../../components/Qui-sommes-nous/Hero"
import Hiring from "../../components/Qui-sommes-nous/Hiring"
import Talent from "../../components/Qui-sommes-nous/Talent"
import Values from "../../components/Qui-sommes-nous/Values"

const index = () => {
    return (
        <section className="min-h-[calc(100vh-95px) bg-[#090909]">
            <Hero/>
            <Founders/>
            <Board/>
            <Talent/>
            <Values/>
            <Hiring/>
        </section>
    )
}

export default index