import React from 'react';
import { withTranslation } from 'react-i18next';
import { BriefcaseIcon, LocationMarkerIcon } from '@heroicons/react/outline';
import ReadMore from 'components/shared/ReadMore';
import Spinner from 'components/shared/Spinner';

class Careers extends React.Component { 
    constructor(props) { 
        super(props); 
        this.state = {
            jobs: [],
        }; 
    } 

    componentDidMount() {
        fetch('https://sheetdb.io/api/v1/r5oeql7plu3ws', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => response.json()).then((response) => {
            this.setState({
                jobs: response
            });
        })
    }

    render() { 
        return (
        <section className='page-section first-section flex flex-col items-center'>
            <h1 className="main-title title-gradient">
                <span className="font-light">{this.props.t('careers.title1')}</span>
                <br/>
                {this.props.t('careers.title2')}
            </h1>
            <div className="mt-6 mb-10 md:mb-20 text-center text-neutral-400 font-extralight text-sm md:text-2xl w-full">
                {this.props.t('careers.subtitle')}
            </div>
            { !this.state.jobs.length ? 
                <Spinner className="w-16 h-16 mt-10"/>
            : this.state.jobs.map((job, idx) => (
                <div key={idx} className="w-full rounded-2xl md:w-10/12 md:rounded-standard border border-gray-700 backgroundCardService text-white mb-12 p-14 flex flex-col">
                    <div className="text-xl md:text-3xl font-medium mb-2">{job.title}</div>
                    <div className="text-base md:text-lg text-gray-400 flex mb-2">
                        <div className="flex items-center">
                            <BriefcaseIcon className="h-5 w-5 mr-2"/>
                            {job.type}
                        </div>
                        <div className="flex items-center ml-4">
                            <LocationMarkerIcon className="h-5 w-5 mr-2"/>
                            Paris
                        </div>
                    </div>
                    <div className='text-base md:text-lg font-light text-gray-400 mb-8 whitespace-pre-line'>
                        <ReadMore readMoreText={this.props.t('careers.knowMore')} readLessText={this.props.t('careers.knowLess')}>
                            {job.mission}
                        </ReadMore>
                    </div>
                    <form action='mailto:contact@colbr.co'>
                        <button className='blue-button'>{this.props.t('careers.apply')}</button>
                    </form>
                </div>
            ))}
        </section>
    )}
};

export default withTranslation()(Careers);
