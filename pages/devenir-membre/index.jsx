import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Hero from "../../components/devenir-membre/Hero";
import FinalStep from "../../components/devenir-membre/FinalStep";
import FormStep1 from "../../components/devenir-membre/FormStep1/FormStep1";
import FormStep2 from "../../components/devenir-membre/FormStep2/FormStep2";
import FormStep4 from "../../components/devenir-membre/FormStep4/FormStep4";
import ScrollTop from "../../components/FormFields/ScrollTop";
import Image from "next/image";
import SpinnerSvg from "assets/spinner.svg";

const Index = () => {
    const [status, setStatus] = useState(0);
    const router = useRouter();
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        let statusUser = localStorage.getItem("status");
        if (statusUser) {
            setStatus(parseInt(statusUser));
        }
    }, []);

    const handleChangeRoute = (url) => {
        router.push(url);
    };

    const props = { handleChangeRoute, setLoader };

    const handleStatus = () => {
        switch (status) {
            case 0:
                return <Hero setStatus={setStatus} ScrollTop={ScrollTop} />;
            case 1:
                return (
                    <FormStep1 setStatus={setStatus} ScrollTop={ScrollTop} />
                );
            case 2:
                return (
                    <FormStep2 setStatus={setStatus} ScrollTop={ScrollTop} />
                );
            case 3:
                return !loader ? (
                    <FormStep4
                        setStatus={setStatus}
                        ScrollTop={ScrollTop}
                        setLoader={setLoader}
                    />
                ) : (
                    <div className="max-h-[112px] max-w-[112px]">
                        <Image alt="" src={SpinnerSvg} />
                    </div>
                );
            case 4:
                return <FinalStep {...props} />;
            default:
                break;
        }
    };

    const handletitle = () => {
        switch (status) {
            case 1:
                return (
                    <h1 className="main-title my-5 text-white">
                        Vos informations personnelles
                    </h1>
                );
            case 2:
                return (
                    <h1 className="main-title my-5 text-white">
                        Revenus & charges
                    </h1>
                );
            case 3:
                return (
                    !loader && (
                        <h1 className=" main-title my-5 text-white">
                            Patrimoine
                        </h1>
                    )
                );
            default:
                break;
        }
    };
    return (
        <section className="page-section hero flex w-full flex-col items-center justify-center overflow-hidden">
            {handletitle()}
            {handleStatus()}
        </section>
    );
};

export default Index;
