import { useEffect, useState, useRef } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { ParallaxProvider } from 'react-scroll-parallax';
import Footer from 'components/Footer/Footer';
import Nav from 'components/Navbar';
import 'keen-slider/keen-slider.min.css';
import 'aos/dist/aos.css';
import 'react-popper-tooltip/dist/styles.css';
import 'styles/globals.scss';
import 'language/i18n';

function MyApp({ Component, pageProps }) {
	const [scrollEl, setScrollElement] = useState(null);
	const ref = useRef();
	useEffect(() => {
		setScrollElement(ref.current);
	}, [])

	const router = useRouter()
	const handleChangeRoute = (url) => {
		router.push(url);
	}

	const props = { handleChangeRoute }

	return (
		<>
			<Head>
				<title>Colbr - La néobanque privée à la hauteur de votre ambition</title>
				<meta name="description" content="Un conseil sur-mesure vous permettant d'accéder à des solutions d'investissement à haute valeur ajoutée, plus transparentes, plus engagées" />
				<meta property="og:title" content="Colbr - La néobanque privée" />
				<meta property="og:url" content="https://colbr.co/" />
				<meta property="og:description" content="Un conseil sur-mesure vous permettant d'accéder à des solutions d'investissement à haute valeur ajoutée, plus transparentes, plus engagées" />
				<meta property="og:type" content="website" />
				<meta property="og:locale" content="fr_FR" />
				<link rel="icon" href="/favicon.ico" />
				<link rel="preload" href="/fonts/Geomanist-Thin.ttf" as="font" crossOrigin="" />
				<link rel="preload" href="/fonts/Geomanist-ExtraLight.ttf" as="font" crossOrigin="" />
				<link rel="preload" href="/fonts/Geomanist-Light.ttf" as="font" crossOrigin="" />
				<link rel="preload" href="/fonts/Geomanist-Regular.ttf" as="font" crossOrigin="" />
				<link rel="preload" href="/fonts/Geomanist-Medium.ttf" as="font" crossOrigin="" />
				<link rel="preload" href="/fonts/Geomanist-Bold.ttf" as="font" crossOrigin="" />
				<link rel="preload" href="/fonts/Geomanist-Black.ttf" as="font" crossOrigin="" />
			</Head>
			<Nav />
			<main className="bg-[#090909]">
				<ParallaxProvider >
					<Component {...pageProps} />
				</ParallaxProvider>
			</main>
			<Footer {...props} />
		</>
	)
}

export default MyApp
