import Distribution from "../../components/Private-Equity/Distribution";
import Funds from "../../components/Private-Equity/Funds";
import Graph from "../../components/Private-Equity/Graph";
import Hero from "../../components/Private-Equity/Hero";
import Insiders from "../../components/Private-Equity/Insiders";
import Processus from "../../components/Private-Equity/Processus";

const index = () => {
    return (
        <main className="min-h-[calc(100vh-95px) bg-[#090909]">
            <Hero />
            <Graph />
            <Funds />
            <Distribution />
            <Processus />
            <Insiders />
        </main>
    );
};
export default index;
