import Hero from "../../components/Immobilier/Hero";
import Selection from "../../components/Immobilier/Selection";
import InvestWithExperts from "../../components/Immobilier/InvestWithExperts";
import Partnership from "../../components/Immobilier/Partnership";
import Opportunities from "../../components/Immobilier/Opportunities";


const index = () => {
    return ( 
        <main className="min-h-[calc(100vh-95px) bg-[#090909]">
            <Hero />
            <Selection />
            <InvestWithExperts />
            <Partnership/>
            <Opportunities/>
        </main>
    );
};
export default index;
