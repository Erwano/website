import React from "react";
import LoginCard from "../components/Auth/LoginCard";

const Login = () => {
    return (
        <section className="flex min-h-[calc(100vh-80px)] w-full items-center justify-center bg-[#090909]">
            <LoginCard />
        </section>
    );
};

export default Login;
