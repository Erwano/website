import Details from "../../../components/Offres/Colbr-Black/Details";
import Hero from "../../../components/Offres/Colbr-Black/Hero";
import Start from "../../../components/Offres/Start";
import Romain from "../../../assets/qui-sommes-nous/rdvRomain.jpeg";

const index = () => {
    return (
        <main className="min-h-[calc(100vh-95px)] bg-[#090909]">
            <Hero />
            <Details />
            <Start img={Romain} title1="Commencez dès maintenant" title2="à échanger avec un expert" />
        </main>
    );
};
export default index;
