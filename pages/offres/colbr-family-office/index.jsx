import Details from "../../../components/Offres/Colbr-Family/Details";
import Hero from "../../../components/Offres/Colbr-Family/Hero";
import Start from "../../../components/Offres/Start";
import Gustav from "assets/Gustav.jpeg";
import Romain from "assets/qui-sommes-nous/rdvRomain.jpeg";

const index = () => {
    return (
        <main className="min-h-[calc(100vh-95px)] bg-[#090909]">
            <Hero />
            <Details />
            <Start img={Gustav} img2={Romain} title1="Commencez dès maintenant" title2="à échanger avec nos experts" />
        </main>
    );
};
export default index;
