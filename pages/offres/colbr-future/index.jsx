import Details from "../../../components/Offres/Colbr-Future/Details";
import Hero from "../../../components/Offres/Colbr-Future/Hero";
import Register from "../../../components/Offres/Colbr-Future/Register";

const index = () => {
    return (
        <main className="min-h-[calc(100vh-95px)] bg-[#090909]">
            <Hero />
            <Details />
            <Register />
        </main>
    );
};
export default index;
