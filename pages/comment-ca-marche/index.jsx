import Start from "components/Comment-ca-marche/Start";
import StepDesktopScreen from "components/Comment-ca-marche/StepDesktopScreen";
import StepMobileScreen from "components/Comment-ca-marche/StepMobileScreen";
import Hero from "../../components/Comment-ca-marche/Hero";
import StayInCommand from "../../components/Comment-ca-marche/StayInCommand";

const index = () => {
    return (
        <main className="min-h-[calc(100vh-95px)] bg-[#090909]">
            <Hero />
            <div className="page-section hero mb-10 flex flex-col items-center justify-center">
                <h1 className="main-title mb-10">
                    Un accompagnement humain <br />{" "}
                    <span className="font-light">
                        augmenté par la technologie
                    </span>{" "}
                </h1>
                <StepDesktopScreen />
                <StepMobileScreen />
            </div>
            <StayInCommand />
            <Start />
        </main>
    );
};
export default index;
