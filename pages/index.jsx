import FirstSection from "../components/Home/FirstSection";
import SecondSection from "../components/Home/SecondSection";
import ThirdSection from "../components/Home/ThirdSection";
import FourthSection from "../components/Home/FourthSection";
import FifthSection from "../components/Home/FifthSection";
import SixthSection from "../components/Home/SixthSection";
import SeventhSection from "../components/Home/SeventhSection/SeventhSection";
import EighthSection from "../components/Home/EighthSection";
import NinethSection from "../components/Home/NinthSection";
import LastSection from "../components/Home/LastSection";
import { useRouter } from "next/router";

export default function Home() {
    const router = useRouter();

    const handleChangeRoute = (url) => {
        router.push(url);
    };

    const props = { handleChangeRoute };

    return (
        <>
            <FirstSection />
            <SecondSection />
            <ThirdSection />
            <SeventhSection />
            <FourthSection />
            <FifthSection />
            <SixthSection />
            <EighthSection />
            <NinethSection />
            <LastSection {...props} />
        </>
    );
}
