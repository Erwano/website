import Conseil from "../../components/Advisory/Conseil";
import Expertise from "../../components/Advisory/Expertise";
import Hero from "../../components/Advisory/Hero";
import Partner from "../../components/Advisory/Partner";

const index = () => {
    return (
        <main className="min-h-[calc(100vh-95px)] bg-[#090909]">
            <Hero/>
            <Conseil />
            <Expertise />
            <Partner/>
        </main>
    );
};
export default index;
