import { useEffect, useState } from "react";
import CalendlyComponent from "components/prendre-rendez-vous/Calendly";
import { useRouter } from "next/router";
import CheckAnimate from "components/shared/CheckAnimate/CheckAnimate";
import Presentation from "components/prendre-rendez-vous/Presentation";
import BlueButton from "components/BlueButton";
import ScrollTop from 'components/FormFields/ScrollTop'

const Calendly = () => {
    const [success, setSucces] = useState(false);
    const router = useRouter();

    useEffect(() => {
        if (success) {
            const interval = setInterval(() => {
                router.push("/");
            }, 3000);
            return () => clearInterval(interval);
        }
    }, [success]);

    return (
        <section className="mb-10 flex min-h-[calc(100vh-76px)] flex-col items-center justify-center lg:min-h-[calc(100vh-96px)]">
            <h1
                className={`main-title mt-10 ${
                    success && "hidden"
                }`}
            >
                Rencontrez nos experts
            </h1>
            <p className={`w-3/5 sm:4/5 md:w-full my-3 subtitle-text text-center ${success && "hidden"}`}>
                Faisons connaissance
            </p>
            {success ? (
                <div className="col-span-12 flex flex-col items-center justify-center">
                    <h1 className="main-title mb-10">
                        Merci d’avoir pris un rendez-vous
                    </h1>
                    <CheckAnimate />
                </div>
            ) : (
                <div className="flex max-h-full w-full justify-center">
                    <CalendlyComponent setSucces={setSucces} ScrollTop={ScrollTop}/>
                </div>
            )}
        </section>
    );
};
export default Calendly;
