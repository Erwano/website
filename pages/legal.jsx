import React from "react";

const LegalInformation = () => {
    return (
        <section className="page-section flex justify-center bg-[#090909] py-10 font-sans font-normal text-gray-400">
            <div className="mx-5 max-w-7xl space-y-6">
                <h1 className="text-5xl font-medium text-white">
                    Informations légales
                </h1>
                <div className="space-y-4">
                    <h6 className="text-2xl text-white">Colbr</h6>
                    <p>
                        Société inscrite sur le registre unique des
                        intermédiaires en assurance, banque et finance de
                        l’Orias (N° 210 01069).
                    </p>
                    <ul className="ml-10 list-disc space-y-2">
                        <li>Conseiller en investissements financiers (CIF)</li>
                        <li>Courtier d’assurance (COA)</li>
                        <li>
                            Préconisation de solutions relatives à l’assurance
                            de personnes
                        </li>
                        <li>
                            Mandataire bancaire et en services de paiements
                            (MOBSP) catégorie « mandataire non exclusif »
                        </li>
                        <li>
                            Agent immobilier Carte T professionnelle
                            (enregistrement en cours)
                        </li>
                    </ul>
                    <p>
                        Colbr est membre de la Chambre Nationale des Conseils en
                        Gestion de Patrimoine.
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">Garantie financière</h6>
                    <p>
                        Souscrite auprès de MMA IARD Assurances Mutuelles/MMA
                        IARD, 14 boulevard Marie et Alexandre Oyon, 72030 LE
                        MANS CEDEX 9
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">Notre rémunération</h6>
                    <ul className="ml-10 list-disc space-y-2">
                        <li>
                            Certaines prestations font l’objet d’une facturation
                            d’honoraires, une lettre de mission précise les
                            modalités de réalisation ainsi que le montant perçu.
                        </li>
                        <li>
                            Colbr perçoit également des commissions et/ou des
                            rétrocessions de commissions versées par les
                            établissements promoteurs de produits (frais
                            uniques, frais récurrents, coûts relatifs aux
                            transactions, coûts marginaux et frais associés aux
                            services auxiliaires). Colbr peut fournir toute
                            précision complémentaire sur la nature et le montant
                            de ses rémunérations sur simple demande.
                        </li>
                        <li>
                            Dans le cadre de la mutualisation des services
                            fournis à l’ensemble de nos clients, Colbr facture
                            des honoraires forfaitaires.
                        </li>
                    </ul>
                    <p>
                        Colbr est régit, en matière de tarification, par
                        l’Article 325-6 du règlement général de l’AMF. Colbr
                        fournit des conseils non-indépendants au sens de la
                        directive MIF 2. Vous trouverez de plus amples
                        informations sur notre Document d’Entrée en Relation.
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">Hébergement</h6>
                    <p>
                        Google Ireland Limited, société de droit irlandais
                        immatriculée en Irlande (sous le numéro : 368047),
                        Gordon House, Barrow Street; Dublin 4, Irlande.
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">
                        Propriété intellectuelle et Copyright
                    </h6>
                    <p>
                        Le site{" "}
                        <a
                            className="text-blue-500"
                            href="www.colbr.co"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            www.colbr.co
                        </a>{" "}
                        est protégé par les lois françaises et internationales
                        en vigueur sur la propriété intellectuelle et le droit
                        d’auteur. Les informations disponibles sur le site sont
                        protégées et sont la propriété de Colbr, sous réserve de
                        droits appartenant à des tiers. Toute reproduction,
                        représentation, diffusion ou rediffusion, totale ou
                        partielle, du contenu de ce site par quelque procédé que
                        ce soit sans l’autorisation expresse et préalable de
                        Colbr est interdite.
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">
                        Objectif et nature de l’information du site
                    </h6>
                    <p>
                        Le site{" "}
                        <a
                            className="text-blue-500"
                            href="www.colbr.co"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            www.colbr.co
                        </a>{" "}
                        est un site à but exclusivement informatif dont
                        l’objectif est de présenter l’activité de Colbr ainsi
                        que les principales caractéristiques de ses services.
                        Toutes les informations disponibles sur le site ont un
                        caractère uniquement indicatif.
                    </p>
                    <p>
                        Toute information peut être modifiée sans préavis par
                        Colbr.
                    </p>
                    <p>
                        Les informations publiées sur le site ne sont
                        constitutives :
                    </p>
                    <ul className="ml-10 list-disc space-y-2">
                        <li>
                            ni d’une offre de produits ou de services pouvant
                            être assimilée à un appel public à l’épargne ou à
                            une quelconque activité de démarchage ou de
                            sollicitation à l’achat ou à la vente de valeurs
                            mobilières ou de tout autre produit de gestion ou
                            d’investissement.
                        </li>
                        <li>
                            ni d’une incitation ou un conseil en vue d’un
                            quelconque investissement ou arbitrage de valeurs
                            mobilières ou tout autre produit de gestion ou
                            d’investissement.
                        </li>
                    </ul>
                    <p>
                        Colbr décline toute responsabilité dans l’utilisation
                        qui pourrait être faite de cette information et des
                        conséquences qui pourraient en découler, notamment au
                        niveau des décisions qui pourraient être prises ou des
                        actions qui pourraient être entreprises à partir de
                        cette information.
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">
                        Protection des données personnelles
                    </h6>
                    <p>
                        Colbr, dans le cadre de ses activités, est amenée à
                        collecter, utiliser ou transmettre à des tiers des
                        données personnelles concernant ses clients.
                    </p>
                    <p>
                        L’utilisation, le traitement et le transfert de ces
                        données par Colbr est obligatoire en raison de
                        dispositions légales et règlementaires, notamment dans
                        le cadre de l’exécution des contrats qui vous lient à
                        Colbr.
                    </p>
                    <p>
                        Par ailleurs, Colbr peut détenir des données
                        personnelles vous concernant parce que vous avez
                        souhaité recevoir des informations au sujet de la
                        société ou de ses prestations.
                    </p>
                    <p>
                        Colbr veille à ce que l’accès à ces données soit
                        sécurisé et réservé aux personnes habilitées ; que
                        l’utilisation de ces données soit strictement cantonnée
                        à la réalisation des prestations qu’elle fournit et des
                        obligations réglementaires auxquelles elle est tenue ;
                        que ces données puissent être mises à jour et conservées
                        conformément à la réglementation.
                    </p>
                    <p>La durée de conservation des données est de 5 ans.</p>
                    <p>
                        Vous bénéficiez d’un droit d’accès, de rectification, de
                        portabilité, d’effacement de celles-ci ou une limitation
                        du traitement.
                    </p>
                    <p>
                        Vous pouvez vous opposer au traitement des données vous
                        concernant et disposez du droit de retirer votre
                        consentement à tout moment en vous adressant à :
                        contact@colbr.co
                    </p>
                    <p>
                        Vous avez la possibilité d’introduire une réclamation
                        auprès d’une autorité de contrôle.
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">Cookies</h6>
                    <p>
                        L’utilisateur est informé que lors de ses visites sur le
                        site, un cookie peut s’installer automatiquement sur son
                        logiciel de navigation et être conservé temporairement
                        en mémoire ou sur son disque dur. Un cookie est un
                        élément qui ne permet pas d’identifier l’utilisateur
                        mais sert à enregistrer des informations relatives à sa
                        navigation sur le site internet. Les utilisateurs du
                        site reconnaissent avoir été informés de cette pratique
                        et autorisent Colbr à l’employer. Ils pourront
                        désactiver ce cookie par l’intermédiaire des paramètres
                        figurant au sein de leur logiciel de navigation.
                    </p>
                    <p>
                        Les cookies sont de petits fichiers implantés sur votre
                        ordinateur. Un cookie ne nous permet pas de vous
                        identifier mais il enregistre des informations relatives
                        à la navigation de votre ordinateur sur notre site que
                        nous pourrons lire lors de vos visites ultérieures afin
                        de faciliter la navigation, d’optimiser la connexion et
                        de personnaliser l’utilisation du site. Vous pouvez
                        refuser l’utilisation des cookies en configurant les
                        paramètres de votre navigateur Internet.
                    </p>
                    <p>
                        Cependant le fait de refuser les cookies peut vous
                        empêcher d’accéder à certaines fonctionnalités du site.
                        L’accès client est construit avec un délai de session,
                        et certaines informations ne seront remises à jour
                        qu’après s’être reconnecté sur le site. Colbr ainsi que
                        ses fournisseurs d’informations déclinent toute
                        responsabilité quant au contenu, à l’exactitude, à la
                        fiabilité, à la pertinence et à l’exhaustivité des
                        informations et données diffusées sur le présent site.
                        Colbr et ses fournisseurs d’information déclinent toute
                        responsabilité quant aux difficultés techniques que
                        pourrait rencontrer les usagers de ce site, ce quelles
                        qu’en soient la cause ou l’origine, notamment dans le
                        cas de survenance de &ldquo;bogues&ldquo;, du
                        non-respect de l’intégrité de l’information à travers
                        les réseaux de communication, de défaut de capacité du
                        terminal de l’utilisateur pour restituer l’information
                        ou de transmission et d’acheminement dans les délais
                        normaux d’ordres du client. L’internaute reconnaît que
                        l’utilisation du site est régie par la loi française et
                        relève de la compétence des juges français. En cas de
                        contestation sur l’interprétation du contrat, sur
                        l’exécution et la réalisation des transactions, seul le
                        juge français est compétent pour connaître du litige.
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">Litige</h6>
                    <p>
                        En cas de litige ou de réclamation du client, les
                        parties contractantes s’engagent à rechercher en premier
                        lieu un arrangement amiable. Le client pourra présenter
                        sa réclamation à l’adresse de la société, à son
                        conseiller ou gestionnaire habituel qui disposera de 10
                        jours pour en accuser réception, puis de 2 mois à
                        compter de la réception de la réclamation pour y
                        répondre.
                    </p>
                    <p>
                        A défaut d’arrangement amiable, les parties pourront en
                        second lieu informer le médiateur de la consommation :
                    </p>
                    <p>
                        Pour l’activité CIF (médiateur public) : L’AMF –
                        L’Autorité des Marchés Financiers, Madame Marielle
                        COHEN-BRANCHE, 17 place de la Bourse 75082 Paris Cedex
                        02 (
                        <a
                            className="text-blue-400"
                            href="www.amf-france.org/Le-mediateur-de-l-AMF/Presentation"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            www.amf-france.org/Le-mediateur-de-l-AMF/Presentation
                        </a>
                        ){" "}
                    </p>
                    <p>
                        Pour les autres activités (médiateur recommandé par la
                        CNCGP) : Centre de Médiation et d’Arbitrage de Paris
                        (CMAP), Service Médiation de la consommation, 39 avenue
                        Franklin D. Roosevelt 75008 PARIS. Médiateurs :
                        Christophe AYELA, Jean-Marc BLAMOUTIER, Catherine
                        BOINEAU, Gilles CHARLOT, Michel GUIGAL (
                        <a
                            href="https://www.cmap.fr/offre/un-consommateur/"
                            target="_blank"
                            rel="noopener noreferrer"
                            className="text-blue-400"
                        >
                            www.mediateur-conso.cmap.fr
                        </a>{" "}
                        – consommation@cmap.fr)
                    </p>
                    <p>
                        En cas d’échec, le litige pourrait être porté devant les
                        tribunaux compétents.
                    </p>
                </div>

                <div className="space-y-4">
                    <h6 className="text-2xl text-white">
                        Autorités de tutelle
                    </h6>
                    <p>
                        Au titre de l’activité de conseil en investissement
                        financier : L’AMF – L’Autorité des Marchés Financiers –
                        17 place de la Bourse 75082 Paris Cedex 02.
                    </p>
                    <p>
                        Au titre de l’activité d’intermédiaire en assurance et
                        d’intermédiaire en opérations de banque et service de
                        paiement : ACPR – Autorité de Contrôle Prudentiel et de
                        Résolution – 4 place de Budapest CS 92459 75436 Paris
                        Cedex 09.
                    </p>
                    <p>
                        Les informations figurant sur le site notamment
                        financières et/ou fiscales, sont données à titre
                        indicatif. Ces informations qui ne prétendent pas à
                        l’exhaustivité ne sauraient avoir valeur de conseil, de
                        quelle que nature que ce soit, et ne sauraient valoir
                        dispense de l’avis d’un conseil personnalisé. En toute
                        hypothèse ces informations ne sauraient engager la
                        responsabilité de Colbr.
                    </p>
                </div>
            </div>
        </section>
    );
};

export default LegalInformation;
