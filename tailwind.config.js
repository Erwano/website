const colors = require('tailwindcss/colors')

module.exports = {
	content: [
		"./pages/**/*.{js,ts,jsx,tsx}",
		"./components/**/*.{js,ts,jsx,tsx}",
	],
	theme: {
		extend: {
			fontFamily: {
                'sans': [ "Geomanist", "ui-sans-serif", "system-ui" ],
            },
			borderRadius: {
				standard: '80px',
			},
			fontSize: {
				xxs: ['10px','12px']
			}
		},
		colors: {
			'colbr-black': '#090909',
			...colors
		},
	},
	plugins: [require('@tailwindcss/aspect-ratio'), require('@tailwindcss/forms')],
}
