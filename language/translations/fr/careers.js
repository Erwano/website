export default {
    title1: 'Des postes sont à pourvoir',
    title2: 'dans la team Colbr',
    subtitle: 'Toi aussi viens faire la différence !',
    apply: 'Envoyer ma candidature',
    knowMore: 'En savoir plus',
    knowLess: 'En savoir moins',
}