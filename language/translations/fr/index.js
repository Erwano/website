import careers from './careers';
import immobilier from './immobilier';
import landing from './landing';
import marchesFinanciers from './marchesFinanciers';
import privateEquity from './privateEquity';

export default {
  careers,
  immobilier,
  landing,
  marchesFinanciers,
  privateEquity,
};