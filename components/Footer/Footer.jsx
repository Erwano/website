import Image from "next/image";
import Link from "next/link";
import ColbrMini from "assets/colbrShort.png";
import { FaLinkedinIn } from "react-icons/fa";
import { BsInstagram } from "react-icons/bs";
import { links } from "./links";

const Footer = () => {
    return (
        <footer className="z-20 grid w-full grid-cols-12 gap-4 border-t-[1px] border-gray-900 bg-[#0C0C0C] p-5 text-white md:p-8">
            {links &&
                links.map((data, idx) => (
                    <div
                        className="col-span-6 flex flex-col items-center text-xs md:col-span-3 md:text-sm "
                        key={idx}
                    >
                        <div>
                            <h6 className="mb-2 font-medium uppercase text-white">
                                {data.name}
                            </h6>
                            {data.menu &&
                                data.menu.map((link, index) => (
                                    <Link href={link.url} key={index}>
                                        <p className="mb-2 font-light hover:cursor-pointer">
                                            {link.name}
                                        </p>
                                    </Link>
                                ))}
                        </div>
                    </div>
                ))}
            <div className="col-span-3 hidden w-full flex-col items-center justify-between space-y-3 md:flex ">
                <div>
                    <Link href="/">
                        <div className="flex max-w-[60px] cursor-pointer">
                            <Image src={ColbrMini} alt="Colbr" />
                        </div>
                    </Link>
                </div>
                <Link href="/login">
                    <a className="max-w-[150px] flex-shrink rounded-full border border-blue-900 py-2 px-8 text-center text-white hover:bg-blue-900">
                        Connexion
                    </a>
                </Link>
                <div className="mb-2 flex space-x-5">
                    <a
                        href="https://www.instagram.com/colbr.co/"
                        rel="noopener noreferrer"
                        target="_blank"
                    >
                        <BsInstagram
                            style={{
                                color: "white",
                                height: "20px",
                                width: "20px",
                                marginRight: "5px",
                            }}
                        />
                    </a>
                    <a
                        href="https://www.linkedin.com/company/colbr/"
                        rel="noopener noreferrer"
                        target="_blank"
                    >
                        <FaLinkedinIn
                            style={{
                                color: "white",
                                height: "20px",
                                width: "20px",
                            }}
                        />
                    </a>
                </div>
            </div>
            <div className="col-span-6 flex flex-col items-center text-xs md:hidden">
                <div>
                    <h6 className="mb-2 font-medium  uppercase">Nous suivre</h6>
                    <div className="mb-2 flex space-x-5">
                        <a
                            href="https://www.instagram.com/colbr.co/"
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            <BsInstagram
                                style={{
                                    color: "white",
                                    height: "20px",
                                    width: "20px",
                                    marginRight: "5px",
                                }}
                            />
                        </a>
                        <a
                            href="https://www.linkedin.com/company/colbr/"
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            <FaLinkedinIn
                                style={{
                                    color: "white",
                                    height: "20px",
                                    width: "20px",
                                }}
                            />
                        </a>
                    </div>
                </div>
            </div>
            <div className="col-span-12 flex justify-center text-xs md:hidden">
                <Link href="/login">
                    <a className="max-w-[150px] rounded-full border border-blue-900 py-2 px-8 text-center text-white hover:bg-blue-900">
                        Connexion
                    </a>
                </Link>
            </div>
        </footer>
    );
};

export default Footer;
