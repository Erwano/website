import {
    OfficeBuildingIcon, CurrencyDollarIcon, ChartBarIcon, ChartPieIcon,
    ColorSwatchIcon, CubeTransparentIcon, UserGroupIcon, NewspaperIcon,
    BriefcaseIcon,
} from "@heroicons/react/outline";
import { QuestionMarkCircleIcon, CubeIcon, StarIcon, PencilAltIcon } from "@heroicons/react/solid";

export const links = [
    {
        name: "Offre",
        current: false,
        icon: CurrencyDollarIcon,
        menu: [
            {
                name: "Marchés financiers",
                url: "/marches-financiers",
                icon: ChartBarIcon
            },
            {
                name: "Immobilier",
                url: "/immobilier",
                icon: OfficeBuildingIcon
            },
            {
                name: "Private Equity",
                url: "/private-equity",
                icon: ChartPieIcon,
            },
            {
                name: "Advisory",
                url: "/advisory",
                icon: ColorSwatchIcon
            },
        ],
    },
    {
        name: "Devenir membre",
        current: false,
        icon: StarIcon,
        menu: [
            {
                name: "Comment ça marche ?",
                url: "/comment-ca-marche",
                icon: QuestionMarkCircleIcon,
            },
            {
                name: "Colbr Black",
                url: "/offres/colbr-black",
                icon: CubeTransparentIcon,
            },
            {
                name: "Colbr Family-Office",
                url: "/offres/colbr-family-office",
                icon: CubeIcon
            },
        ],
    },
    {
        name: "À propos",
        current: false,
        icon: BriefcaseIcon,
        menu: [
            {
                name: "L'équipe",
                url: "/equipe",
                icon: QuestionMarkCircleIcon,
            },
            {
                name: "Mentions légales",
                url: "/legal"
            }
        ],
    },
];