import Image from "next/image";
import CheckCircleIcon from "assets/Immobilier/BlueCheckCircle.svg";
import Door from "assets/faisons-connaissance/door.png";
import BlueButton from "components/BlueButton";

const Presentation = () => {
    const data = [
        "Un conseil humain augmenté par la technologie.",
        "Un conseiller dédié, disponible par téléphone et en visioconférence.",
        "Une expérience fluide, sur mesure et transparente.",
    ];

    return (
        <div className="my-10 grid h-full grid-cols-12 lg:mb-0">
            <div className="col-span-12 flex h-full w-full flex-col-reverse items-center justify-center 2xl:justify-start md:flex-row">
                <div className="mt-5 flex max-w-sm flex-col items-center lg:mr-3">
                    <h1 className="mb-5 flex flex-col text-center text-xl font-medium text-white md:text-2xl">
                        <span className="font-light">
                            {" "}
                            Votre premier contact {" "}
                        </span>
                        avec Colbr{" "}
                    </h1>
                    <p className="text-center text-sm font-light text-white xl:w-11/12 ">
                        Lors de l’entretien, nous vous proposons de vous
                        accompagner seulement si nous pensons être la meilleure
                        solution pour vos projets.
                    </p>
                    <div className="mt-5 w-11/12 space-y-5 text-sm xl:text-left">
                        {data.map((detail, index) => (
                            <div className="flex items-center" key={index}>
                                <div className="mr-5 flex w-[10%] items-center justify-center">
                                    <Image src={CheckCircleIcon} alt="" />
                                </div>
                                <p className="flex w-[90%] text-white">
                                    {detail}
                                </p>
                            </div>
                        ))}
                    </div>
                </div>
                <div className="flex w-4/6 max-w-[240px] overflow-hidden rounded-3xl md:w-full md:max-w-xs">
                    <Image src={Door} alt="" />
                </div>
            </div>
            <div className="col-span-12 flex flex-col items-center justify-end text-white">
                <span className="mt-10 font-normal">Vous hésitez encore ?</span>
                <span className="font-light">
                    {" "}
                    Nous vous expliquons tout juste ici.
                </span>
                <div className="mt-5">
                    <BlueButton />
                </div>
            </div>
        </div>
    );
};
export default Presentation;
