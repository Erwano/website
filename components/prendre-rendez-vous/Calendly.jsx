import { InlineWidget, CalendlyEventListener } from "react-calendly";

const Calendly = (props) => {
    const { setSucces, ScrollTop } = props;

    const handleEventScheduled = (e) => {
        setSucces(true);
        ScrollTop()
    };

    return (    
        <div className="w-11/12 xl:w-full flex justify-center">
            <CalendlyEventListener onEventScheduled={handleEventScheduled}>
                <div style={{height: "100%", width: "100%"}}>
                    <InlineWidget
                        url="https://calendly.com/colbr/entree-en-relation-"
                        width="100%"
                        height="100%"
                        frameborder="0"
                    />
                </div>
            </CalendlyEventListener>
        </div>
    );
};
export default Calendly;
