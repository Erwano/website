import Image from "next/image";
import heroImg from "../../assets/Private-Equity/hero.png";
import BlueButton from "../../components/BlueButton";
import IconHero from "../../assets/Private-Equity/IconHero.svg";
import Bar from "../../assets/marches-financier/BarStayControl.svg";
import s from "./hero.module.scss"
import Mouse_bounce from "components/shared/MouseBounce";

const Hero = () => {
    return (
        <section className={`${s.backgroundImgDesk} page-section hero grid grid-cols-12 md:grid-rows-1`}>
            <div className="hidden md:flex hero-image-block-wrapper">
                <div className="hero-image-block">
                    <div className="hero-icon -left-8 top-20 md:-left-12 md:top-40">
                        <Image
                            src={IconHero}
                            alt="marché financier"
                            objectFit="contain"
                        />
                    </div>
                    <Image src={heroImg} alt="un couloir" className="rounded-3xl"/>
                    <div className="hero-text-block bottom-10 -right-10 md:-right-20">
                        <div className="w-5 h-5 md:w-6 md:h-6 mr-2">
                            <Image
                                src={Bar}
                                alt="signe validé"
                            />
                        </div>
                        Audit de performance
                    </div>
                </div>
            </div>
            <div className="hero-text-block-wrapper">
                <h1 className="main-title hero">
                    Private <span className="font-light">equity</span>
                </h1>
                <blockquote className="hero-text-subtitle">
                    &ldquo;If you don’t know the Jewelry, <br />{" "}
                    know the Jeweller.&rdquo;
                </blockquote>
                <p className="hero-text-author">Warren Buffet</p>
                <BlueButton />
            </div>
            <div className="hidden lg:block absolute bottom-10 left-20 2xl:left-0">
                <Mouse_bounce/>
            </div>
        </section>
    );
};

export default Hero;
