import Image from "next/image";
import Ardian from "../../assets/Private-Equity/Ardian.svg";
import BlackRock from "../../assets/Private-Equity/BlackRock.svg";
import Capza from "../../assets/Private-Equity/Capza.svg";
import Merieux from "../../assets/Private-Equity/merieux.svg";

const Funds = () => {
    return (
        <section className="page-section relative flex flex-col items-center justify-center">
            <h1 className="main-title">
                <span className="font-light">Un accès à des fonds</span>
                <br />
                de niveau institutionnel
            </h1>
            <div className="flex w-11/12 flex-wrap justify-around mt-12 2xl:w-2/3">
                <div className="flex w-1/2 flex-col items-center justify-center lg:w-1/4">
                    <div className="w-3/5">
                        <Image
                            src={Capza}
                            alt="entreprise Investissement Conseils"
                            quality={100}
                        />
                    </div>
                </div>
                <div className="flex w-1/2 flex-col items-center justify-center lg:w-1/4">
                    <div className="w-3/5">
                        <Image
                            src={Ardian}
                            alt="entreprise Les Echos"
                            quality={100}
                        />
                    </div>
                </div>
                <div className="flex w-1/2 flex-col items-center justify-center lg:w-1/4">
                    <div className="w-3/5">
                        <Image
                            src={BlackRock}
                            alt="entreprise l'Agefi Actifs 20 ans"
                            quality={100}
                        />
                    </div>
                </div>
                <div className="flex w-1/2 flex-col items-center justify-center lg:w-1/4">
                    <div className="w-3/5">
                        <Image
                            src={Merieux}
                            alt="entreprise News Managers"
                            width={221}
                            height={81}
                        />
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Funds;
