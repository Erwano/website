import Image from "next/image";
import { useTranslation } from "react-i18next";
import { QuestionMarkCircleIcon } from "@heroicons/react/outline";
import styles from "./Distribution.module.scss";
import Graph from "assets/Private-Equity/secondGraph.png";
import Wave from "assets/Private-Equity/wave.svg";
import Tooltip from "components/shared/Tooltip";

const Distribution = () => {
    const { t } = useTranslation();
    return (
        <section className="page-section flex flex-col items-center justify-center">
            <h1 className="main-title mb-10">
                Une structure d’appel de fonds <br />
                <span className="font-light">et de distribution optimisée</span>
            </h1>
            <div className="relative">
                <Image
                    src={Graph}
                    alt="Graphique montrant la classe d'actifs la plus performante des 20 dernières années"
                />
            </div>
            <div className="mt-10 flex w-full max-w-4xl justify-center flex-wrap text-xxs sm:text-xs md:text-sm lg:text-base">
                <div className="flex items-center text-white space-x-2 w-1/3 md:w-2/6 py-2 justify-center">
                    <Tooltip content={t('privateEquity.tooltips.distribution')}>
                        <QuestionMarkCircleIcon className={`${styles['min-width-4']} h-4 w-4 text-neutral-600 md:h-6 md:w-6`} />
                    </Tooltip>
                    <p>Distribution</p>
                    <div
                        className={`${styles['min-width-4']} h-4 w-4 lg:h-9 lg:w-9 rounded-full ${styles['background-distrib']}`}
                    />
                </div>
                <div className="flex items-center text-white space-x-2 text-center md:text-left  w-1/3 md:w-2/6 py-2 justify-center">
                    <Tooltip content={t('privateEquity.tooltips.progressiveCall')}>
                        <QuestionMarkCircleIcon className={`${styles['min-width-4']} h-4 w-4  text-neutral-600 md:h-6 md:w-6`} />
                    </Tooltip>
                    <p className="flex flex-col">
                        Appel progessif des capitaux
                    </p>
                    <div
                        className={`${styles['min-width-4']} h-4 w-4 lg:h-9 lg:w-9 rounded-full ${styles['background-progressif']}`}
                    />
                </div>
                <div className="flex items-center text-white space-x-2  w-1/3 md:w-2/6 py-2 justify-center">
                    <Tooltip content={t('privateEquity.tooltips.treasury')}>
                        <QuestionMarkCircleIcon className={`${styles['min-width-4']} h-4 w-4 text-neutral-600 md:h-6 md:w-6`} />
                    </Tooltip>
                    <p>Trésorerie</p>
                    <div className=" w-5">
                        <Image src={Wave} alt="" />
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Distribution;
