import Image from "next/image";
import { useTranslation } from "react-i18next";
import Graphique from "../../assets/Private-Equity/graph.svg";
import style from "./Graph.module.scss";
import {
    QuestionMarkCircleIcon,
    ExclamationIcon,
} from "@heroicons/react/outline";
import Tooltip from "components/shared/Tooltip";

const Graph = () => {
    const { t } = useTranslation();

    return (
        <section className="page-section flex flex-col items-center justify-center">
            <h1 className="main-title mb-10">
                <span className="font-light">
                    {" "}
                    Investissez dans la classe d’actifs
                </span> <br/>
                la plus performante des 15 dernières années
            </h1>
            <div
                className={`${style["backgroundGraph"]} flex flex-col items-center justify-center rounded-3xl p-5`}
            >
                <div className="max-w-4xl">
                    <Image
                        src={Graphique}
                        alt="Graphique montrant la classe d'actifs la plus performante des 20 dernières années"
                    />
                </div>
                <div className="mt-5 flex w-11/12 justify-between">
                    <div className="flex items-center text-gray-400 text-xs md:text-base">
                        <Tooltip content={t("privateEquity.tooltips.warning")}>
                            <ExclamationIcon className="mr-2 h-3 w-3 md:h-6 md:w-6" />
                        </Tooltip>
                        Avertissements
                    </div>
                    <div className="text-xs flex items-center font-light text-gray-400 md:text-base md:font-normal">
                        Source : Cambridge Associate
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Graph;
