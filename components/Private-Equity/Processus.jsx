import styles from "./Processus.module.scss"

const Processus = () => {
    return (
        <section className="page-section flex flex-col items-center">
            <h1 className="main-title">
                <span className="font-light">Un processus de sélection</span>{" "}
                <br />
                très rigoureux
            </h1>
            <div className={`${styles['background-private']} h-[900px] max-w-xl`}>
                <div className=" h-4/5 pt-20 pb-20 flex flex-col justify-between">
                    <div className="flex flex-col items-center p-2 text-center">
                        <h6 className="text-white">
                            Sélection du Fund Manager
                        </h6>
                        <p className="text-xs md:text-sm text-gray-600 w-1/2">
                            Audit réputationnel de la société de gestion
                        </p>
                    </div>
                    <div className="flex flex-col items-center p-2 text-center">
                        <h6 className="text-white">
                            Validation de la stratégie
                        </h6>
                        <p className="text-xs md:text-sm text-gray-600 w-1/2">
                            Cohérence avec l’environnement macro et micro
                            économique
                        </p>
                    </div>
                    <div className="flex flex-col items-center p-2 text-center">
                        <h6 className="text-white">Équipe</h6>
                        <p className="text-xs md:text-sm text-gray-600 w-1/2">
                            Validation de la compétence et de l’expérience par
                            rapport à la stratégie choisie
                        </p>
                    </div>
                    <div className="flex flex-col items-center p-2 text-center">
                        <h6 className="text-white">Performances</h6>
                        <p className="text-xs md:text-sm text-gray-600 w-1/2">
                            Analyse des historiques de performances
                        </p>
                    </div>
                    <div className="flex flex-col items-center p-2 text-center">
                        <p className="text-white w-2/5 text-base">
                            Colbr référence <br/> moins de 1% des fonds <br/> du marché{" "}
                        </p>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Processus;
