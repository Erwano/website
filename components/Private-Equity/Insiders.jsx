import Image from "next/image";
import insidersImg from "../../assets/Private-Equity/insiders.png";
import { PopupButton } from "@typeform/embed-react";

const Insiders = () => {
    return (
        <section className="page-section flex flex-col items-center justify-center backgroundImg bg-top bg-contain bg-no-repeat">
            <h1 className="main-title">
                <span className="font-light">
                    Rejoignez notre cercle d’initiés{" "}
                </span>
                <br />
                et recevez les dernières opportunités
            </h1>
            <p className="subtitle-text text-center max-w-lg mt-6">
                Nos analyses, simulations, présentations synthétiques des fonds et la mise en place d’alertes 
            </p>

            {/* <PopupButton id="l8KG3ZgI" className="blue-button my-10">
                S’inscrire à notre liste
            </PopupButton> */}
            <button  className="blue-button my-10">
                S’inscrire à notre liste
            </button>

            <div className="w-11/12 relative max-w-[280px] md:max-w-[550px] md:max-h-[550px] 2xl:max-w-[693px] 2xl:max-h-[693px] rounded-full overflow-hidden bg-top">
                <Image
                    src={insidersImg}
                    alt="Communaute Colbr"
                    objectFit="contain"
                />
            </div>
        </section>
    );
};
export default Insiders;
