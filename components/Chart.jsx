import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
ChartJS.register(ArcElement, Tooltip, Legend);

const Chart = ({data}) => {
    
    return (
       <Doughnut data={data} className="max-w-[250px] max-h-[250px]" />
    )
};

export default Chart;
