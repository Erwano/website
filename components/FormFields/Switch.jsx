import { motion } from "framer-motion";

const Switch = ({ isOn, ...props }) => {
    return (
        <motion.div
            className={`
             mt-3 flex h-10 w-20 cursor-pointer rounded-full p-1 transition-all
            duration-500
            ${isOn ? "justify-end bg-[#1C4699]" : "justify-start bg-[#dddddd]"}
        `}
            {...props}
        >
            <motion.div className="h-full w-8 rounded-full bg-white " />
        </motion.div>
    );
};

export default Switch;
