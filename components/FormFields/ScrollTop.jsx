
const ScrollTop = () => {
    window.scrollTo({
        top:0,
        behavior: "auto"
    })
}

export default ScrollTop