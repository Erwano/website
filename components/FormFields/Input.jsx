const Input = ({
    name,
    type,
    errors,
    touched,
    change,
    blur,
    value,
    placeholder,
}) => {
    const maxDate = () => {
        let date = new Date();
        let dd = date.getDate();
        let mm = date.getMonth() + 1;
        let yyyy = date.getFullYear();
        dd < 10 ? (dd = "0" + dd) : dd;
        mm < 10 ? (mm = "0" + mm) : mm;

        if (type === "date" && name !== "birthday") {
            
            return `${yyyy}-${mm}-${dd}`;

        } else if (type === "date" && name === "birthday") {
            
            yyyy = yyyy - 18;

            return `${yyyy}-${mm}-${dd}`;
        }
        return null;
    };

    return (
        <div className="flex flex-col">
            <input
                type={type}
                name={name}
                className={`w-full  appearance-none rounded-lg py-3 px-2 leading-tight outline-hidden placeholder:italic ${
                    errors && touched
                        ? "border-2 border-red-500 focus:border-red-500 focus:ring-0"
                        : "border-none"
                }`}
                placeholder={placeholder}
                onChange={change}
                onBlur={blur}
                value={value ? value : ""}
                max={maxDate()}
                min="0"
            />
            {errors && touched ? (
                <div className="h-4 w-full pl-1 text-xs text-red-500">
                    {errors}
                </div>
            ) : (
                <div className="h-4"></div>
            )}
        </div>
    );
};

export default Input;
