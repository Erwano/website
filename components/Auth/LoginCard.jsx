import React, { useEffect } from "react";

const LoginCard = () => {
    useEffect(() => {
        let hasError = false;
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        if (urlParams.get("error") === "AUTH") hasError = true;
        if (hasError) {
            document.getElementById("form-errors").innerHTML =
                "Identifiants incorrects";
        }
    }, []);

    const urlError =
        process.env.NODE_ENV === "production"
            ? process.env.NEXT_PUBLIC_PRODUCTION_LOGIN_URL_ERROR
            : process.env.NEXT_PUBLIC_DEVELOPMENT_LOGIN_URL_ERROR;

    const urlForgetPassword =
        process.env.NODE_ENV === "production"
            ? process.env.NEXT_PUBLIC_PRODUCTION_FORGET_PASSWORD_URL
            : process.env.NEXT_PUBLIC_DEVELOPMENT_FORGET_PASSWORD_URL;

    return (
        <div className="w-11/12 max-w-md rounded-2xl p-10">
            <h1 className="my-5 text-center text-2xl text-white md:text-4xl">
                Se connecter
            </h1>
            <p className="mb-8 border-b border-b-gray-500 pb-2 text-center text-white">
                Suivez vos investissements en temps réel.
            </p>
            <form
                className="flex h-full w-full max-w-sm flex-col items-center justify-center space-y-4"
                method="POST"
                action="https://www.moneypitch.fr/pitch/login"
            >
                {" "}
                <p id="form-errors" className="text-red-600"></p>
                <input name="redirect" type="hidden" value={urlError} />
                <div className="flex w-full flex-col space-y-3 py-2">
                    <label className="text-white">Email :</label>
                    <input
                        className=" w-full appearance-none rounded-xl border border-white bg-transparent py-2 px-2 leading-tight text-white placeholder:italic focus:outline-none"
                        type="text"
                        placeholder="vous@exemple.fr"
                        aria-label="Full name"
                        name="login"
                    />
                </div>
                <div className="fflex w-full flex-col space-y-3 py-2">
                    <label className="text-white">Mot de passe :</label>
                    <input
                        className=" w-full appearance-none rounded-xl border border-white bg-transparent py-2 px-2 leading-tight text-white placeholder:italic focus:outline-none"
                        type="password"
                        placeholder="******"
                        aria-label="Full name"
                        name="password"
                    />
                    <div className="mt-2 flex w-full">
                        <a
                            href={urlForgetPassword}
                            className="text-xs text-gray-400"
                        >
                            Mot de passe oublié ?
                        </a>
                    </div>
                </div>
                <div className="flex w-full">
                    <button
                        className="blue-button"
                        type="submit"
                    >
                        Connexion
                    </button>
                </div>
            </form>
        </div>
    );
};

export default LoginCard;
