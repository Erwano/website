import Image from "next/image";
import Timer from "assets/timer.svg";
import Cloud from "assets/cloud2.svg";
import BlueButton from "components/BlueButton";

const Start = () => {
    return (
        <section className="page-section relative flex flex-col items-center justify-center overflow-hidden">
            <div className="mb-10">
                <Image quality="85" src={Timer} alt="un chronomètre" />
            </div>

            <div className="right-2/6 absolute z-10 object-cover object-center">
                <Image quality={100} src={Cloud} alt="un nuage bleu" />
            </div>

            <h1 className="z-10 w-5/6 text-center text-2xl font-extralight text-white sm:flex sm:flex-col sm:items-center sm:justify-center md:text-4xl">
                Prenez 5 minutes pour commencer
                <span className="text-center font-bold">
                    à investir avec Colbr
                </span>
            </h1>
            <div className="mt-10 z-10 flex justify-center">
                <BlueButton name="Je commence avec Colbr" />
            </div>
        </section>
    );
};
export default Start;
