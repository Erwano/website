import Image from "next/image";
import heroImg from "assets/comment-ca-marche/hero.png";
import BlueButton from "components/BlueButton";
import s from "./hero.module.scss"
import Mouse_bounce from "components/shared/MouseBounce";

const Hero = () => {
    return (
        <section className={`${s.backgroundImgDesk} page-section hero grid grid-cols-12`}>
            <div className="hero-text-block-wrapper">
                <h1 className="main-title mb-5">
                    Comment <span className="font-light">ça marche ?</span>
                </h1>
                <p className="mb-5 max-w-xs text-center subtitle-text">
                    Une expérience de <br/> conseil sur-mesure, 
                    fluide et digitale
                </p>
                <div className="mt-5 mb-14">
                    <BlueButton name="Je prends rendez-vous" />
                </div>
            </div>
            <div className="hidden md:flex hero-image-block-wrapper mb-10 md:mb-0">
                <div className="hero-image-block">
                    <Image
                        src={heroImg}
                        className="overflow-hidden rounded-3xl"
                    />
                </div>
            </div>
            <div className="hidden lg:block absolute bottom-10 left-20 2xl:left-0">
                <Mouse_bounce/>
            </div>
        </section>
    );
};
export default Hero;
