import Image from "next/image";
import phone from "assets/comment-ca-marche/phonecolbr.png";
import BlueButton from "../BlueButton";
import CheckCircleIcon from "assets/Immobilier/BlueCheckCircle.svg";

const StayInCommand = () => {
    return (
        <section className="page-section backgroundImg relative flex flex-col items-center bg-top">
            <div className="grid grid-cols-12">
                <div className="col-span-12 flex justify-center md:col-span-5 md:col-start-2">
                    <div className="relative w-1/2 max-w-[300px] md:w-3/5 2xl:w-4/5">
                        <Image
                            src={phone}
                            alt="telephone mobile montrant l'applicatyion Colbr un example de portefeuille client"
                            width={514}
                            heigth={1004}
                            quality={100}
                        />
                    </div>
                </div>
                <div className="col-span-12 md:col-span-5 md:col-start-8 md:flex md:flex-col md:justify-center">
                    <h1 className="text-center font-medium text-2xl text-white md:mb-10 md:text-left lg:text-4xl">
                        Un suivi en temps réel
                        <br />
                        <span className="font-light"> de votre patrimoine </span>
                    </h1>
                    <div className="mt-10 space-y-5 text-sm md:text-base lg:w-11/12 lg:text-left text-white">
                        <div className="flex items-center">
                            <div className="w-8 h-8 mr-5 flex-shrink-0">
                                <Image src={CheckCircleIcon} alt="" />
                            </div>
                            <p>
                                <span className="font-light">Pilotez </span>
                                vos versements de manière programmée ou
                                automatique
                            </p>
                        </div>
                        <div className="flex items-center">
                            <div className="mr-5">
                                <Image src={CheckCircleIcon} alt="" />
                            </div>
                            <p>
                                <span className="font-light">Bénéficiez </span>
                                d’un reporting annuel
                            </p>
                        </div>
                        <div className="flex items-center">
                            <div className="mr-5">
                                <Image src={CheckCircleIcon} alt="" />
                            </div>
                            <p>
                                <span className="font-light">
                                    Accédez à nos{" "}
                                </span>{" "}
                                propositions d’arbitrage
                            </p>
                        </div>
                        <div className="flex items-center">
                            <div className="mr-5">
                                <Image src={CheckCircleIcon} alt="" />
                            </div>
                            <p>
                                <span className="font-light">
                                    Dans un univers
                                </span>{" "}
                                compatible Desktop & Mobile
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default StayInCommand;
