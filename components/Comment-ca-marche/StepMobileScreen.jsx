import BlueButton from "components/BlueButton";
import Image from "next/image";
import step1 from "../../assets/comment-ca-marche/step1.svg";
import step2 from "../../assets/comment-ca-marche/step2.svg";
import step3 from "../../assets/comment-ca-marche/step3.svg";
import SigneValid from "../Home/SeventhSection/ImageValidate";

const StepMobileScreen = () => {
    const cards = [
        {
            img: step1,
            titleStep: "étape 1",
            titleCard: "Faisons connaissance",
            description:
                "À l’issue d’un entretien de 15 min, nous vous proposerons de vous accompagner seulement si nous pensons être la meilleure solution pour vos projets.",
            button: true,
        },
        {
            img: step2,
            titleStep: "étape 2",
            titleCard: "Onboarding",
            description:
                " Nous concevons pour vous une structuration patrimoniale 100% personnalisée avec les meilleures solutions du marché.",
            list: [
                "2 rendez-vous",
                "Votre bilan patrimonial",
                "Proposiiton d’une stratégie sur-mesure",
            ],
        },
        {
            img: step3,
            titleStep: "étape 3",
            titleCard: "Un sparring partner dans la durée",
            description:
                " Nous suivons votre épargne en continu en vous tenant informé des opportunités et des risques de vos placements afin de préserver et valoriser votre patrimoine dans le temps.",
            list: [
                "Un advisor dédié",
                "Des opportunités d’investissements réservées aux membres",
                "Des évènement exclusifs",
            ],
        },
    ];

    return (
        <div className="flex w-full flex-col items-center pt-10 md:hidden">
            {cards.map((card, idx) => (
                <div
                    className="mb-20 flex max-w-xs flex-col items-center space-y-5"
                    key={idx}
                >
                    <div className="flex items-center">
                        <Image src={card.img} alt="" />
                        <h1 className="ml-2 uppercase text-white">
                            {card.titleStep}
                        </h1>
                    </div>
                    <h6 className="text-white">{card.titleCard}</h6>
                    <p className="text-center text-xs text-gray-400">
                        {card.description}
                    </p>
                    {card.button ? <BlueButton name="Prendre rendez-vous"/> : null}
                    {card.list && card.list.length > 0 ? (
                        <ul className="mt-2 w-full space-y-1 pl-4 text-xs text-gray-400">
                            {card.list.map((data, idx) => (
                                <li
                                    key={idx}
                                    className="flex w-full items-center"
                                >
                                    <SigneValid
                                        margin={true}
                                        className="w-1/5"
                                    />{" "}
                                    <span className="w-4/5">{data}</span>
                                </li>
                            ))}
                        </ul>
                    ) : null}
                </div>
            ))}
        </div>
    );
};
export default StepMobileScreen;
