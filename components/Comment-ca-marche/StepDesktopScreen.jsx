import Image from "next/image";
import fil from "../../assets/fil2.svg";
import step1 from "../../assets/comment-ca-marche/step1.svg";
import step2 from "../../assets/comment-ca-marche/step2.svg";
import step3 from "../../assets/comment-ca-marche/step3.svg";
import SigneValid from "../Home/SeventhSection/ImageValidate";
import BlueButton from "components/BlueButton";

const StepDesktopScreen = () => {
    return (
        <div className="relative hidden max-w-[560px] font-light grid-cols-12 grid-rows-3 gap-y-8 md:grid xl:max-w-3xl">
            <div className="relative col-span-2 col-start-6 row-span-3 flex h-full w-full items-center justify-center">
                <Image
                    src={fil}
                    alt=""
                />
            </div>
            <div className="relative col-span-5 col-start-1 row-span-1 row-start-1 flex items-center justify-end">
                <div className="relative flex flex-col items-center">
                    <Image src={step1} alt="batiment" objectFit="cover" />
                    <h6 className="text-white uppercase text-lg font-normal mt-2">étape 1</h6>
                </div>
            </div>
            <div className="relative col-span-5 col-start-8 row-span-1 row-start-1 flex items-center justify-center">
                <div className="flex flex-col items-start ">
                    <h6 className="uppercase text-white text-lg font-normal">
                        Faisons connaissance
                    </h6>
                    <p className="mt-3 mb-6 text-sm text-gray-400">
                        À l’issue d’un entretien de 15 min, nous vous proposerons de vous
                        accompagner seulement si nous pensons être la meilleure
                        solution pour vos projets.
                    </p>
                    <BlueButton name="Prendre rendez-vous"/>
                </div>
            </div>
            <div className="relative col-span-5 col-start-8 row-span-1 row-start-2 flex items-center justify-start">
                <div className="relative max-w-[130px] flex flex-col items-center">
                    <Image src={step2} alt="Communication" />
                    <h6 className="text-white uppercase text-lg font-normal mt-2">étape 2</h6>
                </div>
            </div>
            <div className="relative col-span-5 col-start-1 row-span-1 row-start-2 flex items-center justify-center">
                <div className="flex flex-col items-end">
                    <h6 className="mb-3 uppercase text-white text-lg">Onboarding</h6>
                    <p className="text-right text-sm text-gray-400 mb-2">
                        Nous concevons pour vous une structuration patrimoniale
                        100% personnalisée avec les meilleures solutions du
                        marché.
                    </p>
                    <ul className="mt-2 space-y-2 text-sm text-gray-400 ">
                        <li className="flex w-full items-center flex-row-reverse">
                            <SigneValid/>
                            <span className="mr-2">2 rendez-vous</span>
                        </li>
                        <li className="flex w-full items-center flex-row-reverse">
                            <SigneValid/>
                            <span className="mr-2">
                                Votre bilan patrimonial
                            </span>
                        </li>
                        <li className="flex w-full items-center flex-row-reverse">
                            <SigneValid/>
                            <span className="mr-2">
                                Proposiiton d’une stratégie sur-mesure
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="relative col-span-5 col-start-1 row-span-1 row-start-3 flex items-center justify-end">
                <div className="relative max-w-[116px] flex flex-col items-center">
                    <Image src={step3} alt="euros" />
                    <h6 className="text-white uppercase text-lg font-normal mt-2">étape 3</h6>
                </div>
            </div>
            <div className="relative col-span-5 col-start-8 row-span-1 row-start-3 flex items-center justify-start">
                <div className="flex flex-col items-start">
                    <h6 className="uppercase text-white text-lg font-normal">
                        Un sparring partner dans la durée
                    </h6>
                    <p className="text-gray-400 text-sm mb-2">
                        Nous suivons votre épargne en continu en vous tenant
                        informé des opportunités et des risques de vos
                        placements afin de préserver et valoriser votre
                        patrimoine dans le temps.
                    </p>
                    <ul className="mt-2 space-y-1 text-sm text-gray-400">
                        <li className="flex w-full items-center">
                            <SigneValid margin={true} />{" "}
                            <span>Un advisor dédié</span>
                        </li>
                        <li className="flex w-full items-center">
                            <SigneValid margin={true} />{" "}
                            <span>
                                Des opportunités d’investissements réservées aux
                                membres
                            </span>
                        </li>
                        <li className="flex w-full items-center">
                            <SigneValid margin={true} />{" "}
                            <span>
                                Des évènement exclusifs
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};
export default StepDesktopScreen;
