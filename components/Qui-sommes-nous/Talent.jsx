import Image from "next/image";
import Lucas from "assets/qui-sommes-nous/Lucas.jpg";
import Erwan from "assets/qui-sommes-nous/Erwan.jpg";
import Ryan from "assets/qui-sommes-nous/Ryan.jpg";
import { SiLinkedin } from "react-icons/si";

const cardTalent = [
    {
        img: Ryan,
        name: "Ryan Arfaoui",
        post: "Analyste financier",
        linkedin: "https://www.linkedin.com/in/ryan-arfaoui/"
    },
    {
        img: Erwan,
        name: "Erwan Rouault",
        post: "Développeur Fullstack",
        linkedin: "https://www.linkedin.com/in/erwan-rouault/"
    },
    {
        img: Lucas,
        name: "Lucas Dupuy",
        post: "Marketing & Content Associate",
        linkedin: "https://www.linkedin.com/in/lucas-dupuy-pro01/"
    },
];

const Talent = () => {
    return (
        <section className="page-section flex flex-col items-center justify-center">
            <h1 className="main-title flex flex-col items-center text-center font-light md:flex-row ">
                Des talents{" "}
                <span className="font-medium md:ml-3">complémentaires</span>
            </h1>
            <p className="hidden md:block subtitle-text mt-6 w-11/12 max-w-2xl text-center lg:text-base">
                Nous sommes fiers de pouvoir regrouper des talents différents
                aux parcours complémentaires. Tous animés par les mêmes valeurs
                & la même mission : faire de Colbr la première néo-banque privée
                européenne.
            </p>
            <div className="mt-20 flex w-full max-w-6xl flex-col items-center justify-around space-y-5 md:flex-row md:items-start md:space-y-0">
                {cardTalent.map((profil, index) => (
                    <div
                        className="mb-5 flex flex-col items-center md:mb-0 md:w-1/3"
                        key={index}
                    >
                        <div className="flex h-64 w-64  overflow-hidden rounded-full">
                            <Image
                                src={profil.img}
                                alt={profil.name}
                                objectFit="cover"
                                objectPosition="center"
                            />
                        </div>
                        <h6 className="mt-5 mb-2 flex items-center justify-center text-xl text-white">
                            {profil.name}
                            <a href={profil.linkedin} target="_blank" rel="noopener noreferrer">
                                <SiLinkedin
                                    fill="white"
                                    className="ml-2 mb-1 hover:cursor-pointer"
                                />
                            </a>
                        </h6>
                        <p className="text-gray-400">{profil.post}</p>
                    </div>
                ))}
            </div>
        </section>
    );
};

export default Talent;
