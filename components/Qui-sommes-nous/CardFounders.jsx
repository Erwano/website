import { SiLinkedin } from "react-icons/si";
import Image from "next/image";

const CardFounders = (props) => {
    const { img, alt, title, linkLinkedin, description } = props;

    return (
        <>
            <div className="flex h-64 w-64 overflow-hidden rounded-full">
                <Image src={img} alt={alt} objectFit="cover" className={`${title === "Gustav Sondén" ? "scale-125" : ""}`}/>
            </div>
            <div className="mt-5 w-full max-w-sm text-center">
                <div className="flex w-full items-center justify-center space-x-2">
                    <h6 className="text-white md:text-xl">{title}</h6>
                    <a
                        href={linkLinkedin}
                        rel="noopener noreferrer"
                        target="_blank"
                    >
                        <SiLinkedin
                            fill="white"
                            className="mb-1 hover:cursor-pointer"
                        />
                    </a>
                </div>

                <p className="mt-5 text-xs font-light leading-5 text-gray-400 md:text-base">
                    {description}
                </p>
            </div>
        </>
    );
};
export default CardFounders;
