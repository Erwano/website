import React from "react";
import Image from "next/image";
import Gustav from "assets/Gustav.jpeg";
import Romain from "assets/qui-sommes-nous/Romain.jpeg";
import Edmond from "assets/qui-sommes-nous/Edmond.png";
import Mercialys from "assets/qui-sommes-nous/mercialys.svg";
import Generali from "assets/qui-sommes-nous/Generali.png";
import Butler from "assets/qui-sommes-nous/butler.svg";
import Obc from "assets/qui-sommes-nous/obc.svg";
import Tikehau from "assets/qui-sommes-nous/Tikehau.png";
import styles from "./Founders.module.scss";
import Tooltip from "components/shared/Tooltip";
import Arrow from "assets/marches-financier/doubleArrow.svg";
import CardFounders from "./CardFounders";
import Mickael from "assets/qui-sommes-nous/mickael.jpg";

const Fonders = () => {
    const enterprise = [
        {
            img: Generali,
            name: "Generali",
        },
        {
            img: Mercialys,
            name: "Mercialys",
        },
        {
            img: Butler,
            name: "Corum Butler",
        },
        {
            img: Obc,
            name: "Neuflize OBC",
        },
        {
            img: Tikehau,
            name: "Tikehau Capital",
        },
        {
            img: Edmond,
            name: "Edmond de Rothschild",
        },
    ];

    const logoGustav = [
        {
            img: Generali,
            name: "Generali",
        },
        {
            img: Mercialys,
            name: "Mercialys",
        },

    ];

    const logoRomain = [
        {
            img: Butler,
            name: "Corum Butler",
        },
        {
            img: Obc,
            name: "Neuflize OBC",
        },
    ];

    const logoMickael = [
        {
            img: Tikehau,
            name: "Tikehau Capital",
        },
        {
            img: Edmond,
            name: "Edmond de Rothschild",
        },
    ]

    return (
        <section className="page-section grid grid-cols-12 space-y-5">
            <div className="col-span-12 mb-8 flex flex-col items-center space-y-4">
                <h1 className="main-title text-center ">
                    <span className="font-light">
                        {" "}
                        Des associés <br />
                    </span>{" "}
                    expérimentés et ambitieux
                </h1>
                <p className="w-full max-w-3xl text-center text-xs font-light leading-5 text-gray-400 md:text-base">
                    Le projet Colbr est né de la rencontre de Gustav et Romain
                    en 2019 après 15 ans d’expérience cumulée dans le secteur de
                    la finance. En 2022, ils sont rejoints par Mickael
                    Juvenelle, ancien Head of IT Business de Tikehau Capital et maintenant en
                    charge du département technologique chez Colbr.
                </p>
            </div>

            <div className="col-span-12 flex w-full flex-col items-center md:col-span-4 ">
                <CardFounders
                    img={Gustav}
                    alt="Gustav sondèn"
                    title="Gustav Sondén"
                    linkLinkedin="https://www.linkedin.com/in/gustav-sond%C3%A9n-7bb37435/"
                    description=" Gustav a investi près d’1 milliard d’euros dans 5 pays
                    différents. Il est franco-suédois et parle couramment
                    anglais, suédois et espagnol."
                />
            </div>
            <div className="col-span-12 flex w-full justify-center md:hidden">
                <div className="my-10 flex w-full max-w-6xl flex-wrap justify-evenly">
                    {logoGustav.map((data, idx) => (
                        <div key={idx}>
                            <div
                                className={`flex h-16 w-16 items-center justify-center p-3 md:h-24 md:w-24 md:p-2 ${styles.background_logo} overflow-hidden rounded-full`}
                            >
                                <Tooltip content={data.name}>
                                    <Image
                                        src={data.img}
                                        alt=""
                                        objectPosition="center"
                                    />
                                </Tooltip>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <div className="col-span-12 flex flex-col items-center md:col-span-4">
                <CardFounders
                    img={Romain}
                    alt="Romain Joudelat"
                    title="Romain Joudelat"
                    linkLinkedin="https://www.linkedin.com/in/romain-joudelat-96535832/"
                    description="Romain a levé plus de 250 millions d’euros pour des
                    fonds actions, obligataires et immobiliers auprès de
                    grands investisseurs et clients institutionnels."
                />
            </div>
            <div className="col-span-12 flex w-full justify-center  md:hidden">
                <div className="mt-10 flex w-full justify-evenly">
                    {logoRomain.map((data, idx) => (
                        <div key={idx}>
                            <div
                                className={`flex h-16 w-16 items-center justify-center p-3 md:h-24 md:w-24 md:p-2 ${styles.background_logo} overflow-hidden rounded-full`}
                            >
                                <Tooltip content={data.name}>
                                    <Image
                                        src={data.img}
                                        alt=""
                                        objectPosition="center"
                                    />
                                </Tooltip>
                            </div>
                        </div>
                    ))}
                </div>
            </div>

            <div className="col-span-12 flex flex-col items-center md:col-span-4">
                <div className="mt-10 md:mt-0"></div>
                <CardFounders
                    img={Mickael}
                    alt="Mickael Juvenelle"
                    title="Mickael Juvenelle"
                    linkLinkedin="https://www.linkedin.com/in/mickael-juvenelle-0840a24a/"
                    description="Mickaël cumule plus de 15 années d’expérience dans la Tech et la Finance à divers postes de haut niveau chez des acteurs majeurs européens."
                />
            </div>

            <div className="col-span-12 flex w-full justify-center  md:hidden">
                <div className="mt-10 flex w-full justify-evenly">
                    {logoMickael.map((data, idx) => (
                        <div key={idx}>
                            <div
                                className={`flex h-16 w-16 items-center justify-center p-3 md:h-24 md:w-24 md:p-2 ${styles.background_logo} overflow-hidden rounded-full`}
                            >
                                <Tooltip content={data.name}>
                                    <Image
                                        src={data.img}
                                        alt=""
                                        objectPosition="center"
                                    />
                                </Tooltip>
                            </div>
                        </div>
                    ))}
                </div>
            </div>

            <div className="col-span-12 hidden w-full flex-col items-center md:flex md:pt-10 md:text-xl">
                <h6 className="mb-5 text-center uppercase text-white">
                    Nos références
                </h6>
                <div className="animate-bounce">
                    <Image
                        src={Arrow}
                        alt="deux flèches qui pointes vers le bas"
                        quality={75}
                    />
                </div>
            </div>
            <div className="col-span-12 flex w-full justify-center">
                <div className="mt-10 hidden w-full max-w-6xl flex-wrap justify-evenly md:flex">
                    {enterprise.map((data, idx) => (
                        <div key={idx}>
                            <div
                                className={`flex h-16 w-16 items-center justify-center p-3 md:h-24 md:w-24 md:p-2 ${styles.background_logo} overflow-hidden rounded-full`}
                            >
                                <Tooltip content={data.name}>
                                    <Image
                                        src={data.img}
                                        alt=""
                                        objectPosition="center"
                                    />
                                </Tooltip>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    );
};

export default Fonders;
