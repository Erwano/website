import Image from "next/image";
import team from "assets/qui-sommes-nous/aviron.jpg";
import BlueButton from "../BlueButton";
import Mouse_bounce from "../shared/MouseBounce";
import s from "./hero.module.scss";

const Hero = () => {
    return (
        <section
            className={`${s.backgroundImgDesk} page-section hero flex items-center justify-center`}
        >
            <div className="relative grid h-full w-full grid-cols-12 md:grid-flow-col">
                <div className="hero-image-block-wrapper hidden md:flex">
                    <div className="hero-image-block flex overflow-hidden rounded-3xl bg-center">
                        <Image src={team} alt="équipe Colbr" quality={100} />
                    </div>
                </div>
                <div className="hero-text-block-wrapper">
                    <h1 className="main-title hero mb-5">
                        <span className="font-light">
                            De véritables experts
                        </span>
                        <br />
                        de la finance et de l’investissement
                    </h1>
                    <BlueButton />
                </div>
            </div>
            <div className="absolute bottom-10 left-20 hidden lg:block 2xl:left-0">
                <Mouse_bounce />
            </div>
        </section>
    );
};

export default Hero;
