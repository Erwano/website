import { Disclosure } from "@headlessui/react";
import { BsPlusCircle } from "react-icons/bs";
import { VscChromeMinimize } from "react-icons/vsc";
const ValueText = (props) => {
    const { title, description } = props;

    return (
        <Disclosure>
            {({ open }) => (
                <>
                    <div className="flex flex-shrink text-gray-400">
                        <Disclosure.Button className="flex">
                            <h1 className={`text-5xl ${open ? "text-white" : ""}`}>
                                {title}
                            </h1>
                            <div className="mt-2 ml-2 h-5 w-5">
                                {open ? <VscChromeMinimize /> : <BsPlusCircle />}
                            </div>
                        </Disclosure.Button>
                    </div>
                    <Disclosure.Panel className="w-60 pt-2 font-light text-justify text-gray-400 md:absolute md:mt-11 md:w-80">
                        {description}
                    </Disclosure.Panel>
                </>
            )}
        </Disclosure>
    );
};
export default ValueText;
