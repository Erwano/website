import Image from "next/image";
import logoColbr from "../../assets/logo-colbr.svg";
import CloudHorizontal from "../../assets/EllipseHorizontal.png";
import ValueText from "./ValueText";

const Values = () => {
    return (
        <section className="page-section relative flex flex-col items-center justify-center space-y-20 pb-32">
            <div className="absolute top-10 -left-20">
                <Image
                    src={CloudHorizontal}
                    alt="fond bleu en forme de nuage"
                />
            </div>
            <h1 className="text-center text-xl font-medium text-white md:text-4xl">
                Les valeurs sont plus que des mots <br />{" "}
                <span className="font-light">c’est notre manière de faire</span>
            </h1>
            <div className="grid grid-cols-12 gap-8 md:grid-rows-3 lg:mb-20">
                <div className="col-span-12 flex justify-center md:col-span-3 md:col-start-2 md:row-span-1 md:row-start-1 md:items-center md:justify-end">
                    <div className="relative flex flex-col items-center">
                        <ValueText
                            title="AUDACE"
                            description="Nous disons et faisons ce que nous pensons être juste, indépendamment du consensus."
                        />
                    </div>
                </div>
                <div className="z-10 col-span-12 flex justify-center md:col-span-2 md:col-start-9 md:row-span-1 md:row-start-1 md:items-center md:justify-start">
                    <div className="relative flex flex-col items-center">
                        <ValueText
                            title="ENGAGEMENT"
                            description="Toutes les actions que nous menons sont le reflet des engagements que nous tenons."
                        />
                    </div>
                </div>
                <div className=" hidden col-span-12 md:flex md:col-span-2 md:col-start-6 md:row-span-1 md:row-start-2 md:items-center md:justify-center">
                    <Image src={logoColbr} alt="Entreprise Colbr" />
                </div>
                <div className="z-10 col-span-12 flex justify-center md:col-span-2 md:col-start-4 md:row-span-1 md:row-start-3 md:items-center md:justify-end">
                    <div className="relative flex flex-col items-center">
                        <ValueText
                            title="EXCELLENCE"
                            description="C’est le trait qui caractérise nos partenaires et nous oblige vis-à-vis de nos clients."
                        />
                    </div>
                </div>
                <div className="z-10 col-span-12 flex justify-center md:col-span-2 md:col-start-8 md:row-span-1 md:row-start-3 md:items-center md:justify-start">
                    <div className="relative flex flex-col items-center">
                        <ValueText
                            title="TRANSPARENCE"
                            description="Nous ne laissons place à aucun doute ni surprise sur notre manière de faire et nos frais."
                        />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Values;
