import BlueButton from "components/BlueButton";
import Image from "next/image";
import CloudHorizontal from "../../assets/EllipseHorizontal.png";

const Hiring = () => {
    return (
        <section className="page-section last flex flex-col items-center justify-center space-y-5 relative">
            <h1 className="main-title">
                <span className="font-light">Colbr recrute, </span>
                <br />
                Rejoignez notre équipe !
            </h1>
            <div className="flex w-full items-center justify-center">
                <BlueButton name="Postulez ici" href="/carrieres" />
            </div>
            <div className="absolute -z-10">
                <Image
                    src={CloudHorizontal}
                    alt="fond bleu en forme de nuage"
                />
            </div>
        </section>
    );
};

export default Hiring;
