import Image from "next/image";
import Goreon from "assets/qui-sommes-nous/Etienne.jpeg";
import Prat from "assets/qui-sommes-nous/prat.jpg";
import Ravat from "assets/qui-sommes-nous/ravat.jpg";
import { SiLinkedin } from "react-icons/si";

const cardBoard = [
    {
        img: Goreon,
        name: "Etienne Gorgeon",
        title: "Gérant associé @SANSO IS",
        description:
            "Sanso Investment Solution est une société de gestion de portefeuille ayant plus d’1.5 milliards d’encours.",
        linkedin: "https://www.linkedin.com/in/etienne-gorgeon-1995a558/",
    },
    {
        img: Ravat,
        name: "Vincent Ravat",
        title: "CEO @MERCIALYS",
        description:
            "Mercialys est l’une des principales sociétés foncières européennes cotée en bourse au SBF120.",
        linkedin: "https://www.linkedin.com/in/vincentravat/",
    },
    {
        img: Prat,
        name: "Raphael Prat",
        title: "CTO & Co-founder @TOKY WOKY",
        description:
            "Tokywoky est une plateforme qui rassemble les clients autours des marques pour accélérer leur croissance.",
        linkedin: "https://www.linkedin.com/in/raphaelprat/",
    },
];

const Board = () => {
    return (
        <section className="page-section flex flex-col items-center justify-center">
            <h1 className="main-title flex flex-col items-center font-light text-white md:flex-row">
                Un board <span className="font-medium md:ml-3">de premier rang</span>
            </h1>
            <p className="hidden md:block subtitle-text mt-6 w-11/12 max-w-2xl text-center lg:text-base">
                Notre Board est constitué de personnalités reconnues de
                l’industrie de la finance, de l’investissement et de la
                technologie. Ils jouent un rôle clef dans la définition de nos
                stratégies d’allocations d’actifs et dans notre développement.
            </p>
            <div className="mt-20 flex max-w-6xl flex-col items-center justify-around space-y-5 md:flex-row md:items-start md:space-y-0">
                {cardBoard.map((profil, index) => (
                    <div
                        className="mb-5 flex flex-col items-center md:w-1/3 "
                        key={index}
                    >
                        <div className="flex h-64 w-64 overflow-hidden rounded-full">
                            <Image
                                src={profil.img}
                                alt={profil.name}
                                objectFit="cover"
                                objectPosition="center"
                            />
                        </div>
                        <h6 className="mt-5 mb-2 flex items-center justify-center text-xl text-white">
                            {profil.name}
                            <a href={profil.linkedin} target="_blank" rel="noopener noreferrer">
                                <SiLinkedin
                                    fill="white"
                                    className="ml-2 mb-1 hover:cursor-pointer"
                                />
                            </a>
                        </h6>
                        <p className="text-center text-gray-400">
                            {profil.title}
                        </p>
                        <p className="mt-5 w-4/5 text-center text-gray-400">
                            {profil.description}
                        </p>
                    </div>
                ))}
            </div>
        </section>
    );
};

export default Board;
