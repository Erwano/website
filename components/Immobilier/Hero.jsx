import Image from "next/image";
import BlueButton from "../../components/BlueButton";
import IconHero from "../../assets/Immobilier/iconHero.svg"
import heroImg from "../../assets/Immobilier/imgHero.jpg"
import target from "../../assets/Immobilier/target.svg"
import s from "./hero.module.scss"
import Mouse_bounce from "components/shared/MouseBounce";

const Hero = () => {
    return (
        <section className={`${s.backgroundImgDesk} page-section hero grid grid-cols-12`}>
            <div className="hidden md:flex hero-image-block-wrapper mb-10 md:mb-0">
                <div className="hero-image-block">
                    <div className="hero-icon -right-8 top-10 md:-right-14 md:top-20">
                        <Image
                            src={IconHero}
                            alt=""
                        />
                    </div>
                    <Image src={heroImg} alt="" className="rounded-3xl"/>
                    <div className="hero-text-block bottom-20 -left-10 md:-right-24 md:bottom-20">
                        <div className="w-5 h-5 md:w-6 md:h-6 mr-2">
                            <Image src={target} alt=""/>
                        </div>
                        Ciblage haut de gamme
                    </div>
                </div>
            </div>
            <div className="hero-text-block-wrapper">
                <h1 className="main-title hero">
                    Immobilier
                </h1>
                <blockquote className="hero-text-subtitle">
                    &ldquo;Price is what you pay,<br/> Value is what you
                    get.&rdquo;
                </blockquote>
                <p className="hero-text-author">Warren Buffet</p>
                <BlueButton />
            </div>
            <div className="hidden lg:block absolute bottom-10 left-20 2xl:left-0">
                <Mouse_bounce/>
            </div>
        </section>
    );
};
export default Hero;
