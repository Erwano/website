import Image from 'next/image';
import { useTranslation } from 'react-i18next';
import OneImg from '../../assets/Immobilier/1.svg';
import TwoImg from '../../assets/Immobilier/2.svg';
import ThreeImg from '../../assets/Immobilier/3.svg';
import FourImg from '../../assets/Immobilier/4.svg';
import LineImg from '../../assets/Immobilier/line.svg';

const InvestWithExperts = () => {
    const { t } = useTranslation();
    return (
        <section className="page-section flex-col items-center">
            <h1 className="main-title mt-10 mb-4">
                {t('immobilier.methodTitle1')}<span className="font-light"> {t('immobilier.methodTitle2')}</span>
            </h1>
            
            <div className="grid grid-cols-12 grid-rows-4 px-2 md:px-12">
                <div className="relative col-span-2 col-start-6 row-span-4 h-full flex items-center">
                    <div className='h-4/5 py-2 flex w-full justify-center'>
                        <Image src={LineImg} alt=""/>
                    </div>
                </div>

                <div className="col-start-1 row-start-1 col-span-5 relative flex items-center justify-center w-full h-64 title-gradient">
                    <div className="absolute top-2/4 left-2/4 w-32 h-32 -translate-x-2/4 -translate-y-2/4 md:w-48 md:h-48 md:mt-6">
                        <Image src={OneImg} alt=""/>
                    </div>
                    <div className="text-white text-center text-sm md:text-3xl font-semibold uppercase">
                        {t('immobilier.methodFirstStepTitle')}
                    </div>
                </div>
                <div className="col-start-8 row-start-1 col-span-5 flex items-center justify-start w-full h-72 md:justify-center">
                    <div className="text-gray-400 font-extralight text-sm md:text-xl md:w-4/6">
                        {t('immobilier.methodFirstStep')}
                    </div>
                </div>

                <div className="col-start-1 row-start-2 col-span-5 flex items-center justify-end w-full h-72 md:justify-center">
                    <div className="text-gray-400 font-extralight text-right text-sm md:text-xl md:w-4/6">
                        {t('immobilier.methodSecondStep')}
                    </div>
                </div>
                <div className="col-start-8 row-start-2 col-span-5 relative flex items-center justify-center w-full h-64 title-gradient">
                    <div className="absolute top-2/4 left-2/4 w-32 h-32 md:w-48 md:h-48 -translate-x-2/4 -translate-y-2/4 -mt-6">
                        <Image src={TwoImg} alt=""/>
                    </div>
                    <div className="text-white text-center text-sm md:text-3xl font-semibold uppercase">
                        {t('immobilier.methodSecondStepTitle')}
                    </div>
                </div>

                <div className="col-start-1 row-start-3 col-span-5 relative flex items-center justify-center w-full h-64 pt-4 title-gradient">
                    <div className="absolute top-2/4 left-2/4 w-32 h-32 -translate-x-2/4 -translate-y-2/4 -mt-6 md:mt-0 md:w-48 md:h-48">
                        <Image src={ThreeImg} alt=""/>
                    </div>
                    <div className="text-white text-center text-sm md:text-3xl font-semibold uppercase">
                        {t('immobilier.methodThirdStepTitle')}
                    </div>
                </div>
                <div className="col-start-8 row-start-3 col-span-5 flex items-center justify-start w-full h-72 md:justify-center">
                    <div className="text-gray-400 font-extralight text-sm md:text-xl md:w-4/6">
                        {t('immobilier.methodThirdStep')}
                    </div>
                </div>

                <div className="col-start-1 row-start-4 col-span-5 flex items-center justify-end w-full h-72 pt-6 md:justify-center">
                    <div className="text-gray-400 font-extralight text-right text-sm md:text-xl md:w-4/6">
                        {t('immobilier.methodFourthStep')}
                    </div>
                </div>
                <div className="col-start-8 row-start-4 col-span-5 relative flex items-center justify-center w-full h-72 pt-6 title-gradient">
                    <div className="absolute top-2/4 left-2/4 w-32 h-32 -translate-x-2/4 -translate-y-2/4 -mt-6 md:w-48 md:h-48">
                        <Image src={FourImg} alt=""/>
                    </div>
                    <div className="text-white text-center text-sm md:text-3xl font-semibold uppercase">
                        {t('immobilier.methodFourthStepTitle')}
                    </div>
                </div>
            </div>
        </section>
    );
};
export default InvestWithExperts;
