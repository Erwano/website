import { useTranslation } from "react-i18next";
import Image from "next/image";
import styles from './Opportunities.module.scss';
import Opportunity1 from 'assets/Immobilier/opportunity1.jpg'
import Opportunity2 from 'assets/Immobilier/opportunity2.jpg'
import Opportunity3 from 'assets/Immobilier/opportunity3.jpg'
import { PopupButton } from "@typeform/embed-react";

const Hero = () => {
    const { t } = useTranslation();
    return (
        <section className="w-full py-20 flex flex-col items-center backgroundImg bg-top bg-contain bg-no-repeat overflow-hidden">
            <div className={`${styles['opportunities-background']} absolute block w-full`}></div>
            <h1 className="main-title">
                <span className="font-light">{t('immobilier.opportunitiesTitle1')}</span>
                <br/>
                {t('immobilier.opportunitiesTitle2')}
            </h1>
            <div className="my-6 text-center subtitle-text w-4/5 md:w-1/2">
                {t('immobilier.opportunitiesSubtitle')}
            </div>

            <div className="flex justify-center relative h-52 md:h-60 lg:h-96 w-full title-gradient overflow-hidden">
                <div className="absolute -left-[calc(25%+20px)] w-3/6 h-full blur-sm flex items-center justify-center rounded-[52px] overflow-hidden">
                    <Image src={Opportunity1} objectFit="cover" alt="" className="rounded-3xl"/>
                </div>
                <div className="w-3/6 h-full blur-sm flex justify-center rounded-[52px]  overflow-hidden">
                    <Image src={Opportunity2}  alt="" objectFit="cover" className="rounded-3xl flex overflow-hidden"/>
                </div>
                <div className="absolute -right-[calc(25%+20px)] w-3/6 h-full blur-sm flex items-center justify-center rounded-[52px] overflow-hidden">
                    <Image src={Opportunity3} objectFit="cover" alt="" className="rounded-3xl"/>
                </div>
            </div>

            <div className="flex justify-center mt-6 md:t-12">
                {/* <PopupButton id="uWqiIoyH" className="blue-button">
                    S’inscrire à notre liste
                </PopupButton> */}
                <button  className="blue-button">
                    S’inscrire à notre liste
                </button>
            </div>
        </section>
    );
};
export default Hero;
