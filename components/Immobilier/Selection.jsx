import { useTranslation } from "react-i18next"
import Image from "next/image";
import AnalyseImg from '../../assets/Immobilier/analyse.svg';
import ActifImg from "../../assets/Immobilier/actif.svg";
import InvestImg from '../../assets/Immobilier/invest.svg';

export default () => {
    const { t } = useTranslation();
    const selectionCriterias = [
        {
            img: InvestImg,
            title: t('immobilier.selectionRequiring'),
            desc: t('immobilier.selectionRequiringDesc'),
        },
        {
            img: ActifImg,
            title: t('immobilier.selectionTailored'),
            desc: t('immobilier.selectionTailoredDesc'),
        },
        {
            img: AnalyseImg,
            title: t('immobilier.selectionAnalysed'),
            desc: t('immobilier.selectionAnalysedDesc'),
        },
    ];
    return (
        <section className="page-section flex flex-col items-center">
            <div className="title-gradient flex flex-col items-center">
                <h1 className="main-title mt-16 md:mt-10 ">
                    {t('immobilier.selectionTitle1')}
                    <br className="hidden sm:flex"/>
                    <span className="font-light">{" "}{t('immobilier.selectionTitle2')}</span>
                </h1>
                <div className="hidden md:block subtitle-text mt-6 text-center md:w-8/12">
                    {t('immobilier.selectionSubitle')}
                </div>
            </div>
            <div className="flex flex-col md:flex-row mt-4 md:space-x-6 md:mt-20">
                {selectionCriterias.map((value, index) => (
                    <div key={index} className="flex flex-col max-w-xs items-center mt-20 md:mt-0">
                        <div className="md:h-40 w-40">
                            <Image src={value.img} alt=""/>
                        </div>
                        <div className="mt-5 mb-2 md:mb-5 text-l md:text-lg font-medium uppercase text-center text-white md:h-14">
                            {value.title}
                        </div>
                        <p className="subtitle-text text-center w-10/12 md:w-11/12">
                            {value.desc}
                        </p>
                    </div>
                ))}
            </div>
        </section>
    )
}