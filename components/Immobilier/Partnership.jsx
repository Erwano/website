import Image from "next/image";
import { useTranslation } from "react-i18next";
import BlueButton from "components/BlueButton";
import WindowImg from "assets/Immobilier/window.jpg";
import CheckCircleIcon from "../../assets/Immobilier/BlueCheckCircle.svg";
import AMDGImg from "../../assets/Immobilier/partnerAmdg.svg";
import RothschildImg from "../../assets/Immobilier/partnerRothschild.svg";
import MagellanImg from "../../assets/Immobilier/partnerMagellan.svg";
import KyaneosImg from "../../assets/Immobilier/partnerKyaneos.svg";

export default () => {
    const { t } = useTranslation();
    return (
        <>
            <section className="page-section flex-col items-center">
                <div className="flex flex-col space-x-6 md:flex-row">
                    <div className="flex items-center justify-center md:w-2/4">
                        <div className="flex w-5/6 items-center justify-center overflow-hidden rounded-3xl md:h-96">
                            <Image src={WindowImg} layout="intrinsic" alt="" />
                        </div>
                    </div>
                    <div className="flex flex-col items-center md:w-2/4 md:items-start">
                        <h1 className="main-title mt-6 mb-10 md:ml-10 md:text-left">
                            <span className="font-light">
                                {t("immobilier.partnershipTitle1")}
                            </span>
                            <br />
                            {t("immobilier.partnershipTitle2")}
                        </h1>
                        <div className="text-sm md:text-base font-light text-gray-400 md:ml-10">
                            {[1, 2, 3].map((idx) => (
                                <div key={idx} className="mb-4 flex items-center">
                                    <div className="mr-5 h-7 w-7 text-blue-700">
                                        <Image src={CheckCircleIcon} alt="" />
                                    </div>
                                    {t(`immobilier.partnershipFund${idx}`)}
                                </div>
                            ))}
                        </div>
                        <div className="mt-10 flex w-full justify-center">
                            <BlueButton name="En savoir plus" />
                        </div>
                    </div>
                </div>
            </section>

            <section className="page-section md:my-20 flex flex-col items-center">
                <div className="title-gradient flex flex-col items-center">
                    <h1 className="main-title">
                        <span className="font-light">
                            {t("immobilier.partnershipSubtitle1")}
                        </span>{" "}
                        {t("immobilier.partnershipSubtitle2")}
                    </h1>
                    <div className="subtitle-text mt-6 text-center md:w-8/12">
                        {t("immobilier.partnershipSubtitleDesc")}
                    </div>
                </div>

                <div className="mt-10 flex h-64 w-5/6 flex-wrap justify-between lg:justify-evenly lg:h-auto lg:w-full">
                    <div className="flex w-2/5 flex-col items-center justify-center lg:w-auto">
                        <Image src={AMDGImg} alt="AMDG logo" />
                    </div>
                    <div className="flex w-2/5 flex-col items-center justify-center lg:w-auto">
                        <Image src={RothschildImg} alt="Rothschild logo" />
                    </div>
                    <div className="flex w-2/5 flex-col items-center justify-center lg:w-auto">
                        <Image src={MagellanImg} alt="Magellan logo" />
                    </div>
                    <div className="flex w-2/5 flex-col items-center justify-center lg:w-auto">
                        <Image src={KyaneosImg} alt="Kyaneos logo" />
                    </div>
                </div>
            </section>
        </>
    );
};
