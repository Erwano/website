import { usePopperTooltip } from 'react-popper-tooltip';

export default (props) => {
    const {children, trigger, content} = props;
    const {
        getArrowProps,
        getTooltipProps,
        setTooltipRef,
        setTriggerRef,
        visible,
    } = usePopperTooltip({ trigger: trigger || 'hover' });
    
    return (
        <>
            <div ref={setTriggerRef}>
                {children}
            </div>
            {visible && (
                <div
                    ref={setTooltipRef}
                    {...getTooltipProps({ className: 'tooltip-container c-tooltip' })}
                >
                    <div {...getArrowProps({ className: 'tooltip-arrow c-tooltip' })} />
                        {content}
                </div>
            )}   
        </> 
    )
}