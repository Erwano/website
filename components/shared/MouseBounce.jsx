import Image from "next/image";
import Mouse from "../../assets/mouse.svg";

const Mouse_bounce = () => {
    return (
        <div className="flex flex-col items-center space-y-3 md:space-y-0 md:flex-row md:space-x-2 ">
            <p className="font-light text-white">SCROLL</p>
            <div className="animate-bounce hover:cursor-pointer">
                <Image
                    src={Mouse}
                    alt="correspond à une souris ( le périphérique )"
                />
            </div>
        </div>
    );
};
export default Mouse_bounce;
