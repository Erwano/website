import React, { useState } from "react";
  
export default ({ children, readMoreText, readLessText }) => {
  const text = children;
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };

  return (
    <p className="inline w-full">
      {isReadMore ? text.slice(0, 200) : text}
      <span onClick={toggleReadMore} className="read-or-hide text-white ml-2 cursor-pointer">
        {isReadMore ? `...${readMoreText}` : ` ${readLessText}`}
      </span>
    </p>
  );
};