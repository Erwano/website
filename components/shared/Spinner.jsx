import Image from 'next/image';
import SpinnerSvg from 'assets/spinner.svg'

export default ({className}) => (
    <div className={className}>
        <Image src={SpinnerSvg} alt=""/>
    </div>
);