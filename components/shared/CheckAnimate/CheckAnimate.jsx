import s from "./CheckAnimate.module.scss";

const CheckAnimate = () => {
    return (
        <div className={`${s["success-checkmark"]}`}>
            <div className={`${s["check-icon"]}`}>
                <span className={`${s["icon-line"]} ${s["line-tip"]}`}></span>
                <span className={`${s["icon-line"]} ${s["line-long"]}`}></span>
                <div className={`${s["icon-circle"]}`}></div>
                <div className={`${s["icon-fix"]}`}></div>
            </div>
        </div>
    );
};
export default CheckAnimate;
