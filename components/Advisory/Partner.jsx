import Image from "next/image";
import BC from "../../assets/Advizory/b&c.png";
import Dla from "../../assets/Advizory/dlaPiper.png";
import Urban from "../../assets/Advizory/urban.png";
import Bold from "../../assets/Advizory/bold.png";
import BlueButton from "../BlueButton";

const Partner = () => {
    const images = [
        {
            img: BC,
            alt: "B&C NOTAIRES",
        },
        {
            img: Dla,
            alt: "DLA Piper",
        },
        {
            img: Urban,
            alt: "Urban Act Avocats",
        },
        {
            img: Bold,
            alt: "BOLD Avocats",
        },
    ];

    return (
        <section className="page-section bg-top px-4 md:px-0 pb-0 relative flex flex-col items-center">
            <h1 className="main-title mb-10">
                <span className="font-light">Des partenariats privilégiés</span>
                <br />
                avec des spécialistes dans leur domaine
            </h1>
            <div className=" flex flex-wrap justify-around w-full">
                {images.map((image, idx) => (
                    <div
                        key={idx}
                        className="mb-5 flex w-1/2 flex-col items-center justify-center lg:mb-0 lg:w-1/4"
                    >
                        <Image src={image.img} alt={image.alt} quality={100} />
                    </div>
                ))}
            </div>
            <div className="backgroundImg mt-20 pt-12">
                <h1 className="main-title px-8 md:px-0">
                    <span className="font-light">
                        Vous souhaitez optimiser 
                    </span>
                    {" "}votre patrimoine ?
                </h1>
                <div className="pt-10 pb-52 flex justify-center">
                    <BlueButton name="Rencontrez nos experts" />
                </div>
            </div>
        </section>
    );
};
export default Partner;
