import CardExpertise from "./CardExpertise";
import { useKeenSlider } from "keen-slider/react";
import { useState } from "react";
import CardArrow from "../Marches-Financier/CardArrow";
import RomainD from "assets/RomainD.jpeg";
import Elisabeth from "assets/elisabeth.jpg";
import Lorenz from "assets/lorenz.jpg"

const autoLoop = (slider) => {
    let timeout;
    let mouseOver = false;
    function clearNextTimeout() {
        clearTimeout(timeout);
    }
    function nextTimeout() {
        clearTimeout(timeout);
        if (mouseOver) return;
        timeout = setTimeout(() => {
            slider.next();
        }, 4000);
    }
    slider.on("created", () => {
        slider.container.addEventListener("mouseover", () => {
            mouseOver = true;
            clearNextTimeout();
        });
        slider.container.addEventListener("mouseout", () => {
            mouseOver = false;
            nextTimeout();
        });
        nextTimeout();
    });
    slider.on("dragStarted", clearNextTimeout);
    slider.on("animationEnded", nextTimeout);
    slider.on("updated", nextTimeout);
};

const Expertise = () => {
    const [currentSlide, setCurrentSlide] = useState(1);
    const [loaded, setLoaded] = useState(false);
    const [sliderRef, instanceRef] = useKeenSlider(
        {
            mode: "free-snap",
            slides: {
                origin: "center",
                perView: 1.2,
                spacing: 12,
                size: 0.9,
            },
            initial: 1,
            loop: true,
            breakpoints: {
                "(min-width: 768px)": {
                    slides: {
                        origin: "center",
                        perView: 2,
                        spacing: 28,
                        size: 0.8,
                    },
                },
            },
            created() {
                setLoaded(true);
            },
            slideChanged(slider) {
                setCurrentSlide(slider.track.details.rel);
            },
        },
        [autoLoop]
    );
    const card = [
        {
            name: "Romain D.",
            age: "31 ans",
            post: "Trader - FX and Interest Rates Derivatives",
            enjeu: "Investir en immobilier à crédit malgré une fiscalité élevée.",
            offer: "Colbr Black",
            conseil: "Création d’une SCI à l’impôt sur les sociétés pour optimiser la fiscalité liée au placement.",
            img : RomainD,
            blur: false
        },
        {
            name: "Lorenz V.",
            age: "36 ans",
            post: "Directeur commercial d’une Scale Up",
            enjeu: "Optimisation de la cession de mes actions gratuites reçues il y a 7 ans suite à une forte plus-value latente. ",
            offer: "Colbr Family Office",
            conseil: "Mise en place d’une structuration dite d’apport cession permettant d’optimiser la fiscalité à court terme tout en investissant les fruits de cette cession pour des projets futurs.",
            img: Lorenz,
            blur: true
        },
        {
            name: "Élisabeth B.",
            age: "45 ans",
            post: "Avocate d’affaires",
            enjeu: "Préparer sa retraite et anticiper la transmission de son patrimoine.",
            offer: "Colbr Family Office",
            conseil: "Création d’une SARL de famille, investissement dans un fonds immobilier en loueur meublé pour limiter la fiscalité du placement, puis donation de la nue-propriété de la société aux enfants.",
            img: Elisabeth,
            blur: true
        },
    ];
    return (
        <section className="page-section flex flex-col items-center justify-center">
            <h1 className="main-title">
                Notre savoir-faire
                <span className="font-light"> vu par nos membres</span>
            </h1>
            {/* contain card , add futur slider*/}
            <div className="relative my-10 w-full">
                <div ref={sliderRef} className="keen-slider">
                    {card.map((card, idx) => (
                        <CardExpertise {...card} key={idx} />
                    ))}
                </div>
                {loaded && instanceRef.current && (
                    <>
                        <CardArrow
                            left
                            onClick={(e) =>
                                e.stopPropagation() ||
                                instanceRef.current?.prev()
                            }
                        />

                        <CardArrow
                            onClick={(e) =>
                                e.stopPropagation() ||
                                instanceRef.current?.next()
                            }
                        />
                    </>
                )}
            </div>
        </section>
    );
};
export default Expertise;
