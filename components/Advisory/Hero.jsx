import Image from "next/image";
import heroImg from "../../assets/Advizory/hero.png";
import BlueButton from "../BlueButton";
import Icon from "../../assets/Advizory/icon.png";
import interogation from "assets/Advizory/interogation.svg"
import s from './hero.module.scss'
import Mouse_bounce from "components/shared/MouseBounce";

const Hero = () => {
    return (
        <section className={`${s.backgroundImgDesk} page-section hero grid grid-cols-12 md:grid-rows-1`}>
            <div className="hidden md:flex hero-image-block-wrapper">
                <div className="hero-image-block">
                    <div className="hero-icon top-28 -left-8">
                        <Image
                            src={Icon}
                            alt="le conseil"
                        />
                    </div>
                    <Image src={heroImg} alt="un conseiller" className="rounded-3xl"/>
                    <div className="hero-text-block hero-text-block-no-icon bottom-16 -right-12">
                        <div className="w-5 h-5 md:w-6 md:h-6 mr-2">
                            <Image src={interogation} alt=""/>
                        </div>
                        Posez-nous vos questions
                    </div>
                </div>
            </div>
            
            <div className="hero-text-block-wrapper">
                <h1 className="main-title hero">
                    Advisory
                </h1>
                <blockquote className="hero-text-subtitle">
                    &ldquo;Banking is necessary,
                    <br /> Banks are not&rdquo;
                </blockquote>
                <p className="hero-text-author">Bill Gates</p>
                <BlueButton name="Je démarre mon projet"/>
            </div>
            <div className="hidden lg:block absolute bottom-10 left-20 2xl:left-0">
                <Mouse_bounce/>
            </div>
        </section>
    );
};
export default Hero;
