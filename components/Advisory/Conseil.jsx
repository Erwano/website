import Image from "next/image";
import Expert from "../../assets/Advizory/imgConseil.png";
import ecoSystem from "../../assets/Advizory/ecosysteme.png";
import BlueButton from "components/BlueButton";

const Conseil = () => {
    return (
        <section className="page-section hero">
            <h1 className="main-title">
                <span className="font-light">
                    Nous conseillons les grandes fortunes d’aujourd’hui
                </span>
                <br className="hidden sm:block"/>
                {" "} et celles qui se construisent
            </h1>

            <div className="my-20 grid grid-cols-12 md:grid-flow-col">
                <div className="col-span-12 flex items-center justify-center md:col-span-5 md:col-start-7 md:justify-start">
                    <div className="relative flex w-11/12 max-w-lg items-center justify-center">
                        <Image src={Expert} width={635} height={377} alt=""/>
                    </div>
                </div>
                <div className="col-span-12 flex items-center justify-center md:col-span-5 md:col-start-1 md:justify-end">
                    <div className="w-11/12 max-w-md">
                        <h2 className="mt-5 mb-2 text-white md:flex md:flex-col md:text-right md:text-2xl">
                            Un accès direct à une{" "}
                            <span>équipe d’experts</span>{" "}
                        </h2>
                        <p className="text-gray-400 font-light md:p-0 md:text-right">
                            Grâce à une technologie performante et un accès à
                            des vrais professionnels de l’investissement, nous
                            vous proposons une expérience unique de conseil
                            sur-mesure.
                        </p>
                        <div className="mt-5 flex w-full justify-start md:justify-end">
                            <BlueButton name="Rencontrez-nous" />
                        </div>
                    </div>
                </div>
            </div>

            <div className="mt-10 grid grid-cols-12">
                <div className="col-span-12 flex items-center justify-center md:col-span-6 md:justify-end">
                    <div className="relative flex w-11/12 max-w-lg items-center justify-center">
                        <Image src={ecoSystem} width={635} height={377} alt=""/>
                    </div>
                </div>
                <div className="col-span-12 flex items-center justify-center md:col-span-6 md:col-start-8 md:justify-start">
                    <div className="w-11/12 max-w-md">
                        <h1 className="mt-5 mb-2 text-white md:flex md:flex-col md:text-left md:text-2xl">
                            Un écosystème adapté à{" "}
                            <span>toutes vos problématiques</span>
                        </h1>
                        <p className="text-gray-400 font-light md:text-left">
                            En tant que membre, Colbr vous ouvre la porte à son
                            réseau exclusif de spécialistes : avocats, notaires,
                            avocats fiscalistes, banquiers.
                        </p>
                        <div className="mt-5 flex w-full justify-start">
                            <BlueButton name="Démarrer mon projet" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Conseil;
