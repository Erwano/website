import Image from "next/image";
import Head from "../../assets/Advizory/head.png";

const CardExpertise = (props) => {
    const { name, age, post, enjeu, conseil, img, offer, blur } = props;

    return (
        <div className="keen-slider__slide backgroundCardManagers flex max-w-xl rounded-2xl p-4">
            <div className="flex  flex-col  lg:w-full">
                <div className="mb-5 flex items-center">
                    <div className="mr-5 flex h-16 w-16 items-center overflow-hidden rounded-full md:h-20 md:w-20 lg:mr-8 lg:mb-3">
                        <Image
                            src={img ? img : Head}
                            alt="Client colbr"
                            objectPosition="center"
                            className={blur ? "blur-sm" : "opacity-1"}
                        />
                    </div>
                    <div className="flex flex-col">
                        <h6 className="flex items-center text-white lg:text-2xl">
                            {name}
                        </h6>
                        <span className="text-xs lg:text-sm text-gray-500">
                            Membre {offer}
                        </span>
                        <p className="text-xs text-gray-500 lg:mt-0 lg:text-sm">
                            {age} {post}
                        </p>
                    </div>
                </div>

                <div className="text-sm text-gray-400 md:block">
                    <div>
                        <h6 className="text-white lg:text-lg">ENJEU</h6>
                        <p className="text-xs text-gray-500 lg:text-sm">
                            {enjeu}
                        </p>
                    </div>
                    <div className="mt-5">
                        <h6 className="text-white lg:text-lg">CONSEIL</h6>
                        <p className="text-xs text-gray-500 lg:text-sm">
                            {conseil}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default CardExpertise;
