import Image from "next/image";
import Head from "assets/Advizory/head.png";

const CardComment = (props) => {
    const { name, age, post, comment, offer, img } = props;

    return (
        <div className="keen-slider__slide backgroundCardManagers flex rounded-2xl md:rounded-[40px] p-5 md:p-8 max-w-xl">
            <div className="flex flex-1 flex-col justify-center lg:w-full">
                <div className="flex flex-col items-center md:flex-row mb-5">
                    <div className="flex items-center mb-5 md:mr-5 h-16 w-16 overflow-hidden rounded-full md:h-20 md:w-20 lg:mr-8 lg:mb-3">
                        <Image src={img} alt="Client colbr" />
                    </div>
                    <div className="flex flex-col items-center md:items-start">
                        <h6 className="text-white text-center md:text-left lg:text-2xl">
                            {name}
                            <br className="" />
                            <span className="text-sm text-gray-500">
                                {age} - Membre {offer}
                            </span>
                        </h6>
                        <p className="text-xs text-center md:text-left  text-gray-500 lg:mt-0 lg:text-sm">
                            {post}
                        </p>
                    </div>
                </div>

                <div className="text-sm text-gray-400 md:block md:text-base text-justify">
                &ldquo; {comment} &rdquo;
                </div>
            </div>
        </div>
    );
};
export default CardComment;
