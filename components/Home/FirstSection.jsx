import Image from "next/image";
import { motion } from "framer-motion";
import { useTranslation } from "react-i18next";
import { FaLinkedinIn } from "react-icons/fa";
import { BsInstagram } from "react-icons/bs";
import Mobile from "../../assets/colbrphone2.png";
import Mouse from "../../assets/mouse.svg";
import Cloud from "../../assets/cloud2.svg";
import BlueButton from "components/BlueButton";
import Spiral from "assets/spiral.png";

const FirstSection = () => {
    const { t } = useTranslation([]);
    return (
        <section
            className={`page-section hero relative mx-auto flex max-w-7xl items-center justify-center pt-10
                sm:mx-7
                md:mt-0
                lg:mx-0
                2xl:mx-auto
            `}
        >
            <div className="absolute z-0 flex w-full md:-left-48 lg:-left-36">
                <Image src={Cloud} alt="nuage bleu" width={928} height={854} />
            </div>
            <div
                className="
                    z-10 flex flex-col-reverse
                    md:grid md:grid-cols-2 md:grid-rows-1
                    lg:grid-cols-7 lg:grid-rows-4
                "
            >
                <div
                    className="
                    mx-auto hidden w-1/2 
                    sm:col-span-1 sm:col-start-2 sm:row-span-1 sm:mx-0 sm:w-full sm:justify-center md:flex
                    md:justify-end
                    lg:col-span-2 lg:col-start-5 lg:row-span-4
                    "
                >
                    <motion.div
                        initial="start"
                        animate="end"
                        variants={{
                            start: {
                                scale: 0.6,
                            },
                            end: {
                                scale: 1,
                            },
                        }}
                        transition={{
                            delay: 0,
                            type: "spring",
                            bounce: 0,
                            duration: 1.5,
                        }}
                        className="xl:11/12 overflow-hidden sm:w-1/2 lg:w-3/4 2xl:w-full"
                    >
                        <Image
                            src={Mobile}
                            quality="100"
                            height={2543}
                            width={1243}
                            layout="intrinsic"
                            priority
                            alt="téléphone montrant un graphique appartenant à un exemple de compte client Colbr."
                            className="h-full w-full"
                        />
                    </motion.div>
                </div>
                <div
                    className="
                        relative
                          sm:col-span-1 sm:col-start-1 sm:row-span-1 sm:row-start-1 sm:flex sm:flex-col
                         sm:items-center 
                         md:justify-center lg:col-span-4 lg:col-start-1 lg:row-span-3 lg:row-start-1
                    "
                >
                    <div className="">
                        <div
                            className="
                            mt-4 flex h-auto flex-col items-center justify-center
                            sm:items-center
                            md:items-end
                            lg:mt-0 lg:flex-none
                        "
                        >
                            <h1
                                className="
                                line1
                                z-10 max-w-max text-2xl font-light text-white
                                sm:text-3xl
                                lg:text-4xl
                            "
                            >
                                {t("landing.privateNeobank")}
                            </h1>
                            <h1
                                className="
                                line2
                                z-10  flex max-w-max items-center py-1 text-2xl font-bold text-[#C29771]
                                sm:text-3xl
                                lg:text-4xl
                            "
                            >
                                au service de votre ambition.
                            </h1>
                        </div>
                        <div
                            className="
                            z-10 mt-4
                            flex justify-center
                            sm:justify-center
                            md:justify-end
                        "
                        >
                            <p
                                className="z-10 w-4/5 text-center font-light text-gray-400
                                md:w-full md:text-right md:text-xl
                            "
                            >
                                Accédez enfin à l’univers d’investissement des grandes fortunes.
                            </p>
                        </div>
                        <div
                            className="
                            z-10
                            my-4 flex justify-center
                            md:justify-end
                        "
                        >
                            <BlueButton />
                        </div>
                    </div>
                    <div className="absolute -top-28 -z-10 md:hidden">
                        <Image src={Spiral} alt="" />
                    </div>
                </div>

                <div
                    className="
                        flex justify-center
                        sm:col-span-1 sm:col-start-1 sm:row-span-1 sm:row-start-2 sm:items-center sm:justify-center
                        md:justify-start
                        lg:row-start-4 lg:items-end
                    "
                >
                    <div
                        className="
                            mt-10
                            hidden flex-col items-center
                            sm:h-auto sm:flex-row sm:items-center  md:flex
                        "
                    >
                        <p className="font-extralight text-white sm:mr-2 sm:align-middle sm:font-normal">
                            SCROLL{" "}
                        </p>
                        <div className=" mt-4 animate-bounce hover:cursor-pointer sm:mt-0">
                            <Image
                                src={Mouse}
                                alt="correspond à une souris ( le périphérique )"
                            />
                        </div>
                    </div>
                </div>
                <div
                    className="
                        hidden 
                        md:col-span-1 md:col-start-2 md:row-start-2 md:flex md:items-end md:justify-end
                        lg:col-start-7 lg:row-start-4 
                    "
                >
                    <div
                        className="
                            flex h-1/2 max-h-16 flex-col justify-center space-x-3
                            sm:mt-10 sm:w-1/2 sm:flex-row sm:items-center
                            lg:w-10 lg:flex-col lg:justify-between lg:space-x-0
                        "
                    >
                        <a
                            href="https://www.instagram.com/colbr.co/"
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            <BsInstagram
                                style={{
                                    color: "white",
                                    height: "20px",
                                    width: "20px",
                                }}
                            />
                        </a>
                        <a
                            href="https://www.linkedin.com/company/colbr/"
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            <FaLinkedinIn
                                style={{
                                    color: "white",
                                    height: "20px",
                                    width: "20px",
                                }}
                            />
                        </a>
                    </div>
                </div>
            </div>
            <div className="hidden"></div>
        </section>
    );
};

export default FirstSection;
