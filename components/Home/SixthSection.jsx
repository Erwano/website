import InvestConseil from "../../assets/investissement-conseil-logo.svg";
import LesEchos from "../../assets/Les_echos_logo.png";
import Actif from "../../assets/actif-logo-white.svg";
import NewsManagers from "../../assets/news-manager-logo.svg";
import Image from "next/image";

const SixthSection = () => {
    return (
        <section className="page-section flex flex-col items-center pt-0">
            <h1 className="main-title mb-10">
                La presse<span className="font-light"> en parle </span>{" "}
            </h1>
            <div className="flex h-64 w-5/6 flex-wrap justify-center lg:h-auto lg:w-3/4 2xl:w-3/5">
                <div className="flex w-1/2 flex-col items-center justify-center lg:w-1/4">
                    <Image
                        src={InvestConseil}
                        alt="entreprise Investissement Conseils"
                        quality={100}
                    />
                </div>
                <div className="flex w-1/2 flex-col items-center justify-center lg:w-1/4">
                    <div className="w-3/5">
                        <Image
                            src={LesEchos}
                            alt="entreprise Les Echos"
                            quality={100}
                        />
                    </div>
                </div>
                <div className="flex w-1/2 flex-col items-center justify-center lg:w-1/4">
                    <Image
                        src={Actif}
                        alt="entreprise l'Agefi Actifs 20 ans"
                        quality={100}
                    />
                </div>
                <div className="flex w-1/2 flex-col items-center justify-center lg:w-1/4">
                    <Image
                        src={NewsManagers}
                        alt="entreprise News Managers"
                        quality={100}
                    />
                </div>
            </div>
        </section>
    );
};

export default SixthSection;
