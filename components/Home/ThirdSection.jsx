import Image from "next/image";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import RealstateImg from "assets/immobilier0.png";
import FinancialMarketImg from "assets/financialMarket0.png";
import PatrimonialEngineeringImg from "assets/PatrimonialEngineering.svg";
import HandImg from "assets/hand0.png";
import Link from "next/link";

const goToRight = {
    offscreen: {
        x: -200,
        opacity: 0,
    },
    onscreen: {
        x: 0,
        opacity: 1,
    },
};

const solutionsCards = [
    {
        placement: "col-span-1 row-start-1",
        title: "marchés financiers",
        desc: "Des portefeuilles personnalisés avec les fonds les plus performants du marché.",
        linkRoute: "/marches-financiers",
        image: FinancialMarketImg,
    },
    {
        placement: "col-span-1 row-start-2 md:row-start-1",
        title: "immobilier",
        desc: "De l’immobilier de rendement (SCPI, OPCI) aux stratégies de marchand de biens.",
        linkRoute: "/immobilier",
        image: RealstateImg,
    },
    {
        placement: "col-span-1 row-start-3 md:row-start-2",
        title: "private equity",
        desc: "Le meilleur de l'environnement non coté enfin à votre portée.",
        linkRoute: "/private-equity",
        image: HandImg,
    },
    {
        placement: "col-span-1 row-start-4 md:row-start-2",
        title: "advisory",
        desc: "Des experts pour répondre à chaque problématique patrimoniale.",
        linkRoute: "/advisory",
        image: PatrimonialEngineeringImg,
    },
];

const ThirdSection = () => {
    const router = useRouter();

    const redirect = (url) => {
        router.push(url);
    };

    return (
        <section className="page-section hero flex flex-col items-center">
            <h1 className="main-title my-10 md:my-0 md:mt-10 text-center">
                Colbr vous donne accès au meilleur
                <br className="hidden md:flex" />
                <span className="font-light">
                    {" "}  du conseil et de l’investissement
                </span>
            </h1>

            <div
                className="
                    my-5 w-full max-w-4xl grid grid-cols-1 grid-rows-4 sm:grid-cols-2 sm:grid-rows-2 gap-4 md:gap-6 place-items-center"
            >
                {solutionsCards.map((card, idx) => (
                    <motion.div
                        key={idx}
                        initial="offscreen"
                        whileInView="onscreen"
                        viewport={{ once: true }}
                        variants={{
                            offscreen: {
                                x: -200,
                                opacity: 0,
                            },
                            onscreen: {
                                x: 0,
                                opacity: 1,
                            },
                        }}
                        transition={{
                            delay: 0.1,
                            type: "spring",
                            bounce: 0,
                            duration: 1,
                        }}
                        className={` backgroundCard flex flex-col items-center justify-evenly rounded-xl w-full max-w-xs sm:max-w-md p-2 py-6`}
                    >
                        <div className="w-16 flex md:w-28">
                            <Image
                                src={card.image}
                                layout="intrinsic"
                                quality="75"
                                alt=""
                            />
                        </div>
                        <h3 className="my-4 h-10 md:h-auto flex items-center justify-center text-center text-base md:text-2xl uppercase tracking-[0.1em] text-white md:w-full">
                            {card.title}
                        </h3>
                        <p className="hidden md:flex mt-4 w-4/6 text-center text-gray-400 md:w-4/5 lg:w-5/6">
                            {card.desc}
                        </p>
                        <Link href={card.linkRoute}>
                            <a
                                className="
                                    rounded-full border border-blue-900  py-2 px-8 text-white hover:bg-blue-900
                                    md:mt-10
                                "
                            >
                                En savoir +
                            </a>
                        </Link>
                    </motion.div>
                ))}
            </div>
        </section>
    );
};

export default ThirdSection;
