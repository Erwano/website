import Image from "next/image";
import Desk from "../../assets/bureau.png";
import Profile from "../../assets/profil.png";
import Work from "../../assets/iconwork.png";
import { Parallax } from "react-scroll-parallax";
import Cloud from "../../assets/cloud2.svg";
import CheckCircleIcon from "../../assets/Immobilier/BlueCheckCircle.svg";
import BlueButton from "components/BlueButton";
import s from "./secondSection.module.scss"

const SecondSection = () => {
    const data = [
        "Un accès à des vrais professionnels de l’investissement",
        "Une sélection de produits d’investissement haut de gamme",
        "Un service client disponible 6 jours/7 de 8h à 21h",
        "Un suivi en temps réel de votre patrimoine",
    ];

    return (
        <section
            className={`
                ${s.backgroundImgDesk}
                relative page-section hero p-0 
                mt-20 md:mt-10 w-full max-w-7xl flex flex-col justify-center items-center
                lg:flex lg:flex-row lg:px-5
            `}
        >
            <div
                className="
                    hidden
                    sm:w1/2 relative mb-6 md:flex w-full flex-col items-center
                    justify-center
                    lg:w-1/2
                    xl:w-2/5
                "
            >
                <div className="relative w-2/3 sm:w-1/2 md:w-2/5 lg:w-2/3 xl:w-3/4">
                    <div
                        className="
                            rounded-3xl overflow-hidden flex
                            md:justify-center relative
                        "
                    >
                        <Image
                            objectFit="cover"
                            quality={100}
                            layout="intrinsic"
                            height={720}
                            width={481}
                            src={Desk}
                            alt="un client qui consulte le site Colbr"
                        />
                    </div>
                    <Parallax
                        className="
                            absolute top-0 -right-12 w-24
                            md:w-28
                            lg:w-28
                        "
                        y={["-50px", "200px"]}
                    >
                        <Image src={Profile} alt="un client de profile" />
                    </Parallax>
                    <Parallax
                        className="
                            absolute bottom-0 -left-10 w-20 rounded-full
                            md:w-24 
                        "
                        y={["0px", "-400px"]}
                    >
                        <Image
                            src={Work}
                            alt="un client de profile"
                            width={127}
                            height={133}
                        />
                    </Parallax>
                </div>
            </div>
            <div
                className="
                    opacity-100
                    relative z-10 flex flex-col items-center justify-center text-white
                    lg:w-1/2
                "
            >
                <div className="absolute -left-20 -top-48 h-full lg:top-20 lg:-left-28 ">
                    <Image src={Cloud} quality={100} alt="nuage bleu" />
                </div>
                <div className="z-10 flex w-11/12 flex-col items-center lg:items-start xl:ml-28">
                    <h1
                        className="main-title md:text-left lg:w:11/12"
                    >
                        <span className="font-light">
                            Un conseil humain
                        </span>
                        <br/>
                        augmenté par la technologie
                    </h1>
                    <div className="mt-10 space-y-5 text-sm md:text-base lg:w-11/12 lg:text-left">
                        {data.map((detail, index) => (
                            <div className="flex items-center" key={index}>
                                <div className="mr-5 flex-shrink-0">
                                    <Image src={CheckCircleIcon} alt="" />
                                </div>
                                <p>{detail}</p>
                            </div>
                        ))}
                    </div>
                </div>
                <div className="z-10 flex w-11/12 justify-center  lg:justify-start xl:w-3/4">
                        <div className="mt-10 md:mt-20">
                            <BlueButton name="En savoir plus" href="/comment-ca-marche"/> 
                        </div>
                </div>
            </div>
        </section>
    );
};

export default SecondSection;
