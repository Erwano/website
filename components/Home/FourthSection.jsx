import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";

const FourthSection = () => {
    const cards = [
        {
            start: 0,
            end: 22,
            duration: 2,
            title: "M €",
            symbol: "+",
            description: "d’actifs conseillés",
        },
        {
            start: 0,
            end: 5,
            duration: 2,
            title: "M €",
            symbol: "+",
            description: "d’encours financiers",
        },
        {
            start: 0,
            end: 36,
            duration: 2,
            title: "ans",
            symbol: "+",
            description: "de moyenne d’âge\n de nos clients",
        },
        {
            start: 0,
            end: 20,
            duration: 2,
            label: "Jusqu'à",
            title: "%",
            symbol: "+",
            description: "d’objectif de performance\n selon votre profil de risque",
        },
    ];

    return (
        <section className="page-section relative flex flex-col items-center overflow-hidden">
            <h1 className="main-title">
                Colbr est la solution
                <br />
                <span className="font-light">des investisseurs exigeants</span>
            </h1>

            <div
                className={` relative z-10 mt-10 flex flex-wrap justify-center md:mt-16 md:w-full md:justify-evenly`}
                id="numbers"
            >
                {cards.map((card, idx) => (
                    <VisibilitySensor partialVisibility key={idx}>
                        {({ isVisible }) => (
                            <div className={`${idx > 1 && "mt-5 md:mt-0"} mb-5 flex w-1/2 flex-col items-center md:mb-0 md:w-auto`}>
                                <h5 className="-ml-3 text-center text-3xl font-bold text-white">
                                    <span className="text-blue-900">
                                        {card.symbol}
                                    </span>{" "}
                                    <span className="text-xl sm:text-xl">
                                        {card.label
                                            ? `${card.label} ${" "}`
                                            : ""}
                                    </span>
                                    {isVisible && (
                                        <CountUp
                                            start={card.start}
                                            end={card.end}
                                            duration={card.duration}
                                        />
                                    )}{" "}
                                    {card.title}{" "}
                                </h5>
                                <p className="whitespace-pre-line text-center text-xs font-light text-gray-400 md:text-base">
                                    {card.description}
                                </p>
                            </div>
                        )}
                    </VisibilitySensor>
                ))}
            </div>
        </section>
    );
};

export default FourthSection;
