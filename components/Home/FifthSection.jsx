import Image from "next/image";
import CardComment from "./CardComment";
import Thomas from "assets/Thomas.jpeg";
import Philippe from "assets/Philippe.jpeg";
import Mathilde from "assets/Mathilde.jpeg";
import Romain from "assets/RomainD.jpeg";
import Slider from "helpers/Slider";

const FifthSection = () => {
    const card = [
        {
            name: "Thomas G.",
            age: "32 ans",
            post: "Asset Manager immobilier (Paris)",
            comment:
                "Après mes deux premiers investissements immobiliers, je cherchais un partenaire capable de me proposer des solutions visant l'enrichissement de la valeur de mes placements. Colbr m'a ouvert de nouvelles perspectives d'investissement que je ne pensais initialement pas accessibles.",
            offer: "Colbr Black",
            img: Thomas,
        },
        {
            name: "Mathilde D.",
            age: "28 ans",
            post: "Ingénieur d affaires en cyber sécurité (Paris)",
            comment:
                "Assez peu à l'aise avec la finance au départ, j'ai découvert avec Colbr une équipe pédagogue et des solutions complètement nouvelles. Depuis que je suis devenue membres j'ai pu affiner mon projet et commencer à investir en accord avec mes ressources et mes principes.",
            offer: "Colbr Black",
            img: Mathilde,
        },
        {
            name: "Philippe G.",
            age: "31 ans",
            post: "Directeur en financement structurés (Genève)",
            comment:
                "En tant que français résidant en Suisse, je m'interrogeais sur les meilleures solutions disponibles pour valoriser mon épargne. Colbr m'a permis de développer mon patrimoine immobilier et financier tout en appréhendant intelligemment la complexité fiscale liée à ma situation.",
            offer: "Colbr Black",
            img: Philippe,
        },
        {
            name: "Romain D.",
            age: "31 ans",
            post: "Trader - FX and Interest Rates Derivatives",
            comment:
                "Je me suis rapproché de Colbr pour avoir des interlocuteurs expérimentés et un conseil de qualité afin d'optimiser mes investissements financiers et immobiliers. Leur connaissance des marchés et leur expertise en termes de diversification m'ont ouvert les portes des fonds d'investissement de niches indispensables dans le contexte de marché actuel.",
            offer: "Colbr Black",
            img: Romain,
        }
    ];

    return (
        <section className="page-section flex w-full max-w-none flex-col items-center justify-center px-0">
            <h1 className="main-title">
                {" "}
                <span className="font-light">Nos membres </span>le disent
            </h1>
            <Slider cardDatas={card} Component={CardComment} loopTime={4000} />
        </section>
    );
};

export default FifthSection;
