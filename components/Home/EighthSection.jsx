import { motion } from "framer-motion";
import Image from "next/image";
import Bank from "assets/bank.svg";
import Fil from "assets/fil.svg";
import Cloud from "assets/cloudY.svg";
import AMF from "assets/AMF.png";
import CNCGP from "assets/CNCGP.png";

const goToRight = {
    offscreen: {
        x: -1000,
    },
    onscreen: {
        x: 0,
    },
};
const goToLeft = {
    offscreen: {
        x: 1000,
    },
    onscreen: {
        x: 0,
    },
};

const EighthSection = () => {
    return (
        <section className="page-section flex flex-col justify-end overflow-hidden">
            <h1 className="main-title flex flex-col md:flex-row md:justify-center">
                <span className="font-light md:mr-2">
                    La confiance d’une banque :
                </span>
                l’expertise en plus
            </h1>
            <div className="mt-12 flex w-full justify-center">
                <Image
                    src={Bank}
                    alt="une banque"
                    quality={100}
                    width={99}
                    height={96}
                />
            </div>
            <div className=" relative my-10 flex h-[650px] flex-col items-center justify-center">
                <motion.div
                    className="flex h-1/4 z-20 w-full flex-col items-start justify-center md:w-4/6"
                    initial="offscreen"
                    whileInView="onscreen"
                    viewport={{ once: true }}
                >
                    <motion.div
                        className="w-2/5"
                        variants={goToRight}
                        transition={{
                            delay: 0.3,
                            type: "spring",
                            bounce: 0.5,
                            duration: 1,
                        }}
                    >
                        <h4 className="flex  flex-col text-right font-bold  text-white md:text-lg">
                            UNE SOCIÉTÉ AGRÉÉE
                            <a
                                href="https://www.orias.fr/home/intermidiaire/892957721"
                                rel="noopener noreferrer"
                                target="_blank"
                                className="my-2 w-full text-right text-xs font-normal text-gray-400 md:text-base"
                            >
                                Immatriculée à l’ORIAS (CIF, IAS, IOBSP) et CCI
                                de Paris (Carte T)
                            </a>
                            <div className="flex flex-col items-end space-x-2 sm:flex-row sm:items-start sm:justify-end sm:space-x-5 md:text-base">
                                <div className="max-w-[80px]">
                                    <Image src={CNCGP} alt="CNCGP" />
                                </div>
                            </div>
                        </h4>
                    </motion.div>
                </motion.div>

                <motion.div
                    className="flex h-1/4 w-full flex-col items-end justify-center md:w-4/6"
                    initial="offscreen"
                    whileInView="onscreen"
                    viewport={{ once: true }}
                >
                    <motion.div
                        className="w-2/5"
                        variants={goToLeft}
                        transition={{
                            delay: 0.3,
                            type: "spring",
                            bounce: 0.5,
                            duration: 1,
                        }}
                    >
                        <h4 className="font-bold uppercase text-white md:text-lg">
                            Une activité réglementée
                        </h4>
                        <p className="mt-2 text-xs font-normal text-gray-400 md:text-base">
                            Supervisée par les autorités financières <br /> (AMF
                            & ACPR)
                        </p>
                        <div className="mt-2 flex flex-col space-x-2 text-sm text-gray-400 sm:flex-row sm:space-x-5 md:text-base">
                            <div className="-ml-1 max-w-[100px]">
                                <Image src={AMF} alt="AMF" />
                            </div>
                        </div>
                    </motion.div>
                </motion.div>

                <div className="absolute left-1/2 h-4/5 shrink-0">
                    <Image
                        src={Fil}
                        alt="une ligne pour afficher les différentes étapes"
                        quality={100}
                        width={12}
                        height={512}
                    />
                </div>
                <div className="-top-25 absolute z-10 md:-top-32">
                    <Image
                        src={Cloud}
                        alt="une ligne pour afficher les différentes étapes"
                        quality={100}
                        width={667}
                        height={1000}
                    />
                </div>

                <motion.div
                    className="flex h-1/4 w-full flex-col items-start justify-center  md:w-4/6"
                    initial="offscreen"
                    whileInView="onscreen"
                    viewport={{ once: true }}
                >
                    <motion.div
                        className="w-2/5"
                        variants={goToRight}
                        transition={{
                            delay: 0.3,
                            type: "spring",
                            bounce: 0.5,
                            duration: 1,
                        }}
                    >
                        <h4 className="flex flex-col text-right font-bold text-white md:text-lg">
                            DES GARANTIES SOLIDES
                        </h4>
                        <div className="mt-2 text-right text-xs font-normal text-gray-400 md:text-base">
                            RCP et garantie financière
                            <br />
                            jusqu’à 2 500 000 euros
                        </div>
                    </motion.div>
                </motion.div>

                <motion.div
                    className="flex h-1/4 w-full flex-col items-end justify-center md:w-4/6"
                    initial="offscreen"
                    whileInView="onscreen"
                    viewport={{ once: true }}
                >
                    <motion.div
                        className="w-2/5"
                        variants={goToLeft}
                        transition={{
                            delay: 0.3,
                            type: "spring",
                            bounce: 0.5,
                            duration: 1,
                        }}
                    >
                        <h4 className="font-bold uppercase text-white md:text-lg">
                            Des données sécurisées
                        </h4>
                        <p className="mt-2 text-xs font-normal text-gray-400 md:text-base">
                            Vos données sont protégées sur nos serveurs
                            sécurisés
                        </p>
                    </motion.div>
                </motion.div>
            </div>
        </section>
    );
};

export default EighthSection;
