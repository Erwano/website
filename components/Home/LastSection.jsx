import Image from "next/image";
import Timer from "../../assets/timer.svg";
import Cloud from "../../assets/cloud2.svg";
import Link from "next/link";
import BlueButton from "components/BlueButton";

const Lastsection = () => {
    return (
        <section className="page-section last relative flex flex-col items-center justify-center overflow-hidden">
            <div className="mb-10">
                <Image quality="85" src={Timer} alt="un chronomètre" />
            </div>

            <div className="right-2/6 absolute max-h-full w-full">
                <Image src={Cloud} alt="" objectFit="cover"/>
            </div>

            <h1 className="z-10 main-title">
                Prenez la bonne décision dès maintenant
                <br/>
                <span className="hidden md:block text-center font-light">
                    {" "} Commencez à investir avec Colbr
                </span>
            </h1>
            <div className="z-10 mt-10">
                <BlueButton name="Je commence avec Colbr"/>
            </div>
        </section>
    );
};

export default Lastsection;
