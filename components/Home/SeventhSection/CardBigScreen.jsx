import React from "react";
import VectorOffre from "assets/VectorOffre.svg";
import Image from "next/image";
import Link from "next/link";
import BlueButton from "components/BlueButton";

const CardBigScreen = ({ props }) => {
    return (
        <div className="backgroundFifth flex w-full max-w-md scale-95 flex-col items-center rounded-3xl p-7 py-14 transition-all hover:scale-105">
            <h1 className="text-center text-2xl text-white">{props.title}</h1>
            <div className="my-4 flex w-full rotate-[-1deg] justify-center">
                <Image src={VectorOffre} alt="" />
            </div>
            <p className="my-2 mb-5 whitespace-pre-line text-center text-gray-400">
                {props.subtitle}
            </p>
            <div className="mb-5 hidden md:block">
                <BlueButton name={props.buttonName} />
            </div>
            <Link href={props.link}>
                <a className="mb-8 text-sm text-white  hidden md:block"> En savoir plus &gt; </a>
            </Link>
            <div className="mb-5 flex w-full items-center justify-center border-b-[0.5px] border-white pb-2 text-xl text-white">
                {props.price}
            </div>
            <div className="hidden w-full space-y-5 p-2 text-white md:block">
                {props &&
                    props.details.map((details, i) => (
                        <div key={i} className="flex w-full items-center">
                            <div className="flex w-1/5 items-center justify-center">
                                <div className="mr-2 flex h-8 w-8 items-center justify-center rounded-full bg-blue-700 text-white">
                                    {details.symbol}
                                </div>
                            </div>
                            <p className="w-4/5 whitespace-pre-line text-sm">
                                {details.text}
                            </p>
                        </div>
                    ))}
            </div>
            <div className="w-full mt-5 flex flex-col items-center md:hidden">
                <div className="mb-5">
                    <BlueButton name={props.buttonName} />
                </div>
                <Link href={props.link}>
                    <a className="text-sm text-white">
                        {" "}
                        En savoir plus &gt;{" "}
                    </a>
                </Link>
            </div>
        </div>
    );
};

export default CardBigScreen;
