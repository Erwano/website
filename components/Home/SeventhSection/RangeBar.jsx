import React from "react";

const RangeBar = (propsRangeBar) => {
    return (
        <div className="mb-5 w-5/6 md:w-4/6 lg:w-1/2 xl:w-1/2 2xl:w-1/3 max-w-[500px]">
            <input
                type="range"
                className="w-full"
                min={0}
                max={100000}
                onChange={(e) => propsRangeBar.handleChangeBudget(e)}
                value={propsRangeBar.budget}
            />

            {/* <div className="flex w-full justify-between ">
                <div className="before:ml-1 before:h-[10px] before:w-[1px] before:bg-white before:absolute before:left-0 before:top-0 relative w-1/4"></div>
                <div className="h-[10px] w-[1px] bg-white"></div>
                <div className="mr-1 h-[10px] w-[1px] bg-white"></div>
                <div className="mr-1 h-[10px] w-[1px] bg-white"></div>
            </div> */}
            <div className="mt-1 flex w-full text-white">
                <span className="relative w-1/4 before:absolute before:left-0 before:-top-3 before:ml-1 before:h-[10px] before:w-[1px] before:bg-white">
                    1000
                </span>
                <span className="relative w-1/4 pl-2 before:absolute before:left-7 before:-top-3 before:ml-1 before:h-[10px] before:w-[1px] before:bg-white sm:pl-4 sm:before:left-9">
                    50 000
                </span>
                <span className="relative w-1/4 text-center pl-1 before:absolute before:right-10 before:-top-3 before:ml-1 before:h-[10px] before:w-[1px] before:bg-white sm:pl-4 sm:before:right-12">
                    500 000
                </span>
                <span className="relative w-1/4 text-right before:absolute before:right-2 before:-top-3 before:ml-1 before:h-[10px] before:w-[1px] before:bg-white">
                    + 1 000 000
                </span>
            </div>
        </div>
    );
};

export default RangeBar;
