import React from "react";
import { motion } from "framer-motion";
import Image from "next/image";
import Validate from "../../../assets/validate-sign.svg";
import ImageValidate from "./ImageValidate";
import { useRouter } from "next/router";

const imgBoxVariants = {
    hidden: {
        x: 500,
        opacity: 0,
    },
    visible: {
        x: 0,
        opacity: 1,
        transition: {
            delay: 0,
            duration: 0.5,
        },
    },
};

const Card = ({ props }) => {
    const router = useRouter()

    const redirect = (url) => {
        router.push(url)
    }

    return (
        <motion.div
            className={`backgroundFifth flex  w-4/5 flex-col items-center overflow-hidden rounded-[50px] max-w-sm lg:hidden`}
            variants={imgBoxVariants}
            initial="hidden"
            animate="visible"
        >
            <div className="flex flex-col  w-full py-5  items-center justify-center rounded-t-[50px] border border-gray-800 ">
                <h1 className="text-white text-xl">{props.title}</h1>
                <p className="text-gray-600 my-2" >Sed ut perspiciatis unde omnis ist</p>
                <button className="py-2 px-5 rounded-full bg-blue-800 text-white" onClick={()=> {redirect(props.link)}}>Démarrer</button>
            </div>

            <div className="flex h-full w-full flex-col items-center space-y-6 py-4 mb-8 text-sm text-white">
                {props &&
                    props.details.map((details, i) => (
                        <div
                            className="flex w-full h-6 items-center justify-center"
                            key={i}
                        >
                            <div className="flex items-center mr-2">
                                <ImageValidate />
                            </div>
                            <p className="w-3/5 sm:w-3/4">{details}</p>
                        </div>
                    ))}
            </div>
        </motion.div>
    );
};

export default Card;
