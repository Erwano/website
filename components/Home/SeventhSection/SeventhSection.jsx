import { useEffect, useState } from "react";
import CardBigScreen from "./CardBigScreen";

const SeventhSection = () => {
    const [index, setIndex] = useState(null);
    const [budget, setBudget] = useState(50000);

    const handleChangeBudget = (e) => {
        setBudget(e.target.value);
    };

    useEffect(() => {
        if (budget < 33000) {
            setIndex(1);
        } else if (budget >= 33000 && budget < 66000) {
            setIndex(2);
        } else if (budget >= 66000) {
            setIndex(3);
        }
    }, [budget]);

    const plus = (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth={2}
        >
            <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M12 4v16m8-8H4"
            />
        </svg>
    );

    const validate = (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth={2}
        >
            <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M5 13l4 4L19 7"
            />
        </svg>
    );

    const ColbrFuture = {
        title: "Colbr Future",
        subtitle: "",
        details: [
            "1,8% de frais de souscripotion",
            "Frais de gestion minimum",
            "Accès à tout l’univers coté",
            "Portefeuille personnalisé",
            "Conseil financier d’expert",
            "Arbitrages gratuits",
        ],
        link: "/Offres/Colbr-Future",
    };

    const ColbrBlack = {
        background: "bg-black",
        backgroundButton: "bg-[#BE901F]",
        colorTextButton: "text-black",
        title: "Colbr Black",
        subtitle:
            "L’offre des néo investisseurs ambitieux",
        buttonName: "Prendre rendez-vous",
        price: "Dès 50 000€",
        details: [
            {
                symbol: plus,
                text: "Conseil sur-mesure",
            },
            {
                symbol: plus,
                text: "Accès à notre offre globale de placements",
            },
            {
                symbol: plus,
                text: "Portefeuilles personnalisés",
            },
            {
                symbol: plus,
                text: "Ingénierie fiscale et patrimoniale",
            },
            {
                symbol: plus,
                text: "Experts disponibles et suivi en temps réel de votre épargne",
            },
        ],
        link: "/offres/colbr-black",
    };

    const ColbrFamilyOffice = {
        background: "bg-[#BE901F]",
        backgroundButton: "bg-black",
        colorTextButton: "text-white",
        title: "Colbr Family Office",
        subtitle: " L’offre des investisseurs aguerris",
        buttonName: "Prendre rendez-vous",
        price: "Dès 500 000€",
        details: [
            {
                symbol: validate,
                text: 'Tous les avantages de Colbr Black',
            },
            {
                symbol: plus,
                text: "Binôme d'experts",
            },
            {
                symbol: plus,
                text: "Stratégie d'allocation individualisée",
            },
            {
                symbol: plus,
                text: "Accès à des produits dédiés",
            },
            {
                symbol: plus,
                text: "Gouvernance familiale, analyse patrimoniale et étude fiscale approfondie",
            },
        ],
        link: "/offres/colbr-family-office",
    };

    return (
        <section className="page-section flex flex-col items-center">
            <h1
                className="main-title flex flex-col"
            >
                <span className="font-light">Une offre qui vous accompagne</span>
                à chaque étape
            </h1>

            <p className="my-5 w-4/5 subtitle-text text-center md:w-3/4">
                Un service destiné aux grandes fortunes et à celles qui se construisent. Dès vos débuts, Colbr vous aide à définir la bonne structure et la bonne stratégie pour atteindre vos objectifs.
            </p>
            <div className="mt-10 flex w-11/12 max-w-6xl flex-col md:flex-row  items-center justify-center  md:justify-around space-y-5 md:space-y-0 md:space-x-5">
                <CardBigScreen props={ColbrBlack} />
                <CardBigScreen props={ColbrFamilyOffice} />
            </div>
        </section>
    );
};

export default SeventhSection;
