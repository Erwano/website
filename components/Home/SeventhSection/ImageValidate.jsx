import React from "react";
import Image from "next/image";
import Validate from "../../../assets/validate-sign.svg";

const ImageValidate = (props) => {
    const {margin} = props

    const style = margin ? `mr-3` : ''
    return (
        <div className={`flex items-center min-w-[24px] min-h-[24px] ${style} `}>
            <Image src={Validate} height={24} width={24} alt="signe validé" />
        </div>
    );
};

export default ImageValidate;
