import Marquee from "react-fast-marquee";

const NinethSection = () => {
    return (
        <section className="w-full pt-10 pb-20 flex flex-col items-center">
            <h1 className="main-title mb-10 md:mb-20">
                <span className="font-light">Des partenariats avec</span>
                <br />
                les meilleurs gérants mondiaux
            </h1>

            <Marquee
                gradient={false}
                className="h-12 cursor-pointer text-4xl font-bold uppercase tracking-wider text-gray-800 md:h-20 md:text-6xl"
            >
                <span className="mr-4 hover:text-blue-800">JP Morgan</span>{" "}
                <span className="mr-4 hover:text-blue-800">
                    Rothschild & Co
                </span>{" "}
                <span className="mr-4 hover:text-blue-800">Moneta</span>{" "}
                <span className="mr-4 hover:text-blue-800">Eleva</span>{" "}
                <span className="mr-4 hover:text-blue-800">Morgan Stanley</span>{" "}
                <span className="mr-4 hover:text-blue-800">Comgest</span>{" "}
                <span className="mr-4 hover:text-blue-800">M&G</span>{" "}
            </Marquee>
            <Marquee
                gradient={false}
                direction={"right"}
                className="h-12 cursor-pointer text-4xl font-bold uppercase tracking-wider text-gray-800 md:h-20 md:text-6xl"
            >
                <span className="mr-4 hover:text-blue-800">Blackrock</span>{" "}
                <span className="mr-4 hover:text-blue-800">AMDG</span>{" "}
                <span className="mr-4 hover:text-blue-800">Blackstone</span>{" "}
                <span className="mr-4 hover:text-blue-800">
                    Meanings Capital Partners
                </span>{" "}
                <span className="mr-4 hover:text-blue-800">Ardian</span>{" "}
                <span className="mr-4 hover:text-blue-800">Blackrock</span>{" "}
                <span className="mr-4 hover:text-blue-800">AMDG</span>{" "}
                <span className="mr-4 hover:text-blue-800">Blackstone</span>{" "}
                <span className="mr-4 hover:text-blue-800">
                    Meanings Capital Partners
                </span>{" "}
                <span className="mr-4 hover:text-blue-800">Ardian</span>{" "}
            </Marquee>
            <Marquee
                gradient={false}
                className="h-12 cursor-pointer text-4xl font-bold uppercase tracking-wider text-gray-800 md:h-20 md:text-6xl"
            >
                <span className="mr-4 hover:text-blue-800">VIE PLUS</span>{" "}
                <span className="mr-4 hover:text-blue-800">
                    Lombard International
                </span>{" "}
                <span className="mr-4 hover:text-blue-800">AXA</span>{" "}
                <span className="mr-4 hover:text-blue-800">Nortia</span>{" "}
                <span className="mr-4 hover:text-blue-800">VIE PLUS</span>{" "}
                <span className="mr-4 hover:text-blue-800">
                    Lombard International
                </span>{" "}
                <span className="mr-4 hover:text-blue-800">AXA</span>{" "}
                <span className="mr-4 hover:text-blue-800">Nortia</span>{" "}
            </Marquee>
        </section>
    );
};

export default NinethSection;
