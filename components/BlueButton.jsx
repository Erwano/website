import Link from "next/link";

const BlueButton = (props) => {
    const { name, href } = props;

    return (
        <Link href={href ? href : "/prendre-rendez-vous"}>
            <a className="blue-button">
                {name ? name : "Je commence à investir"}
            </a>
        </Link>
    );
};

export default BlueButton;
