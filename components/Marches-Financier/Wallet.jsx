import React from "react";
import { useTranslation } from "react-i18next";
import {
    QuestionMarkCircleIcon,
    ExclamationIcon,
} from "@heroicons/react/outline";
import styles from "./Wallet.module.scss";
import Tooltip from "components/shared/Tooltip";

const Wallet = () => {
    const { t } = useTranslation();
    const bars = [
        {
            performance: t("marchesFinanciers.euroFundPerformance"),
            loss: t("marchesFinanciers.euroFundLoss"),
            profil: t("marchesFinanciers.euroFundTitle"),
        },
        {
            performance: t("marchesFinanciers.carefulPerformance"),
            loss: t("marchesFinanciers.carefulLoss"),
            profil: t("marchesFinanciers.carefulTitle"),
        },
        {
            performance: t("marchesFinanciers.balancedPerformance"),
            loss: t("marchesFinanciers.balancedLoss"),
            profil: t("marchesFinanciers.balancedTitle"),
        },
        {
            performance: t("marchesFinanciers.dynamicPerformance"),
            loss: t("marchesFinanciers.dynamicLoss"),
            profil: t("marchesFinanciers.dynamicTitle"),
        },
        {
            performance: t("marchesFinanciers.agressivePerformance"),
            loss: t("marchesFinanciers.agressiveLoss"),
            profil: t("marchesFinanciers.agressiveTitle"),
        },
    ];
    return (
        <section className="page-section flex flex-col items-center justify-center md:px-10">
            <h1 className="main-title mt-10 mb-16">Constituez votre portefeuille personnalisé et {" "} <br className="hidden sm:flex"/> <span className="font-light">commencez à investir sur le long terme</span></h1>
            <div
                className={`md:rounded-standard w-full max-w-6xl rounded-3xl p-4 py-12 md:p-12 lg:w-4/5 ${styles["gradient-black"]}`}
            >
                <h1 className="main-title">
                    <span className="font-light">
                        {t("marchesFinanciers.walletTitle1")}
                    </span>
                    <br />
                    {t("marchesFinanciers.walletTitle2")}
                </h1>

                <div className="my-3 text-center text-sm text-gray-500 md:text-base">
                    {t("marchesFinanciers.data5years")}
                </div>

                <div className="relative flex w-full justify-between">
                    <div
                        className="absolute top-10 flex w-52 flex-col justify-end text-xs font-light text-gray-500 sm:relative 
                        sm:top-0 sm:text-lg"
                    >
                        <div className="mb-3 flex items-center text-[#1C4699]">
                            Performances
                            <Tooltip
                                content={t(
                                    "marchesFinanciers.tooltips.graphPerf"
                                )}
                            >
                                <QuestionMarkCircleIcon className="ml-1 h-3 w-3 sm:ml-3 sm:h-4 sm:w-4" />
                            </Tooltip>
                        </div>
                        <div className="flex items-center text-[#C19771]">
                            Pertes max
                            <Tooltip
                                content={t(
                                    "marchesFinanciers.tooltips.graphLoss"
                                )}
                            >
                                <QuestionMarkCircleIcon className="ml-1 h-3 w-3 sm:ml-3 sm:h-4 sm:w-4" />
                            </Tooltip>
                        </div>
                    </div>
                    <div className="flex flex-1 flex-col">
                        <div className="mt-10 flex w-full justify-around sm:w-auto">
                            {bars.map((bar, idx) => {
                                return (
                                    <div
                                        key={idx}
                                        className="w-1/5 flex-col items-center justify-end sm:w-24"
                                    >
                                        <div className="flex h-48 flex-col items-center justify-end text-xs text-white sm:h-56 sm:text-xl">
                                            <p className="text-xs sm:text-lg">
                                                {bar.performance}
                                            </p>
                                            <div
                                                style={{
                                                    "--bar-height": `${bar.performance.match(
                                                        /\d+/
                                                    )}%`,
                                                }}
                                                className={`${styles.bar} ${styles["bar-top"]}`}
                                            ></div>
                                        </div>
                                        <div className="flex flex-col items-center justify-center text-xs text-[#C19771] sm:text-lg">
                                            <p className="mt-2 font-light">
                                                {bar.loss}
                                            </p>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <div className={styles.divider} />
                </div>
                <div className="flex w-full justify-between">
                    <div className="mt-3 hidden w-52 items-center text-white sm:flex">
                        Profils de risques
                    </div>
                    <div className="text-xxs mt-3 flex flex-1 justify-around font-light text-white sm:text-base">
                        {bars.map((bar, idx) => {
                            return (
                                <span
                                    key={idx}
                                    className="w-1/5 min-w-fit text-center sm:w-24"
                                >
                                    {bar.profil}
                                </span>
                            );
                        })}
                    </div>
                </div>
                <div className="mt-8 flex items-center justify-between">
                    <div className="flex items-center justify-center text-xs text-gray-400 sm:text-sm">
                        <Tooltip
                            content={t(
                                "marchesFinanciers.tooltips.graphWarning"
                            )}
                        >
                            <ExclamationIcon className="mr-2 h-4 w-4" />
                        </Tooltip>
                        Avertissements
                    </div>
                    <div className="flex items-center justify-center text-xs text-gray-400 sm:text-sm">
                        Source : Colbr, Bloomberg
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Wallet;
