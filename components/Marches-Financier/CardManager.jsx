import Image from "next/image";
import { useTranslation } from "react-i18next";
import { QuestionMarkCircleIcon, ExclamationIcon } from "@heroicons/react/outline";
import Tooltip from "components/shared/Tooltip";

const CardManager = (props) => {
    const { t } = useTranslation();
    
    return (
        <div className={`keen-slider__slide border rounded-3xl md:rounded-[54px] border-transparent md:border-blue-900 md:p-2 duration-500 transition-opacity
            ${props.isShown ? '' : 'opacity-60'}`}>
            <div className="backgroundCardManagers rounded-3xl md:rounded-[44px] border p-4 md:p-8 text-white h-full">
                <div className="flex flex-col-reverse justify-between px-3 md:px-0 md:flex-row">
                    <div className="text-white w-full mt-4 h-24 flex items-center overflow-hidden md:mt-0 md:w-5/12">
                        <Image src={props.data.img} alt="" objectFit="contain"/>
                    </div>
                    <div className="flex md:mt-3 text-gray-500 items-center h-7">
                        <div className="box-border border-gray-500 grid grid-cols-7 justify-items-stretch rounded-full border w-60">
                            {
                                Array.from(Array(7).keys()).map((nb) => {
                                    return(<div key={nb} className={`${nb + 1 === props.data.srri ? 'bg-blue-700 text-white ' : ''} h-full text-center`}>{ nb+1 }</div>)
                                })
                            }
                        </div>
                        <span className="text-sm mx-2">SRRI</span>
                        <Tooltip content={t('marchesFinanciers.tooltips.srri')}>
                            <QuestionMarkCircleIcon className="h-5 w-5" />
                        </Tooltip>
                    </div>
                </div>
                <h6 className="font-light mt-4 text-sm md:text-lg md:mt-8">
                    {props.data.title}
                </h6>
                <h6 className="mt-4 mb-8 text-sm md:mt-0 md:text-lg md:h-9">
                    {props.data.subtitle}
                </h6>
                <p className="mb-4 text-gray-500 text-sm md:text-base">
                    {props.data.desc}
                </p>
                <div className="my-4 flex justify-between">
                    <div className="flex flex-col justify-center">
                        <h6 className="text-gray-500 text-sm flex items-center font-light md:text-base">
                            {t('marchesFinanciers.performances')}
                            <Tooltip content={t('marchesFinanciers.tooltips.managerPerf')}>
                                <QuestionMarkCircleIcon className="h-3 w-3 ml-1 md:h-4 md:w-4 md:ml-2"/>
                            </Tooltip>
                        </h6>
                        <p className="mt-2 text-xl text-center md:text-left md:text-4xl">{props.data.perf}</p>
                    </div>
                    <div className="flex flex-col items-center justify-center">
                        <h6 className="text-gray-500 text-sm flex items-center font-light md:text-base">
                            {t('marchesFinanciers.managers.volatility')}
                            <Tooltip content={t('marchesFinanciers.tooltips.managerVolatility')}>
                                <QuestionMarkCircleIcon className="h-3 w-3 ml-1 md:h-4 md:w-4 md:ml-2"/>
                            </Tooltip>
                        </h6>
                        <p className="mt-2 text-xl md:text-4xl">{props.data.volatility}</p>
                    </div>
                    <div className="flex flex-col items-center justify-center">
                        <h6 className="text-gray-500 text-sm flex items-center font-light md:text-base">
                            {t('marchesFinanciers.managers.maxLoss')}
                            <Tooltip content={t('marchesFinanciers.tooltips.managerLoss')}>
                                <QuestionMarkCircleIcon className="h-3 w-3 ml-1 md:h-4 md:w-4 md:ml-2"/>
                            </Tooltip>
                        </h6>
                        <p className="mt-2 text-xl md:text-4xl">{props.data.loss}</p>
                    </div>
                </div>
                <div className="flex justify-between text-gray-500 text-xs  font-light md:text-sm">
                    <p>Données sur 5 ans <br className="block md:hidden"/>(2017-2021)</p>
                    <div className="flex items-center">
                        <Tooltip content={t('marchesFinanciers.tooltips.managerWarning')}>
                            <ExclamationIcon className="w-5 h-5 mr-2"/>
                        </Tooltip>
                        <div>Avertissements</div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default CardManager;
