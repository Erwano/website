import Image from "next/image";
import { useTranslation } from "react-i18next";
import Macro from "../../assets/marches-financier/macro-eco.svg";
import Valorisation from "../../assets/marches-financier/valorisation.svg";
import Filter from "../../assets/marches-financier/picto.png";

const Processus = () => {
    const { t } = useTranslation();
    const process = [
        {
            img: Macro,
            title: t("marchesFinanciers.macroTitle"),
            paragraph: t("marchesFinanciers.macroDesc"),
            placeOfGrid: "col-start-4",
            classNumber: " before:content-['1']",
        },
        {
            img: Filter,
            classImg: "max-w-[100px]",
            title: t("marchesFinanciers.valoTitle"),
            paragraph: t("marchesFinanciers.valoDesc"),
            classNumber: "before:content-['2']",
            placeOfGrid: "col-start-6",
        },
        {
            img: Valorisation,
            title: t("marchesFinanciers.filterTitle"),
            paragraph: t("marchesFinanciers.filterDesc"),
            classNumber: "before:content-['3']",
            placeOfGrid: "col-start-8",
        },
    ];
    return (
        <section className="page-section flex flex-col items-center">
            <h1 className="main-title">
                <span className="font-light">Un processus</span>
                <br />
                d’analyse en trois piliers
            </h1>
            <div className="mt-16 flex w-full max-w-6xl flex-col items-center space-y-16 md:flex-row md:justify-between md:space-y-0">
                {process.map((value, index) => (
                    <div
                        className="mt-4 flex max-w-xs flex-col items-center md:mt-0"
                        key={index}
                    >
                        <div
                            className={`flex h-24 w-24 items-center justify-center md:h-32 md:w-32 lg:h-40 lg:w-40 ${
                                value && value.classImg
                            }`}
                        >
                            <Image
                                alt="symbol macro économique"
                                src={value.img}
                            />
                        </div>
                        <h6
                            className={`
                                text-l relative my-5 uppercase text-white before:absolute 
                                before:-left-8 before:top-1/2 before:-ml-4 before:-mt-4 before:flex before:h-8 before:w-8 before:items-center before:justify-center before:rounded-full before:bg-blue-800 md:text-lg ${value.classNumber}
                            `}
                        >
                            {value.title}
                        </h6>
                        <p className="w-9/12 text-center text-sm font-light text-gray-400 md:w-11/12 md:text-base md:font-extralight">
                            {value.paragraph}
                        </p>
                    </div>
                ))}
            </div>
        </section>
    );
};
export default Processus;
