import { useState } from 'react';
import Image from "next/image";
import { useTranslation } from "react-i18next";
import { useKeenSlider } from "keen-slider/react"
import styles from './BestManager.module.scss';
import Arrow from "../../assets/marches-financier/doubleArrow.svg";
import Cloud from "../../assets/marches-financier/cloud.svg";
import CardManager from "./CardManager";
import CardArrow from "./CardArrow";
import VarenneImg from "../../assets/marches-financier/varenne.png"
import JpMorganImg from "../../assets/marches-financier/jpmorgan.png"
import PictetImg from "../../assets/marches-financier/pictet.png"

const autoLoop = (slider) => {
    let timeout
    let mouseOver = false
    function clearNextTimeout() {
        clearTimeout(timeout)
    }
    function nextTimeout() {
        clearTimeout(timeout)
        if (mouseOver) return
        timeout = setTimeout(() => {
            slider.next()
        }, 4000)
    }
    slider.on("created", () => {
        slider.container.addEventListener("mouseover", () => {
            mouseOver = true
            clearNextTimeout()
        })
        slider.container.addEventListener("mouseout", () => {
            mouseOver = false
            nextTimeout()
        })
        nextTimeout()
    })
    slider.on("dragStarted", clearNextTimeout)
    slider.on("animationEnded", nextTimeout)
    slider.on("updated", nextTimeout)
};

const BestManager = () => {
    const { t } = useTranslation();
    const [currentSlide, setCurrentSlide] = useState(1)
    const [loaded, setLoaded] = useState(false)
    const [sliderRef, instanceRef] = useKeenSlider({
        mode: "free-snap",
        slides: {
            origin: "center",
            perView: 1.2,
            spacing: 12,
            size: 0.9
        },
        initial: 1,
        loop: true,
        breakpoints: {
            '(min-width: 768px)': {
                slides: {
                    origin: "center",
                    perView: 2,
                    spacing: 28,
                    size: 0.8
                },
            },
        },
        created() {
            setLoaded(true)
        },
        slideChanged(slider) {
            setCurrentSlide(slider.track.details.rel)
        },
    }, [autoLoop])
    const cards = [
        {
            img: JpMorganImg,
            srri: 6,
            title: t('marchesFinanciers.managers.jpMorgan.title'),
            perf: t('marchesFinanciers.managers.jpMorgan.perf'),
            loss:  t('marchesFinanciers.managers.jpMorgan.maxLoss'),
            volatility:  t('marchesFinanciers.managers.jpMorgan.volatility'),
            subtitle:  t('marchesFinanciers.managers.jpMorgan.subtitle'),
            desc:  t('marchesFinanciers.managers.jpMorgan.desc'),
        },
        {
            img: VarenneImg,
            srri: 4,
            title: t('marchesFinanciers.managers.varenne.title'),
            perf: t('marchesFinanciers.managers.varenne.perf'),
            loss:  t('marchesFinanciers.managers.varenne.maxLoss'),
            volatility:  t('marchesFinanciers.managers.varenne.volatility'),
            subtitle:  t('marchesFinanciers.managers.varenne.subtitle'),
            desc:  t('marchesFinanciers.managers.varenne.desc'),
        },
        {
            img: PictetImg,
            srri: 3,
            title: t('marchesFinanciers.managers.pictet.title'),
            perf: t('marchesFinanciers.managers.pictet.perf'),
            loss:  t('marchesFinanciers.managers.pictet.maxLoss'),
            volatility:  t('marchesFinanciers.managers.pictet.volatility'),
            subtitle:  t('marchesFinanciers.managers.pictet.subtitle'),
            desc:  t('marchesFinanciers.managers.pictet.desc'),
        },
    ]
    return (
        <section className="page-section px-0">
            <div className="flex flex-col items-center w-full mb-8 md:mb-10">
                <div className="mb-10 text-center text-lg md:text-xl uppercase text-white">
                    Notre sélection
                </div>
                <div className="relative flex justify-center items-center h-9 w-9 animate-bounce">
                    <Image
                        src={Arrow}
                        alt="deux flèches qui pointes vers le bas"
                        quality={75}
                    />
                    <div className="absolute top-1/2 left-1/2 w-[400%] h-[400%] -translate-x-1/2 -translate-y-1/2 z-0">
                        <Image
                            src={Cloud}
                            alt="petit nuage bleu pour donner du relief au composant"
                            quality={75}
                            width={184}
                            height={175}
                        />
                    </div>
                </div>
            </div>
            <h1 className="main-title mb-8 text-center md:mb-20">
                {t('marchesFinanciers.managers.title1')}
                <br />
                <span className="font-light">
                    {t('marchesFinanciers.managers.title2')}
                </span>
            </h1>
            <div className="relative">
                <div ref={sliderRef} className="keen-slider">
                    { cards.map((card, idx) => (
                        <CardManager key={card.title} data={card} isShown={currentSlide === idx}/>
                    ))}
                </div>
                {loaded && instanceRef.current && (
                    <>
                        <CardArrow
                        left
                        onClick={(e) =>
                            e.stopPropagation() || instanceRef.current?.prev()
                        }
                        />

                        <CardArrow
                        onClick={(e) =>
                            e.stopPropagation() || instanceRef.current?.next()
                        }
                        />
                    </>
                )}
            </div>
        </section>
    );
};

export default BestManager;
