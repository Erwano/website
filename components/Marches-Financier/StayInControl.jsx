import Image from "next/image";
import styles from "./StayInControl.module.scss";
import BlueButton from "../BlueButton";
import stayInControl from "../../assets/marches-financier/stayInControl.png";
import stayInControlBorder from "../../assets/marches-financier/stayInControlBorder.svg";
import Bar from "../../assets/marches-financier/BarStayControl.svg";
import Arrow from "../../assets/marches-financier/arrow.svg";
import JulesImg from "../../assets/marches-financier/jules.jpg";

const StayInControl = () => {
    return (
        <section className="page-section bg-top px-4 pb-0 md:px-0">
            <div className="grid grid-cols-12">
                <div className="col-span-12  flex items-center justify-center lg:col-span-4 lg:col-start-3">
                    <div className="relative w-11/12 max-w-sm text-xs md:text-base">
                        <div className="absolute top-12 left-0 z-10 h-8 w-8 md:left-5 md:h-10 md:w-10">
                            <Image src={Arrow} alt="signe de croissance" />
                        </div>
                        <div className="absolute -left-3 bottom-24 z-10 md:-left-10 md:bottom-32 ">
                            <div className="relative flex items-center rounded-full bg-white  p-1 md:p-2">
                                <div className="h-5 w-5 overflow-hidden rounded-full md:h-7 md:w-7">
                                    <Image
                                        src={JulesImg}
                                        alt="Conseiller colbr"
                                    />
                                </div>
                                <span className="mx-2 text-sm font-light text-black">
                                    Meeting Jules...
                                </span>
                                <div className="absolute -top-2 right-0 flex h-5 w-5 items-center justify-center rounded-full bg-blue-800">
                                    <span className="text-xs text-white">
                                        3
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="relative md:p-0">
                            <Image src={stayInControlBorder} alt="" />
                            <div className="absolute top-[8px] left-[8px] h-[calc(100%-16px)] w-[calc(100%-16px)]">
                                <Image
                                    src={stayInControl}
                                    alt="un client heureux d'avoir le contrôle sur son patrimoine"
                                />
                            </div>
                        </div>
                        <div
                            className={`${styles.backgroundStayControl} absolute -right-3 top-12 flex origin-top-right flex-col items-center rounded-lg py-2 px-6 md:-right-24 md:top-12 md:px-9 md:py-4 md:text-sm`}
                        >
                            <p className="text-white opacity-100 ">
                                Portefeuille
                            </p>
                            <p className="text-base font-semibold text-white opacity-100 md:text-4xl">
                                476 670 €
                            </p>
                        </div>
                        <div className="absolute -right-4 bottom-12 z-10 md:bottom-20">
                            <div className="flex items-center rounded-full bg-white p-1 text-black md:p-2">
                                <div className="h-5 w-5 md:h-7 md:w-7">
                                    <Image src={Bar} alt="graphique avec bar" />{" "}
                                </div>
                                <span className="mx-2 text-sm font-light text-black">
                                    75% de l’objectif atteint
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-span-12 flex flex-col items-center lg:col-span-4 lg:col-start-8 lg:items-start lg:pl-2">
                    <h1 className="main-title lg-mt-0 title-gradient mt-12 mb-8 text-center lg:text-left">
                        <span className="font-light">Restez aux commandes</span>
                        <br />
                        de votre patrimoine
                    </h1>
                    <div className="space-y-6 text-sm md:text-lg">
                        <p className="font-medium text-white">
                            Suivi de vos investissements en temps réel <br />
                            <span className="font-light">
                                depuis votre espace sur le site ou l’application
                                mobile
                            </span>
                        </p>
                        <p className=" font-medium text-white">
                            Accédez à nos{" "}
                            <span className="font-normal">
                                {" "}
                                propositions d’arbitrage
                            </span>
                        </p>
                        <p className="font-medium text-white">
                            Accédez à de multiples ressources <br />
                            <span className="font-light">
                                Webinar, reporting, analyses
                            </span>
                        </p>
                        <p className="font-medium text-white">
                            Contactez votre advisor en 1 clic
                        </p>
                        <p className="font-medium text-white">
                            <span className="font-light">
                                Un univers compatible{" "}
                            </span>
                            Desktop & Mobile
                        </p>
                    </div>
                </div>
            </div>
            <div className="backgroundImg mt-20 pt-12">
                <h1 className="main-title md:px-0">
                    <span className="sm:font-light">
                        Vous êtes prêts pour faire travailler votre épargne
                    </span>
                    <br className="hidden sm:flex" />
                    <span className="hidden sm:block">
                        Démarrez votre projet avec Colbr dès maintenant
                    </span>{" "}
                </h1>
                <div className="flex justify-center pt-10 pb-52">
                    <BlueButton />
                </div>
            </div>
        </section>
    );
};
export default StayInControl;
