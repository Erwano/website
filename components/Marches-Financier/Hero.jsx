import Image from "next/image";
import heroImg from "../../assets/marches-financier/hero.png";
import BlueButton from "../BlueButton";
import IconHero from "../../assets/marches-financier/iconHero.svg";
import Valide from "../../assets/marches-financier/valide.svg";
import s from "./hero.module.scss"
import Mouse_bounce from "components/shared/MouseBounce";

const Hero = () => {
    return (
        <section className={`${s.backgroundImgDesk} page-section hero grid grid-cols-12 relative`}>
            <div className="hidden md:hero-image-block-wrapper mb-10 md:mb-0">
                <div className="hero-image-block">
                    <div className="hero-icon -left-10 top-8 md:top-20">
                        <Image
                            src={IconHero}
                            alt="marché financier"
                        />
                    </div>
                    <Image src={heroImg} alt="un couloir" className="rounded-3xl"/>
                    <div className="hero-text-block -right-12 bottom-8 md:-right-24 md:bottom-40">
                        <div className="w-5 h-5 md:w-6 md:h-6 mr-2">
                            <Image src={Valide} alt=""/>
                        </div>
                        Investissement réussi
                    </div>
                </div>
            </div>
            <div className="hero-text-block-wrapper md:title-gradient">
                <h1 className="main-title hero">
                    <span className="font-light">Les marchés </span>
                    financiers
                </h1>
                <blockquote className="hero-text-subtitle">
                    &ldquo;The big money is not in the buying or selling, <br className="hidden md:block"/> but in the waiting&rdquo;
                </blockquote>
                <div className="hero-text-author">Charlie Munger</div>
                <BlueButton />
            </div>
            <div className="hidden lg:block absolute bottom-10 left-20 2xl:left-0">
                <Mouse_bounce/>
            </div>
        </section>
    );
};

export default Hero;
