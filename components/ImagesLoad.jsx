import Image from "next/image";

const ImagesLoad = ({ img, width, height, alt }) => {

    return ( 
        <Image 
            src={img}  
            height={height}
            width={width}
            alt={alt}
        />
    );
};

export default ImagesLoad;
