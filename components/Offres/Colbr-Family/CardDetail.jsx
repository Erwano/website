import Image from "next/image";

const CardDetail = (props) => {
    const { img, title, text } = props;

    return (
        <div className="backgroundCardService flex w-11/12 max-w-sm md:max-w-none items-center rounded-[92px] p-5 md:rounded-r-none ">
            <div className="flex items-center justify-center">
                <div className="relative h-16 w-16 lg:h-20 lg:w-20">
                    <Image src={img} alt="formule" />
                </div>
            </div>

            <div className="ml-3">
                <h6 className="text-sm text-white lg:text-xl">{title}</h6>
                <p className="hidden md:block text-xs text-gray-400 lg:text-sm">{text}</p>
            </div>
        </div>
    );
};
export default CardDetail;
