import Image from "next/image";
import ImgDetails from "../../../assets/colbr-family/detail.png";
import Team from "../../../assets/colbr-family/team.svg";
import Univers from "assets/colbr-family/univers.svg"
import Groupe from "../../../assets/colbr-family/groupe.svg";
import CardDetail from "./CardDetail";

const Details = () => {
    return (
        <section className="page-section">
            <div className=" mb-16 flex flex-col items-center justify-center text-white">
                <div className="text-xl md:text-2xl md:mb-2">
                    Rejoignez
                    <span className="font-light"> un vrai Family Office </span>
                </div>
                <div className="text-4xl md:ml-2 md:text-5xl">
                    {" "}
                    dès 500 000€
                </div>
            </div>
            <div className="grid h-full w-full grid-cols-12 md:grid-flow-col">
                <div className="col-span-12 flex items-center justify-center md:col-span-6 md:col-start-7 md:justify-start lg:col-span-4 xl:col-span-4">
                    <div className="relative w-4/6 max-w-xs md:w-11/12 md:max-w-[340px] 2xl:max-w-lg">
                        <Image
                            src={ImgDetails}
                            alt="personne qui regarde le toit"
                            width={447}
                            height={717}
                        />
                    </div>
                </div>
                <div className="col-span-12 mt-5 flex items-center justify-center md:col-span-6 md:col-start-1 md:mt-0 lg:col-start-2">
                    <div className="flex flex-col items-center justify-center space-y-5 md:w-full md:items-end">
                        <h1 className="text-2xl font-medium text-white w-5/6 text-center xl:text-4xl">
                            <span className="font-light">
                                Un service d’exception
                            </span>{" "}
                            <br />
                            pour votre patrimoine
                        </h1>
                        <CardDetail
                            img={Team}
                            title="Une équipe dédiée"
                            text={`Bénéficiez d’un binôme de conseillers family office attitré et de l’appui de tous nos experts sectoriels.`}
                        />
                        <CardDetail
                            img={Univers}
                            title="Un univers d’investissement exclusif"
                            text={`Accès à l’écosystème luxembourgeois, des solutions de financement spécifiques & des opportunités d’investissements dédiées.`}
                        />
                        <CardDetail
                            img={Groupe}
                            title="Une communauté de passionnés"
                            text={`Découvrez des lieux et des personnes d’exception au travers de nos évènements.`}
                        />
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Details;
