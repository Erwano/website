import Image from "next/image";
import HeroImg from "../../../assets/colbr-family/hero.png";
import ArcCircle from "../../../assets/Vector-219.svg";
import BlueButton from "../../BlueButton";
import Cloud from "../../../assets/cloud2.svg";
import s from "./hero.module.scss"
import Mouse_bounce from "components/shared/MouseBounce";

const Hero = () => {
    return (
        <section className={`${s.backgroundImgDesk} page-section hero flex items-center justify-center`}>
            <div className="grid h-full w-full grid-cols-12 ">
                <div className="hidden relative col-span-12 md:flex justify-center md:col-span-6 md:col-start-1 md:items-center md:justify-center xl:col-span-4 xl:col-start-3">
                    <div className="relative w-4/6 max-w-[240px] md:w-4/5 2xl:max-w-md">
                        <Image
                            src={HeroImg}
                            alt="personne qui regarde le toit"
                            width={549}
                            height={881}
                            className="rounded-3xl"
                        />
                    </div>
                </div>
                <div className="col-span-12 mt-5 flex items-center justify-center md:col-span-5 md:mt-0 xl:col-start-7 xl:justify-start">
                    <div className="relative z-10 mt-5 flex flex-col items-center justify-center md:mt-0">
                        <h1 className="main-title mb-5">
                            <span className="font-light">Colbr </span>
                            Family Office
                        </h1>
                        <div className="relative mr-5 mb-5 flex w-11/12 justify-center">
                            <Image
                                src={ArcCircle}
                                alt="Arc de cercle bleu"
                                width={233}
                                height={19}
                            />
                        </div>
                        <div className="absolute -z-10">
                            <Image
                                src={Cloud}
                                alt="Arc de cercle bleu"
                                width={928}
                                height={854}
                            />
                        </div>
                        <p className="text-center text-sm text-white sm:text-base lg:text-xl mb-7">
                            <span className="font-light">
                                Construire un patrimoine avec une équipe dédiée{" "}
                            </span>{" "}
                            <br />à votre patrimoine et vos investissements
                        </p>
                        <BlueButton />
                    </div>
                </div>
            </div>
            <div className="hidden lg:block absolute bottom-10 left-20 2xl:left-0">
                <Mouse_bounce/>
            </div>
        </section>
    );
};
export default Hero;
