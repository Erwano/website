import Image from "next/image";
import BlueButton from "../BlueButton";
import CloudHorizontal from "../../assets/EllipseHorizontal.png";

const Start = (props) => {
    const { img, img2, title1, title2 } = props;

    return (
        <section className="page-section last flex flex-col items-center justify-center">
            <div className="relative flex items-center justify-center space-x-5 text-white md:flex-row md:space-x-0">
                <div className="mb-7 flex h-16 w-16 overflow-hidden rounded-full md:mr-6 md:h-32 md:w-32 md:flex-shrink">
                    <Image
                        src={img}
                        alt="Expert Colbr pret a vous acceuillir"
                        width={128}
                        height={128}
                        quality={100}
                        objectFit="cover"
                        objectPosition="top"
                    />
                </div>
                <div className="absolute">
                    <Image
                        src={CloudHorizontal}
                        alt="fond bleu en forme de nuage"
                    />
                </div>
                {img2 && (
                    <div className="mb-7 flex h-16 w-16 overflow-hidden rounded-full md:ml-6 md:h-32 md:w-32 md:flex-shrink">
                        <Image
                            src={img2}
                            alt="Expert Colbr pret a vous acceuillir"
                            width={128}
                            height={128}
                            quality={100}
                            objectFit="cover"
                            objectPosition="top"
                        />
                    </div>
                )}
            </div>
            <h1 className="main-title mb-7">
                <span className="font-light">{title1}</span>{" "}
                <br /> {title2}
            </h1>
            <div className="z-10 mt-10 flex justify-center">
                <BlueButton />
            </div>
        </section>
    );
};
export default Start;
