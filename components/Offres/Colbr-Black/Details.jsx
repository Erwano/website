import Image from "next/image";
import ImgDetails from "../../../assets/colbr-black/imgDetails.png";
import Hand from "../../../assets/colbr-black/hand.svg";
import Frais from "../../../assets/colbr-black/frais.svg";
import Support from "../../../assets/colbr-black/support.svg";
import CardDetail from "./CardDetail";

const Details = () => {
    return (
        <section className="page-section">
            <div className=" mb-16 flex flex-col items-center justify-center text-white">
                <div className="text-xl md:text-2xl md:mb-2">
                    Commencez
                    <span className="font-light"> à investir</span>
                </div>
                <div className="text-4xl md:text-5xl">
                    dès 50 000 €
                </div>
            </div>
            <div className="grid h-full w-full grid-cols-12 md:grid-flow-col">
                <div className="col-span-12 flex items-center justify-center md:col-span-6 md:col-start-7 md:justify-start lg:col-span-4 xl:col-span-4">
                    <div className="relative w-4/6 max-w-xs md:w-11/12 md:max-w-[340px] 2xl:max-w-lg">
                        <Image
                            src={ImgDetails}
                            alt="personne qui regarde le toit"
                            width={447}
                            height={717}
                        />
                    </div>
                </div>
                <div className="col-span-12 mt-5 flex items-center justify-center md:col-span-6 md:col-start-1 md:mt-0 lg:col-start-2">
                    <div className="flex flex-col items-center justify-center space-y-5 md:w-full md:items-end">
                        <h1 className="text-2xl text-white font-medium w-5/6 text-center xl:text-4xl">
                            <span className="font-light">
                                Une offre adaptée
                            </span>{" "} <br/>
                            à vos besoins
                        </h1>
                        <CardDetail
                            img={Hand}
                            title={`Un accès à l’univers d’investissement premium`}
                            text="Les meilleurs gérants mondiaux (JP Morgan, Moneta, M&G), notre sélection immobilière et des fonds de Private Equity Tier 1."
                        />
                        <CardDetail
                            img={Frais}
                            title="Un suivi personnalisé"
                            text="Vous bénéficiez d’un parcours sur-mesure avec un conseiller qui vous est dédié."
                        />
                        <CardDetail
                            img={Support}
                            title="Un pricing concurrentiel"
                            text="Une offre compétitive et radicalement transparente (pas de frais cachés)."
                        />
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Details;
