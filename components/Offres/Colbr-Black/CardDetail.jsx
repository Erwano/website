import Image from "next/image";

const CardDetail = (props) => {
    const { img, title, text } = props;

    return (
        <div className="backgroundCardService p-5 flex w-11/12  items-center rounded-[92px] md:rounded-r-none">
            <div className="flex justify-center items-center">
                <div className="relative h-16 w-16 flex lg:w-20 lg:h-20">
                    <Image
                        src={img}
                        alt="formule"
                    />
                </div>
            </div>

            <div className="ml-3">
                <h6 className="text-white text-base lg:text-xl">{title}</h6>
                <p className="hidden md:block text-xs text-gray-400 lg:text-sm">{text}</p>
            </div>
        </div>
    );
};
export default CardDetail;
