import Image from "next/image";
import HeroImg from "assets/colbr-black/hero.png";
import ArcCircle from "assets/Vector-219.svg";
import BlueButton from "../../BlueButton";
import Cloud from "assets/cloud2.svg";
import s from "./hero.module.scss"
import Mouse_bounce from "components/shared/MouseBounce";

const Hero = () => {
    return (
        <section className={`${s.backgroundImgDesk} page-section hero flex justify-center items-center`}>
            <div className="grid h-full w-full grid-cols-12 ">
                <div className="hidden relative col-span-12 md:flex justify-center md:col-span-6 md:col-start-1 md:items-center md:justify-center xl:col-span-4 xl:col-start-3">
                    <div className="relative w-4/6 max-w-[240px] md:w-4/5 2xl:max-w-md">
                        <Image
                            src={HeroImg}
                            alt="personne qui regarde le toit"
                            width={549}
                            height={881}
                            className="rounded-3xl"
                        />
                    </div>
                </div>
                <div className="col-span-12 mt-5 flex items-center justify-center md:col-span-6 md:mt-0 xl:justify-start xl:col-start-7">
                    <div className="flex flex-col items-center justify-center relative z-10 mt-5 md:mt-0">
                        <h1 className="main-title mb-5">
                            <span className="font-light">Colbr </span>
                            Black
                        </h1>
                        <div className="relative mr-5 mb-5 flex w-11/12 justify-center">
                            <Image
                                src={ArcCircle}
                                alt="Arc de cercle bleu"
                                width={233}
                                height={19}
                            />
                        </div>
                        <div className="absolute -z-10">
                            <Image
                                src={Cloud}
                                alt="Arc de cercle bleu"
                                width={928}
                                height={854}
                            />
                        </div>
                        <p className="mb-8 text-center text-sm text-white sm:text-base lg:text-xl">
                            <span className="font-light">
                                Accédez à un univers de placement haut de gamme 
                            </span>
                            <br />
                            à travers un service premium
                        </p>
                        <BlueButton />
                    </div>
                </div>
            </div>
            <div className="hidden lg:block absolute bottom-10 left-20 2xl:left-0">
                <Mouse_bounce/>
            </div>
        </section>
    );
};
export default Hero;
