import Image from "next/image";
import Enveloppe from "../../../assets/colbrFuture/enveloppe.png";
const Register = () => {
    return (
        <section className="h-[calc(100vh-95px)] w-full ">
            <div className="w-full h-full flex justify-center items-center">
                <div className="flex flex-col justify-center items-center space-y-5">
                    <div className="relative w-1/2 flex justify-center">
                        <Image
                            src={Enveloppe}
                            alt="designe une inscription a un evenement"
                            width={134}
                            height={127}
                        />
                    </div>
                    <h1 className="text-center text-white text-xl md:text-2xl xl:text-5xl">
                        <span className="font-light">
                            Inscrivez vous dès maintenant
                        </span>
                        <br/>
                        Pour bénéficier d’informations exclusives
                    </h1>
                    <button className="text-white bg-blue-900 py-2 px-5 rounded-full">Je m’inscris</button>
                </div>
            </div>
        </section>
    );
};
export default Register;
