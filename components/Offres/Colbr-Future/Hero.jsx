import Image from "next/image";
import HeroImg from "../../../assets/colbrFuture/hero.png";
import ArcCircle from "../../../assets/Vector-219.svg";
import BlueButton from "../../BlueButton";

const Hero = () => {
    return (
        <section className="min-h-[calc(100vh-95px)] w-full pt-4 md:h-[calc(100vh-95px)]">
            <div className="grid h-full w-full grid-cols-12 md:grid-rows-1 mt-5 md:mt-0">
                <div className="relative col-span-12 md:col-span-6 md:col-start-7 xl:col-span-5 flex justify-center items-center after:content-['COMING_SOON_...'] after:absolute after:text-white after:text-xs md:after:text-base after:flex after:w-full after:h-full after:justify-center after:items-center">
                    <div className="relative w-4/6 blur-lg opacity-50 max-w-xs 2xl:max-w-sm">
                        <Image
                            src={HeroImg}
                            alt="personne qui regarde le toit"
                            width={447}
                            height={717}
                        />
                    </div>
                </div>
                <div className="col-span-12 mt-5 flex items-center justify-center md:row-start-1 md:col-span-4 md:col-start-2 xl:col-start-3 md:w-ful">
                    <div className="flex flex-col items-center justify-center space-y-5 md-w-full md:items-center ">
                        <h1 className="text-xl md:text-2xl xl:text-5xl mt-10 md:mt-0 tex-center text-white ">
                            <span className="font-light">Colbr</span> Future
                        </h1>
                        <div className="relative mr-5 flex w-11/12 justify-center md:justify-center">
                            <Image
                                src={ArcCircle}
                                alt="Arc de cercle bleu"
                                width={233}
                                height={19}
                            />
                        </div>
                        <p className="max-w-sm w-11/12 text-sm lg:text-base text-center md:w-full md:max-w-md text-gray-700">
                            Dans le prolongement de notre volonté de
                            démocratiser les meilleures solutions
                            d’investissement, Nous développons une nouvelle
                            offre
                        </p>
                        <BlueButton title="Je m’inscris sur la liste d’attente" />
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Hero;
