import Image from "next/image";
import ImgDetails from "../../../assets/colbrFuture/imgDetails.png";

const Details = () => {
    return (
        <section className="min-h-[calc(100vh - 95px)] w-full mt-40 backgroundImg bg-top bg-no-repeat bg-cover">
            <div className=" flex flex-col md:flex-row justify-center items-center text-white mb-16">
                <h1 className="text-center text-xl md:text-2xl -mt-20 md:-mt-8 mb-2 ">
                    <span className="font-light">Une formule qui vous permettra</span> <br/>
                    d’investir avec Colbr à partir de
                </h1>
                <h1 className="text-5xl md:-mt-8 md:ml-4">10 000€</h1>
            </div>
            <div className="grid h-full w-full grid-cols-12 ">
                <div className="relative col-span-12 flex justify-center md:col-span-6 md:col-start-1 md:items-center md:justify-center xl:col-span-5 xl:col-start-2">
                    <div className="relative w-4/6 max-w-xs md:w-2/3 md:max-w-sm">
                        <Image
                            src={ImgDetails}
                            alt="personne qui regarde le toit"
                            width={447}
                            height={717}
                        />
                    </div>
                </div>
                
                <div className="col-span-12 mt-5 flex items-center justify-center md:mt-0 md:col-span-5 xl:col-start-7">
                    <div className="flex flex-col items-center justify-center space-y-5 md:items-start md:w-full">
                        <h1 className="text-2xl text-white md:text-5xl">
                            <span className="font-light">Bientôt</span>{" "}
                            Disponible
                        </h1>
                        <p className="max-w-sm text-sm md:text-base text-center text-gray-700 md:text-left lg:max-w-xl">
                            Colbr Future a été pensé pour toutes les personnes
                            qui travaillent dur chaque jour pour se construire
                            le patrimoine de demain. Nous croyons en vous dès
                            aujourd’hui et nous vous donnons les clés pour
                            atteindre vos objectifs ensemble.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default Details;
