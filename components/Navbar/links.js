import {
    OfficeBuildingIcon, CurrencyDollarIcon, ChartBarIcon, ChartPieIcon,
    ColorSwatchIcon, CubeTransparentIcon, UserGroupIcon, NewspaperIcon,
    BriefcaseIcon,
} from "@heroicons/react/outline";
import { QuestionMarkCircleIcon, CubeIcon, StarIcon, PencilAltIcon } from "@heroicons/react/solid";

export const links = [
    {
        name: "Investir",
        current: false,
        icon: CurrencyDollarIcon,
        menu: [
            {
                name: "Marchés financiers",
                url: "/marches-financiers",
                icon: ChartBarIcon
            },
            {
                name: "Immobilier",
                url: "/immobilier",
                icon: OfficeBuildingIcon
            },
            {
                name: "Private Equity",
                url: "/private-equity",
                icon: ChartPieIcon,
            },
        ],
    },
    {
        name: "Advisory",
        current: false,
        url: "/advisory",
        icon: ColorSwatchIcon,
        menu: [],
    },
    {
        name: "Devenir membre",
        current: false,
        icon: StarIcon,
        menu: [
            {
                name: "Comment ça marche ?",
                url: "/comment-ca-marche",
                icon: QuestionMarkCircleIcon,
            },
            {
                name: "Colbr Black",
                url: "/offres/colbr-black",
                icon: CubeTransparentIcon,
            },
            {
                name: "Colbr Family-Office",
                url: "/offres/colbr-family-office",
                icon: CubeIcon
            },
        ],
    },
    {
        name: "L'équipe",
        current: false,
        url: "/equipe",
        icon: BriefcaseIcon,
        menu: [],
    },
];