import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/solid";
import styles from './NavDropdown.module.scss';

const Dropdown = ({ data, handleChangeRoute }) => {
    return (
        <>
            <Menu as="div" className="relative">
                <Menu.Button className="text-white flex hover:underline">
                    {data && data.name}
                    <ChevronDownIcon className="h-5 w-5 ml-1"/>
                </Menu.Button>
                <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                    <Menu.Items className={`absolute right-0 z-20 mt-2 flex w-48 origin-top-right flex-col rounded p-1 ${styles['menu-background']}`}>
                        {data &&
                            data.menu.map((menu, index) => (
                                <Menu.Item key={index}>
                                    <button
                                        className="rounded p-2 text-left text-white hover:underline"
                                        onClick={() => {
                                            menu.url
                                                ? handleChangeRoute(menu.url)
                                                : null;
                                        }}
                                    >
                                        {menu.name}
                                    </button>
                                </Menu.Item>
                            ))}
                    </Menu.Items>
                </Transition>
            </Menu>
        </>
    );
};

export default Dropdown;
