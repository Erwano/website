import { XIcon, UserIcon } from "@heroicons/react/outline";
import Logo from "assets/logo-colbr.svg";
import Image from "next/image";
import Burger from "assets/burger.svg";
import { useEffect, useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { useRouter } from "next/router";
import { links } from "./links";
import NavDropdown from "./NavDropdown";
import MenuMobile from "./MenuMobile";
import Link from "next/link";


const NavBar = () => {
    const [open, setOpen] = useState(false);
    const router = useRouter();

    useEffect(() => {
        if (open === true) {
            document.body.classList.add('overflow-hidden');
        } else {
            document.body.classList.remove('overflow-hidden');
        }
    }, [open]);

    const handleChangeRoute = (url) => {
        router.push(url);
    };

    return (
        <nav className="w-full sticky top-0 z-50 flex flex-col items-center justify-center xl:bg-[#090909] border-b-[1px] border-gray-800 2xl:mx-auto">
            <div className="relative flex h-full w-full flex-col justify-center items-center bg-[#090909]">
                <div className="z-10 flex w-full max-w-7xl items-center justify-between py-5 px-4">
                    <div
                        className="h-9 w-24 md:w-40 md:h-14 hover:cursor-pointer"
                        onClick={() => {
                            handleChangeRoute("/");
                            setOpen(false);
                        }}
                    >
                        <Image src={Logo} alt="Logo Colbr avec nom de l'entreprise"/>
                    </div>

                    <button
                        className="inline-flex items-center justify-center rounded-md text-gray-400 h-6 w-6 hover:bg-gray-700 hover:text-white focus:outline-none md:hidden"
                        onClick={(e) => setOpen(!open)}
                    >
                        {open === true ? (
                            <XIcon className="block h-6 w-6" />
                        ) : (
                            <Image
                                src={Burger}
                                alt="Colbr"
                            />
                        )}
                    </button>
                    <div className="hidden flex-1 md:text-sm md:block">
                        <div className="flex items-center space-x-10 md:justify-end">
                            {links.map((item, index) => (
                                item.menu.length 
                                ?   <NavDropdown
                                        key={index}
                                        data={item}
                                        handleChangeRoute={handleChangeRoute}
                                    />
                                :   <Link key={index} href={item.url}><a className="text-white hover:underline">{item.name}</a></Link>

                            ))}
                            <UserIcon
                                className="w-5 h-5 cursor-pointer text-white"
                                onClick={() => handleChangeRoute("/login")}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <AnimatePresence>
                {open && (
                    <motion.div
                        className="h-[calc(100vh-76px)] fixed top-[77px] left-0 overflow-hidden overflow-y-auto w-full bg-[#090909] px-4 md:hidden"
                        initial={{
                            x: 1000,
                            opacity: 0,
                        }}
                        animate={{
                            x: 0,
                            opacity: 1,
                        }}
                        exit={{
                            x: 1000,
                            opacity: 0
                        }}
                        transition={{ duration: 0.5 }}
                    >
                        <div className="space-y-3 px-2 pt-2 pb-3">
                            {links.map((item, index) => (
                                <MenuMobile
                                    key={index}
                                    data={item}
                                    handleChangeRoute={handleChangeRoute}
                                    close={setOpen}
                                />
                            ))}
                            <div className="flex flex-col items-center pt-10">
                                <button
                                    className="blue-button"
                                    onClick={() => {
                                        handleChangeRoute("/login");
                                        setOpen(false);
                                    }}
                                >
                                    Login
                                </button>
                            </div>
                        </div>
                    </motion.div>
                )}
            </AnimatePresence>
        </nav>
    );
};

export default NavBar;
