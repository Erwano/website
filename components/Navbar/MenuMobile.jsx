import React from "react";
import { Disclosure } from "@headlessui/react";
import { ChevronRightIcon } from "@heroicons/react/outline";

const MenuMobile = ({ data, handleChangeRoute, close }) => {
    return (
        <div className="w-full px-2 ">
            <div className="mx-auto w-full">
                { data.menu.length ?
                    <Disclosure>
                        <Disclosure.Button className="text-white hover:underline  w-full border-b-[0.5px] border-gray-700 px-3 py-4 text-left text-sm font-light flex">
                            { data.icon ? React.createElement(data.icon, { className: 'w-5 h-5 mr-4' }) : '' }
                            {data && data.name}
                        </Disclosure.Button>
                        <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                            {data &&
                                data.menu.map((menu, index) => (
                                    <button
                                        key={index}
                                        className="text-white hover:underline  w-full border-b-[0.5px] border-gray-700 px-3 py-4 text-left text-sm font-light flex"
                                        onClick={() => {menu.url && handleChangeRoute(menu.url); close(false)}}
                                    >
                                        <ChevronRightIcon className="w-5 h-5 mr-1"/>
                                        { menu.icon ? React.createElement(menu.icon, { className: 'w-5 h-5 mr-4' }) : '' }
                                        {menu.name}
                                    </button>
                                ))}
                        </Disclosure.Panel>
                    </Disclosure>
                    : <button
                        className="text-white hover:underline  w-full border-b-[0.5px] border-gray-700 px-3 py-4 text-left text-sm font-light flex"
                        onClick={() => {data.url && handleChangeRoute(data.url); close(false)}}
                    >
                        { data.icon ? React.createElement(data.icon, { className: 'w-5 h-5 mr-4' }) : '' }
                        {data.name}
                    </button>
                }  
            </div>
        </div>
    );
};
export default MenuMobile;
