import { useEffect } from "react";
import CheckAnimate from "components/shared/CheckAnimate/CheckAnimate";
import BlueButton from "components/BlueButton";

const FinalStep = (props) => {
    const { handleChangeRoute } = props;

    useEffect(() => {
        const timer = setTimeout(() => {
            localStorage.clear()
            handleChangeRoute('/')
        }, 3000);
        return () => clearTimeout(timer);
    }, []);

    return (
        <section className="h-[calc(100vh-95px)] w-full">
            <div className="flex h-full w-full flex-col items-center justify-center text-center">
                <h1 className="mb-10 text-4xl text-white">
                    Vous avez complété le questionnaire <br /> patrimonial avec
                    succès !
                </h1>
                <CheckAnimate />
                {/* <button className="blue-button" onClick={() => closePage()}>
                    Terminé
                </button> */}
            </div>
        </section>
    );
};

export default FinalStep;
