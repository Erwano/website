import Image from "next/image";
import heroImg from "assets/faisons-connaissance/door.png";
import s from "./hero.module.scss";
import CheckCircleIcon from "assets/Immobilier/BlueCheckCircle.svg";

const Hero = (props) => {
    const { setStatus, ScrollTop } = props;

    const handleChangeStep = () => {
        setStatus(1);
        ScrollTop();
    };

    const datas = [
        "Vos informations personnelles ",
        "Votre patrimoine (actifs & passifs)",
        "Vos revenus & charges ",
        "Vos objectifs patrimoniaux"
    ];

    return (
        <section
            className={`${s.backgroundImgDesk} w-full max-w-7xl min-h-[calc(100vh-76px)] lg:min-h-[calc(100vh-96px)] m-0 grid grid-cols-12 p-0`}
        >
            <div className="md:title-gradient col-span-12 mt-5 flex h-full w-full flex-col items-center justify-center md:col-span-6 lg:col-span-5 lg:col-start-2 lg:row-start-1 lg:mt-0">
                <h1 className="main-title mb-5 max-w-md">
                    <span className="font-light">Recueil d’informations </span>
                    patrimoniales
                </h1>
                <p className="mb-5 max-w-xs p-2 text-sm md:text-base text-white md:max-w-md">
                    Avant tout conseil en investissement, nous devons recueillir
                    les informations nécessaires afin d’agir au mieux de vos
                    intérêts et de délivrer un conseil adapté à votre situation.
                </p>
                <p className="mb-5 max-w-xs p-2 text-sm md:text-base text-white md:max-w-md">
                    A cet effet, nous vous soumettons ce recueil d’informations
                    qui vous permettra de nous communiquer vos informations
                    patrimoniales contenant :
                </p>
                <div className="mb-5 max-w-xs text-sm md:text-base text-white md:max-w-md font-light">
                    {datas.map((text, idx) => (
                        <div key={idx} className="mb-4 flex items-center ">
                            <div className="mr-5 h-7 w-7 text-blue-700">
                                <Image src={CheckCircleIcon} alt="" />
                            </div>
                            {text}
                        </div>
                    ))}
                </div>
                <div className="mt-5">
                    <button
                        onClick={() => handleChangeStep()}
                        className="blue-button"
                    >
                        Commencer
                    </button>
                </div>
            </div>
            <div className="hero-image-block-wrapper mb-10 hidden md:mb-0 md:flex">
                <div className="relative w-4/6 max-w-[240px] md:w-full md:max-w-xs 2xl:max-w-md">
                    <Image
                        src={heroImg}
                        className="overflow-hidden rounded-3xl"
                    />
                </div>
            </div>
        </section>
    );
};
export default Hero;
