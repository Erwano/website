import { useFormik } from "formik";
import { ValidationStep1 } from "./ValidationStep1";
import Select from "../../FormFields/Select";
import Input from "../../FormFields/Input";
import PhoneInput from "react-phone-input-2";
import {
    country_list,
    nationality,
    genre,
    investor,
    childrens,
    socioProfessionalCategory,
    familySituation,
    matrimonialRegime,
} from "../optionsData";
import { useState, useEffect } from "react";
import Switch from "../../FormFields/Switch";
import "react-phone-input-2/lib/bootstrap.css";
import { createUser } from "../../../pages/api";
import Tooltip from "components/shared/Tooltip";

const FormStep1 = ({ setStatus, ScrollTop }) => {
    const initialValue = {
        civility: "",
        email: "",
        lastName: "",
        firstName: "",
        birthday: "",
        cityOfBirth: "",
        countryOfBirth: "",
        nationality: "",
        mobile: "",
        profession: "",
        socioProfessionalCategory: "",
        fiscalAdress: "",
        familyStatus: "",
        matrimonialRegime: "",
        numberOfChildrens: 0,
        investorProfessional: false,
        usPerson: false,
        politicallyExposedPerson: false,
        status: 1,
    };

    const formik = useFormik({
        initialValues: initialValue,
        validationSchema: ValidationStep1,
        onSubmit: async (values) => {
            values.investorProfessional = indexInvestor === 0 ? false : true
            values.usPerson = isOnUs
            values.politicallyExposedPerson = isOn
            const user = await createUser(values);
            if (user && user.status && user.status === 409) {
                formik.setErrors({ email: "email already Exist" });
            } else if (user.email) {
                localStorage.setItem("user", user.id);
                localStorage.setItem("@", user.email);
                localStorage.setItem("status", 2);
                setStatus(2);
                ScrollTop();
            }
        },
    });

    const handleChangeCivi = (name, value) => {
        formik.setFieldValue(name, value);
    };

    const [indexChildren, setIndexChildren] = useState(0);
    const [indexInvestor, setInvestor] = useState(0);
    const [isOnUs, setIsOnUs] = useState(false);
    const [isOn, setIsOn] = useState(false);

    useEffect(() => {
        if (!formik.isSubmitting) return;
        if (Object.keys(formik.errors).length > 0) {
            ScrollTop();
        }
    }, [formik]);

    return (
        <div className="w-11/12">
            <form onSubmit={formik.handleSubmit}>
                <div className="overflow-hidden shadow sm:rounded-md">
                    <div className="wrap flex flex-col items-center gap-5">
                        <div className="grid w-full max-w-2xl grid-cols-12 gap-4 rounded-lg bg-[#071f4f6a] p-5 md:p-10">
                            <div className="col-span-12 mb-5 border-b-[1px] border-b-white">
                                <h1 className="mb-5 text-center text-xl uppercase text-white sm:text-left md:text-2xl">
                                    Information civile
                                </h1>
                            </div>
                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="civility"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Civilité
                                </label>
                                <Select
                                    data={genre}
                                    name="civility"
                                    handleChange={handleChangeCivi}
                                    errors={formik.errors.civility}
                                    touched={formik.touched.civility}
                                />
                            </div>
                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="firstName"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Prénom
                                </label>
                                <Input
                                    name="firstName"
                                    type="text"
                                    errors={formik.errors.firstName}
                                    touched={formik.touched.firstName}
                                    change={formik.handleChange}
                                    blur={formik.handleBlur}
                                    value={formik.values.firstName}
                                    placeholder="Prénom"
                                />
                            </div>
                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="lastName"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Nom
                                </label>
                                <Input
                                    name="lastName"
                                    type="text"
                                    errors={formik.errors.lastName}
                                    touched={formik.touched.lastName}
                                    change={formik.handleChange}
                                    blur={formik.handleBlur}
                                    value={formik.values.lastName}
                                    placeholder="Nom"
                                />
                            </div>
                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="email-address"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Email
                                </label>
                                <Input
                                    name="email"
                                    type="email"
                                    errors={formik.errors.email}
                                    touched={formik.touched.email}
                                    change={formik.handleChange}
                                    blur={formik.handleBlur}
                                    value={formik.values.email}
                                    placeholder="vous@exemple.fr"
                                />
                            </div>
                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="email-address"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Téléphone
                                </label>
                                <PhoneInput
                                    inputStyle={{ width: "100%" }}
                                    country={"fr"}
                                    placeholder="+33 6 60 89 05 48"
                                    inputClass={`h-11  appearance-none rounded-lg py-3 px-2 leading-tight placeholder:italic ${
                                        formik.touched.mobile &&
                                        formik.errors.mobile
                                            ? "border-2 border-red-400 focus:border-red-400 focus:ring-0 rounded-lg"
                                            : ""
                                    }`}
                                    inputProps={{
                                        name: "mobile",
                                    }}
                                    onChange={(phone) =>
                                        formik.setFieldValue("mobile", phone)
                                    }
                                    onBlur={formik.handleBlur}
                                    value={formik.values.mobile}
                                />
                                {formik.errors.mobile &&
                                    formik.touched.mobile && (
                                        <div className="w-72 pl-1 text-xs text-red-500">
                                            {formik.errors.mobile}
                                        </div>
                                    )}
                            </div>
                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="birthday"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Date de naissance
                                </label>
                                <Input
                                    name="birthday"
                                    type="date"
                                    errors={formik.errors.birthday}
                                    touched={formik.touched.birthday}
                                    change={formik.handleChange}
                                    blur={formik.handleBlur}
                                    value={formik.values.birthday}
                                    placeholder="19/05/1997"
                                />
                            </div>

                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="city"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Ville de naissance
                                </label>
                                <Input
                                    name="cityOfBirth"
                                    type="text"
                                    errors={formik.errors.cityOfBirth}
                                    touched={formik.touched.cityOfBirth}
                                    change={formik.handleChange}
                                    blur={formik.handleBlur}
                                    value={formik.values.cityOfBirth}
                                    placeholder="Paris"
                                />
                            </div>

                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="countryOfBirth"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Pays de naissance
                                </label>
                                <Select
                                    data={country_list}
                                    name="countryOfBirth"
                                    handleChange={handleChangeCivi}
                                    errors={formik.errors.countryOfBirth}
                                    touched={formik.touched.countryOfBirth}
                                />
                            </div>

                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="nationality"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Nationalité
                                </label>
                                <Select
                                    data={nationality}
                                    name="nationality"
                                    handleChange={handleChangeCivi}
                                    errors={formik.errors.nationality}
                                    touched={formik.touched.nationality}
                                />
                            </div>

                            <div className="col-span-12">
                                <label
                                    htmlFor="profession"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Profession
                                </label>
                                <Input
                                    name="profession"
                                    type="text"
                                    errors={formik.errors.profession}
                                    touched={formik.touched.profession}
                                    change={formik.handleChange}
                                    blur={formik.handleBlur}
                                    value={formik.values.profession}
                                    placeholder="Profession (ou dernière exercée)"
                                />
                            </div>

                            <div className="col-span-12">
                                <label
                                    htmlFor="socioProfessionalCategory"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Catégorie Socio-Professionnelle
                                </label>
                                <Select
                                    data={socioProfessionalCategory}
                                    name="socioProfessionalCategory"
                                    handleChange={handleChangeCivi}
                                    errors={
                                        formik.errors.socioProfessionalCategory
                                    }
                                    touched={
                                        formik.touched.socioProfessionalCategory
                                    }
                                />
                            </div>
                        </div>

                        <div className="grid max-w-2xl grid-cols-12 gap-4 rounded-lg bg-[#071f4f6a] p-5 md:p-10">
                            <div className="col-span-12 mb-5 border-b-[1px] border-b-white ">
                                <h1 className="mb-5 text-xl text-center sm:text-left uppercase text-white md:text-2xl">
                                    Profil
                                </h1>
                            </div>
                            <div className="col-span-12">
                                <label
                                    htmlFor="fiscalAdress"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Adresse fiscale
                                </label>
                                <Input
                                    name="fiscalAdress"
                                    type="text"
                                    errors={formik.errors.fiscalAdress}
                                    touched={formik.touched.fiscalAdress}
                                    change={formik.handleChange}
                                    blur={formik.handleBlur}
                                    value={formik.values.fiscalAdress}
                                    placeholder="ex: 10 rue des Champs 75000 Paris"
                                />
                            </div>
                            <div className="col-span-12 sm:col-span-6">
                                <label
                                    htmlFor="familyStatus"
                                    className="block text-sm text-gray-400 md:text-base"
                                >
                                    Situation familiale
                                </label>
                                <Select
                                    data={familySituation}
                                    name="familyStatus"
                                    handleChange={handleChangeCivi}
                                    errors={formik.errors.familyStatus}
                                    touched={formik.touched.familyStatus}
                                />
                            </div>
                            {formik.values.familyStatus === "Marié(e)" && (
                                <div className="col-span-12 sm:col-span-6">
                                    <label
                                        htmlFor="matrimonialRegime"
                                        className="block text-sm text-gray-400 md:text-base"
                                    >
                                        Régime matrimonial
                                    </label>
                                    <Select
                                        data={matrimonialRegime}
                                        name="matrimonialRegime"
                                        handleChange={handleChangeCivi}
                                        errors={formik.errors.matrimonialRegime}
                                        touched={
                                            formik.touched.matrimonialRegime
                                        }
                                    />
                                </div>
                            )}

                            <div className="col-span-12">
                                <label className="block text-sm text-gray-400 md:text-base">
                                    Nombre d’enfants à charge
                                </label>
                                <div className="mt-4 flex w-full flex-wrap items-center justify-between gap-4 md:h-11 md:gap-0">
                                    {childrens.map((children, i) => (
                                        <div
                                            key={i}
                                            className={` flex max-h-11 w-2/5 items-center justify-center rounded-lg border border-gray-400 p-2 text-white hover:cursor-pointer md:w-1/5 
                                                ${
                                                    i === indexChildren
                                                        ? "border-[#C29771] bg-[#C29771]"
                                                        : ""
                                                }
                                            `}
                                            onClick={() => {
                                                setIndexChildren(i);
                                                formik.setFieldValue(
                                                    "numberOfChildrens",
                                                    i
                                                );
                                            }}
                                        >
                                            {children}
                                        </div>
                                    ))}
                                </div>
                                {formik.errors.numberOfChildrens && (
                                    <div className="h-4 w-full pl-1 text-xs text-red-500">
                                        {formik.errors.numberOfChildrens}
                                    </div>
                                )}
                            </div>
                            <div className="col-span-12">
                                <label className="block text-sm text-gray-400 md:text-base">
                                    Vous êtes ...
                                </label>
                                <div className="mt-4 flex flex-col flex-wrap items-center justify-around gap-4 sm:flex-row">
                                    {investor.map((invest, i) => (
                                        <div
                                            className={`flex w-4/6 items-center justify-center rounded-lg border py-6 text-center text-sm text-white hover:cursor-pointer sm:w-2/6
                                                ${
                                                    indexInvestor === i
                                                        ? "border-[#C29771] bg-[#C29771] "
                                                        : ""
                                                }
                                            `}
                                            key={i}
                                            onClick={() => setInvestor(i)}
                                        >
                                            <h6>{invest}</h6>
                                        </div>
                                    ))}
                                </div>
                                {formik.errors.investorProfessional && (
                                    <div className="h-4 w-full pl-1 text-xs text-red-500">
                                        {formik.errors.investorProfessional}
                                    </div>
                                )}
                            </div>
                            <div className="col-span-12 ">
                                <p className="col-span-6 my-2 w-11/12 text-sm text-gray-400">
                                    Vous êtes considéré comme inestisseur non
                                    professionnel si vous ne représentez pas
                                    d’institution financière (banque, compagnie
                                    d’assurance, ...) et que vous n’êtes pas un
                                    investisseur professionnel au sens de
                                    l’article D411-1 du Code Monétaire et
                                    Financier. Un doute ?{" "}
                                    <a
                                        className="text-[#1C4699]"
                                        href="https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006640841/2009-05-22"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >
                                        {" "}
                                        En savoir plus
                                    </a>
                                </p>
                            </div>
                        </div>

                        <div className="grid w-full  max-w-2xl grid-cols-12 gap-4 rounded-lg bg-[#071f4f6a] p-5 md:p-10">
                            <div className="col-span-12 mb-5 border-b-[1px] border-b-white ">
                                <h1 className="mb-5 text-xl text-center sm:text-left uppercase text-white md:text-2xl">
                                    SITUATION JURIDIQUE
                                </h1>
                            </div>
                            <div className="col-span-12">
                                <label className="text-sm text-gray-400 md:text-base flex">
                                    Avez-vous le statut de contribuable &nbsp;<Tooltip content="Citoyen, résident, carte verte, possession d'entité" > *US Person*</Tooltip> &nbsp; ?
                                </label>
                                <a
                                    className="text-[#1C4699] text-sm"
                                    href="https://bofip.impots.gouv.fr/bofip/10246-PGP.html/identifiant=BOI-INT-AEA-10-30-10-20200226"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    En savoir plus
                                </a>
                                <Switch
                                    isOn={isOnUs}
                                    onClick={() => setIsOnUs(!isOnUs)}
                                />
                            </div>
                            <div className="col-span-12">
                                <label className="block text-sm text-gray-400 md:text-base">
                                    Êtes-vous une personne politiquement exposée
                                    ?
                                </label>
                                <a
                                    className="text-[#1C4699] text-sm"
                                    href="https://www.amf-france.org/fr/reglementation/doctrine/doc-2019-17"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    En savoir plus
                                </a>
                                <Switch
                                    isOn={isOn}
                                    onClick={() => setIsOn(!isOn)}
                                />
                            </div>
                        </div>
                        <div className="mb-4 flex w-full max-w-2xl justify-end">
                            <button
                                type="submit"
                                className="inline-flex justify-center rounded-md bg-[#1C4699] px-6 py-4 text-sm font-medium text-white md:text-xl"
                            >
                                Suivant
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default FormStep1;
