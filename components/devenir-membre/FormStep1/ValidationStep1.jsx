import * as Yup from "yup";

export const ValidationStep1 = Yup.object().shape({
    civility: 
        Yup.string()
        .required("Champs obligatoire"),
    email:  
        Yup.string()
        .email('Format invalide')
        .required("Champs obligatoire"),
    lastName:
        Yup.string()
        .min(1, "Minimum 1 charactères")
        .max(20, "Max 20 charactère")
        .required("Champs obligatoire"),
    firstName: 
        Yup.string()
        .min(2, "Minimum 2 charactères")
        .max(20, "Max 20 charactère")
        .required("Champs obligatoire"),
    birthday: 
        Yup.date()
        .required("Champs obligatoire")
        .max(new Date(Date.now() - 567648000000), "Vous devez avoir minimum 18 ans"),
    cityOfBirth:  
        Yup.string()
        .min(2,  "Minimum 2 charactères")
        .required("Champs obligatoire"),
    countryOfBirth: 
        Yup.string()
        .required("Champs obligatoire"),
    nationality:  
        Yup.string()
        .required("Champs obligatoire"),
    mobile: 
        Yup.string().required("Champs obligatoire").min(10, "10 chiffres minimun"),
    profession: 
        Yup.string()
        .min(2,  "Minimum 2 charactères")
        .required("Champs obligatoire"),
    socioProfessionalCategory: 
        Yup.string()
        .required("Champs obligatoire"),
    fiscalAdress: 
        Yup.string()
        .required("Champs obligatoire"),
    familyStatus: 
        Yup.string()
        .required("Champs obligatoire"),
    matrimonialRegime:
        Yup.string()
        .when("familyStatus", {
            is: value => value && value === "Marié",
            then: Yup.string().required("Champs obligatoire"),
            otherwise: Yup.string()
        }),
    numberOfChildrens: 
        Yup.number()
        .required("number of kids required"),
    investorProfessional: 
        Yup.bool()
        .required("is required"),
    usPerson: 
        Yup.bool()
        .required("us person required"),
    politicallyExposedPerson: 
        Yup.bool()
        .required("politically exposed person required"),
})
