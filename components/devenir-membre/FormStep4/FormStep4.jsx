import {useState} from "react"
import {
    Formik,
    Form,
    FieldArray,
} from "formik";
import Input from "../../FormFields/Input";
import Select from "../../FormFields/Select";
import { objectLoan, typeLoan } from "../optionsData";
import { ArrayValidation } from "./ArrayValidation";
import { addField, updatedUser, generateO2s } from "../../../pages/api";
import Chart from "../../Chart";
import FormStep5 from "../FormStep5/FormStep5";

const FormStep4 = ({setStatus, ScrollTop, setLoader}) => {

    const id = localStorage.getItem('user')
    const [fields, setFields] = useState({
        valeur_epargne_disponibilite_1: "",
        valeur_bien_usage_1: "",
        valeur_immobilier_1: "",
        valeur_valeur_mobiliere_1: "",
        valeur_assurance_vie_1: "",
        valeur_placement_foncier_1: ""
    });
    const [errors, setErrors] = useState(false);

    const [fieldProject, setFieldProject] = useState({
        objectif_1: "",
        objectif_2: "",
        objectif_3: "",
        objectif_4: "",
        objectif_5: "",
        objectif_6: "",
    })

    const [errorsField, setErrorField] = useState(false);


    const checkFields = () => {
        if (
            fields.valeur_epargne_disponibilite_1 ||
            fields.valeur_bien_usage_1 ||
            fields.valeur_immobilier_1 ||
            fields.valeur_valeur_mobiliere_1 ||
            fields.valeur_assurance_vie_1 ||
            fields.valeur_placement_foncier_1
        ) {
            setErrors(false);
        } else {
            setErrors(true);
        }
    }

    const checkFieldsProject = () => {
        if (
            fieldProject.objectif_1 ||
            fieldProject.objectif_2 ||
            fieldProject.objectif_3 ||
            fieldProject.objectif_4 ||
            fieldProject.objectif_5 ||
            fieldProject.objectif_6
        ) {
            setErrorField(false);
        } else {
            setErrorField(true);
        }
    }
    
    const data = {
        labels: [],
        datasets: [
            {
                label: "# of Votes",
                data: [
                    fields?.valeur_epargne_disponibilite_1 ? fields.valeur_epargne_disponibilite_1 : 1,
                    fields?.valeur_bien_usage_1 ? fields.valeur_bien_usage_1 : 1,
                    fields?.valeur_immobilier_1 ? fields.valeur_immobilier_1 : 1,
                    fields?.valeur_valeur_mobiliere_1 ? fields.valeur_valeur_mobiliere_1 : 1,
                    fields?.valeur_assurance_vie_1 ? fields.valeur_assurance_vie_1 : 1,
                    fields?.valeur_placement_foncier_1 ? fields.valeur_placement_foncier_1 : 1,
                ],
                backgroundColor: [
                    "#2A2A28",
                    "#071F4F",
                    "#1C4699",
                    "#2DABF1",
                    "#C29771",
                    "#E6E6E8",
                ],
            },
        ],
    };

    const handleChange = (e) => {
        setFields({ ...fields, [e.target.name]: e.target.value });
    };

    const initialValue = {
        revenues: [
            {
                id_credit_: "",
                nature_credit_: "Emprunt sur résidence principale",
                type_echeance_credit_: "Échéances constantes",
                date_souscription_credit_: "",
                capital_emprunte_credit_: "",
                duree_credit_: "",
                taux_interet_credit_: "",
            },
        ]
    };

    const newValue = {
        id_credit_: "",
        nature_credit_: "Emprunt immobilier",
        type_echeance_credit_: "Échéances constantes",
        date_souscription_credit_: "",
        capital_emprunte_credit_: "",
        duree_credit_: "",
        taux_interet_credit_: "",
    }

    const addFinalObject = async (loans) => {
        const array = []
        loans && loans.map((loan, index) => {
            for(const [key, value] of Object.entries(loan)) {
                if(key === `nature_credit_` || key === `type_echeance_credit_`) {
                    array.push({
                        type: "dropdown",
                        key: `${key}${index + 1}`,
                        answer: value,
                        UserId: id 
                    })
                } else if (key === `date_souscription_credit_`) {
                    const date = new Date(value)
                    value = date.toLocaleDateString('fr-Fr')
                    array.push({
                        type: "date",
                        key: `${key}${index + 1}`,
                        answer: value,
                        UserId: id 
                    })
                } else {
                    array.push({
                        type: "text",
                        key: `${key}${index + 1}`,
                        answer: value,
                        UserId: id 
                    })
                }
            }
        })
        for(const [key, value] of Object.entries(fields)) {
            array.push({
                type: "text",
                key: `${key}`,
                answer: value,
                UserId: id 
            })
        }
        for (const [key, value] of Object.entries(fieldProject)) {
            if (value) {
                array.push({
                    type: "dropdown",
                    key: key,
                    answer: value,
                    UserId: id,
                });
            }
        }
        return array
    }


    return (
        <>
            <div className="flex flex-col items-center gap-5 rounded-lg bg-[#071f4f6a] p-5 md:p-10 w-full">
                <div className="mb-5 w-full border-b-[1px] border-b-white xl:col-span-12 ">
                    <h1 className="mb-5 text-center text-xl uppercase text-white sm:text-left md:text-2xl">Revenu passif</h1>
                </div>
                <div className="flex flex-col items-center justify-center gap-5 lg:flex-row">
                    <div className="col-span-12 flex justify-center">
                        <Chart data={data} />
                    </div>
                    <div className="grid  grid-cols-12 gap-4 rounded-lg p-2">
                        <div className="col-span-12 lg:col-span-6">
                            <label
                                htmlFor="valeur_epargne_disponibilite_1"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Liquidités (comptes courants etc)
                            </label>
                            <Input
                                name="valeur_epargne_disponibilite_1"
                                type="number"
                                placeholder="3200"
                                change={handleChange}
                                className="rounded"
                                value={fields?.valeur_epargne_disponibilite_1}
                            />
                        </div>
                        <div className="col-span-12 lg:col-span-6">
                            <label
                                htmlFor="valeur_bien_usage_1"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Biens d’usage (RP, résidence secondaire etc){" "}
                            </label>
                            <Input
                                name="valeur_bien_usage_1"
                                type="number"
                                placeholder="3200"
                                change={handleChange}
                                value={fields?.valeur_bien_usage_1}
                            />
                        </div>
                        <div className="col-span-12 lg:col-span-6">
                            <label
                                htmlFor="valeur_immobilier_1"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Immobilier locatif & immobilier indirect (SCPI, OPCI
                                etc){" "}
                            </label>
                            <Input
                                name="valeur_immobilier_1"
                                type="number"
                                placeholder="3200"
                                change={handleChange}
                                value={fields?.valeur_immobilier_1}
                            />
                        </div>
                        <div className="col-span-12 lg:col-span-6">
                            <label
                                htmlFor="valeur_valeur_mobiliere_1"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Epargne financière (compte titre / PEA / PEE etc)
                            </label>
                            <Input
                                name="valeur_valeur_mobiliere_1"
                                type="number"
                                placeholder="3200"
                                change={handleChange}
                                value={fields?.valeur_valeur_mobiliere_1}
                            />
                        </div>
                        <div className="col-span-12 lg:col-span-6">
                            <label
                                htmlFor="valeur_assurance_vie_1"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Assurance vie
                            </label>
                            <Input
                                name="valeur_assurance_vie_1"
                                type="number"
                                placeholder="3200"
                                change={handleChange}
                                value={fields?.valeur_assurance_vie_1}
                            />
                        </div>
                        <div className="col-span-12 lg:col-span-6">
                            <label
                                htmlFor="valeur_placement_foncier_1"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Autre investissement (Or, crypto etc)
                            </label>
                            <Input
                                name="valeur_placement_foncier_1"
                                type="number"
                                placeholder="3200"
                                change={handleChange}
                                value={fields?.valeur_placement_foncier_1}
                            />
                        </div>
                    </div>
                </div>
                {
                    errors && <p className="text-red-500"> Vous devez entrer au moins un des champs ci dessus</p>
                }
            </div>

            <Formik
                initialValues={initialValue}
                validationSchema={ArrayValidation}
                onSubmit={async (values) => {
                    const array = await addFinalObject(values.revenues)
                    
                    if(!errors && !errorsField && array && array.length > 0) {
                        const newFields = await addField(id, array)
                        if(newFields === 200) {
                            ScrollTop();
                            const user = await updatedUser(id, {status: 5})
                            if (user.status === 200) {
                                ScrollTop();
                                setLoader(true);
                                const newPdf = await generateO2s(id);
                                if (newPdf.status === 200) {
                                    setLoader(false);
                                    setStatus(4);
                                    ScrollTop();
                                }
                            }
                        }
                    } else {
                        ScrollTop();
                    }
                }}
            >
                {({values, errors, handleChange, handleBlur, touched, setFieldValue, isValid, handleSubmit}) => (
                    <Form autoComplete="off" >
                        <FieldArray name="revenues">
                            {({push, remove}) => (
                                <>
                                    {
                                        values.revenues.length > 0 &&
                                        values.revenues.map((_, index) => {
                                            return (
                                                <div key={index} className="grid grid-cols-6 xl:grid-cols-12 gap-4 bg-[#071f4f6a] rounded-lg mt-10 p-5 md:p-10" >
                                                    <div className="col-span-6 xl:col-span-12 mb-5 border-b-[1px] border-b-white ">
                                                        <h1 className="mb-5 text-center text-xl uppercase text-white sm:text-left md:text-2xl">
                                                            Crédit numéro {index + 1}
                                                        </h1>
                                                    </div>
                                                    <div className="col-span-6 sm:col-span-3 xl:col-span-3">
                                                        <label  name={`revenues.${index}.nature_credit_`} className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base">
                                                            Objet du prêt
                                                        </label>
                                                        <Select
                                                            name={`revenues.${index}.nature_credit_`}
                                                            data={objectLoan}
                                                            handleChange={setFieldValue}
                                                            value={values.revenues[index].nature_credit_}
                                                            errors={errors.revenues && errors.revenues[index] ? errors.revenues[index].nature_credit_ : null}
                                                            touched={touched.revenues && touched.revenues[index] ? touched.revenues[index].nature_credit_ : null}
                                                        />
                                                    </div>
                                                    <div className="col-span-6 sm:col-span-3 xl:col-span-3">
                                                        <label  name={`revenues.${index}.type_echeance_credit_`}  className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base">
                                                            Type de prêt
                                                        </label>
                                                        <Select
                                                            name={`revenues.${index}.type_echeance_credit_`}
                                                            data={typeLoan}
                                                            handleChange={setFieldValue}
                                                            value={values.revenues[index].type_echeance_credit_}
                                                            errors={errors.revenues && errors.revenues[index] ? errors.revenues[index].type_echeance_credit_ : null}
                                                            touched={touched.revenues && touched.revenues[index] ? touched.revenues[index].type_echeance_credit_ : null}
                                                        />
                                                    </div>
                                                    <div className="col-span-6 sm:col-span-3 xl:col-span-2">
                                                        <label  name={`revenues.${index}.date_souscription_credit_`}  className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base">
                                                            Date de souscription
                                                        </label>
                                                        <Input
                                                            name={`revenues.${index}.date_souscription_credit_`}
                                                            type="date"
                                                            change={handleChange}
                                                            value={values.revenues[index].date_souscription_credit_}
                                                            errors={errors.revenues && errors.revenues[index] ? errors.revenues[index].date_souscription_credit_ : null}
                                                            touched={touched.revenues && touched.revenues[index] ? touched.revenues[index].date_souscription_credit_ : null}
                                                            blur={handleBlur}
                                                        />
                                                    </div>
                                                    <div className="col-span-6  sm:col-span-2 xl:col-span-2">
                                                        <label  name={`revenues.${index}.capital_emprunte_credit_`}  className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base">
                                                            Montant emprunté
                                                        </label>
                                                        <Input
                                                            name={`revenues.${index}.capital_emprunte_credit_`}
                                                            type="number"
                                                            placeholder="Ex : 25 000"
                                                            change={handleChange}
                                                            value={values.revenues[index].capital_emprunte_credit_}
                                                            errors={errors.revenues && errors.revenues[index] ? errors.revenues[index].capital_emprunte_credit_ : null}
                                                            touched={touched.revenues && touched.revenues[index] ? touched.revenues[index].capital_emprunte_credit_ : null}
                                                            blur={handleBlur}
                                                        />
                                                    </div>
                                                    <div className="col-span-6  sm:col-span-2 xl:col-span-1">
                                                        <label  name={`revenues.${index}.duree_credit_`} className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base">
                                                            Durée
                                                        </label>
                                                        <Input
                                                            name={`revenues.${index}.duree_credit_`}
                                                            type="number"
                                                            placeholder="mois"
                                                            change={handleChange}
                                                            value={values.revenues[index].duree_credit_}
                                                            errors={errors.revenues && errors.revenues[index] ? errors.revenues[index].duree_credit_ : null}
                                                            touched={touched.revenues && touched.revenues[index] ? touched.revenues[index].duree_credit_ : null}
                                                            blur={handleBlur}
                                                        />
                                                    </div>
                                                    <div className="col-span-6  sm:col-span-2 xl:col-span-1">
                                                        <label  name={`revenues.${index}.taux_interet_credit_`} className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base">
                                                            Taux
                                                        </label>
                                                        <Input
                                                            name={`revenues.${index}.taux_interet_credit_`}
                                                            type="number"
                                                            placeholder="en %"
                                                            change={handleChange}
                                                            value={values.revenues[index].taux_interet_credit_}
                                                            errors={errors.revenues && errors.revenues[index] ? errors.revenues[index].taux_interet_credit_ : null}
                                                            touched={touched.revenues && touched.revenues[index] ? touched.revenues[index].taux_interet_credit_ : null}
                                                            blur={handleBlur}
                                                        />
                                                    </div>
                                                    
                                                    <div className="col-span-6 space-x-4" >
                                                        {
                                                            index === values.revenues.length - 1 ? (
                                                                <button 
                                                                    disabled={!isValid || !values.revenues[0].taux_interet_credit_ ? true : false }
                                                                    className={`
                                                                        bg-green-700 text-white rounded-lg p-3
                                                                        ${!isValid || !values.revenues[0].taux_interet_credit_ ? "bg-gray-400 cursor-not-allowed" : ""}                                                            
                                                                    `} 
                                                                    onClick={() => {push(newValue)}}
                                                                >
                                                                    Ajouter un prêt
                                                                </button>
                                                            ) : null
                                                        }
                                                        {
                                                            values.revenues.length > 1 ? (
                                                                <button 
                                                                className="bg-red-700 text-white rounded-lg p-3"
                                                                onClick={() => {remove(index)}}
                                                            >
                                                                Supprimer le prêt
                                                            </button>
                                                            ) : null
                                                        }
                                                    </div>
                                                </div>
                                            )
                                        }) 
                                        
                                    }
                                </>
                            )}
                        </FieldArray>
                                
                        <FormStep5 values={fieldProject} setValues={setFieldProject} errors={errorsField}/>
                        

                        <div className="w-full flex justify-end mt-10">
                            <button type="submit" onClick={() => handleSubmit(checkFields(), checkFieldsProject())} className="inline-flex justify-center rounded-md bg-[#1C4699] px-6 py-4 text-sm font-medium text-white md:text-xl">
                                Suivant
                            </button>
                        </div>
                    </Form>
                )}
            </Formik>
        </>
    );
};

export default FormStep4;
