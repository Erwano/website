import { string, object, array, number, date } from "yup";

export const ArrayValidation = object().shape({
    revenues: array().of(
        object().shape({
            nature_credit_: 
                string(),
            type_echeance_credit_: 
                string(),
            date_souscription_credit_: 
                date()
                .max(new Date(), "la date de souscription ne peut pas être supérieur à celle du jour"),
            capital_emprunte_credit_: number(),
            duree_credit_: number(),
            taux_interet_credit_: number()
                .min(0, "Ne peut pas être inférieur à 0")
                .max(100, "Ne peut pas être supérieur à 100"),
        })
    ),
});
