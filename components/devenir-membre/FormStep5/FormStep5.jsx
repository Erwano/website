import { fields } from "../optionsData";

const FormStep5 = (props) => {
    const { values, setValues, errors } = props;

    const handleClick = (name, value) => {
        if (values[name]) {
            setValues({ ...values, [name]: "" });
        } else {
            setValues({ ...values, [name]: value });
        }
    };

    return (
        <div className="mt-10 grid w-full grid-cols-2 gap-6 rounded-lg bg-[#071f4f6a] p-5 text-white md:p-10 lg:grid-rows-3">
            <div className="col-span-2 mb-5 border-b-[1px] border-b-white ">
                <h1 className="mb-5 text-center text-xl uppercase text-white sm:text-left md:text-2xl">
                    Quel est votre projet d’investissement ?
                </h1>
            </div>
            {fields.map((field, i) => (
                <div
                    key={i}
                    className={`col-span-2 flex items-center justify-center text-center text-xs sm:text-sm md:col-span-1`}
                    name={field.name}
                    onClick={() => {
                        handleClick(field.name, field.label);
                    }}
                >
                    <span
                        className={`hover:cursor-pointer border-white w-full max-w-[400px] rounded-xl h-11 flex justify-center items-center ${
                            values[field.name]
                            ? "bg-[#C29771] p-2"
                            : "border p-2"
                        }
                        `}
                    >
                        {field.label}
                    </span>
                </div>
            ))}
            {errors && (
                <p className="text-red-500 col-span-2 text-center">
                    {" "}
                    Vous devez entrer au moins un des champs ci dessus
                </p>
            )}
        </div>
    );
};

export default FormStep5;
