import { useState, useEffect } from "react";
import Chart from "../../Chart";
import Input from "../../FormFields/Input";
import { addField, updatedUser } from "../../../pages/api/index";

const FormStep2 = ({ setStatus, ScrollTop }) => {
    const id = localStorage.getItem("user");
    const [values, setValues] = useState({
        salary: "",
        otherActivity: "",
        revenuFoncier: "",
        revenuMobilier: "",
        revenuLMNP: "",
        otherBecome: "",
    });

    const [valuesCharge, setValuesCharge] = useState({
        loyer: "",
        chargeActif: "",
        ir: "",
        otherCharge: "",
        ifi: "",
        capacity: "",
    });

    const data = {
        labels: [],
        datasets: [
            {
                label: "# of Votes",
                data: [
                    values.salary ? values.salary : 1,
                    values.otherActivity ? values.salotherActivityary : 1,
                    values.revenuFoncier ? values.revenuFoncier : 1,
                    values.revenuMobilier ? values.revenuMobilier : 1,
                    values.revenuLMNP ? values.revenuLMNP : 1,
                    values.otherBecome ? values.otherBecome : 1,
                ],
                backgroundColor: [
                    "#2A2A28",
                    "#071F4F",
                    "#1C4699",
                    "#2DABF1",
                    "#C29771",
                    "#E6E6E8",
                ],
            },
        ],
    };

    const dataCharge = {
        labels: [],
        datasets: [
            {
                label: "# of Votes",
                data: [
                    valuesCharge.loyer ? valuesCharge.loyer : 1,
                    valuesCharge.chargeActif ? valuesCharge.chargeActif : 1,
                    valuesCharge.ir ? valuesCharge.ir : 1,
                    valuesCharge.otherCharge ? valuesCharge.otherCharge : 1,
                    valuesCharge.ifi ? valuesCharge.ifi : 1,
                ],
                backgroundColor: [
                    "#2A2A28",
                    "#071F4F",
                    "#1C4699",
                    "#2DABF1",
                    "#C29771",
                    "#E6E6E8",
                ],
            },
        ],
    };

    const handleChangeRevenu = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
    };

    const handleChangeCharge = (e) => {
        setValuesCharge({ ...valuesCharge, [e.target.name]: e.target.value });
    };

    const finalObject = [
        {
            type: "text",
            key: "montant_revenu_1",
            answer: values.salary,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_revenu_2",
            answer: values.revenuFoncier,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_revenu_3",
            answer: values.revenuLMNP,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_revenu_4",
            answer: values.otherActivity,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_revenu_5",
            answer: values.revenuMobilier,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_revenu_6",
            answer: values.otherBecome,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_charge_1",
            answer: valuesCharge.loyer,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_charge_2",
            answer: valuesCharge.ir,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_charge_3",
            answer: valuesCharge.ifi,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_charge_4",
            answer: valuesCharge.chargeActif,
            UserId: id,
        },
        {
            type: "text",
            key: "montant_charge_5",
            answer: valuesCharge.otherCharge,
            UserId: id,
        },
        {
            type: "text",
            key: "effort_epargne",
            answer: valuesCharge.capacity,
            UserId: id,
        },
    ];

    const [capacityError, setError] = useState(false);

    const handleSubmit = async () => {
        if (valuesCharge.capacity) {
            const newFields = await addField(id, finalObject);
            if (newFields === 200) {
                const user = await updatedUser(id, { status: 3 });
                localStorage.setItem("status", 3);
                setStatus(3);
                ScrollTop();
            }
        } else {
            setError(true);
        }
    };

    return (
        <div className="page-section hero flex flex-col items-center gap-5">
            <div className="w-full rounded-lg bg-[#071f4f6a] p-2 md:p-10">
                <div className="mb-5 w-full border-b-[1px] border-b-white ">
                    <h1 className="mb-5 text-center text-xl text-white sm:text-left md:text-2xl">
                        REVENUS  <span className="text-sm">(Annuels)</span>
                    </h1>
                </div>
                <div className="flex w-full flex-col items-center justify-center gap-5 lg:flex-row">
                    <div className="col-span-12 flex justify-center">
                        <Chart data={data} />
                    </div>
                    <div className="flex-1  w-full grid grid-cols-12 gap-4 rounded-lg p-2">
                        <div className="col-span-6 ">
                            <label
                                htmlFor="salary"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Salaire (Brut)
                            </label>
                            <Input
                                name="salary"
                                type="number"
                                placeholder="3200"
                                change={handleChangeRevenu}
                                className="rounded"
                                value={values.salary}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="otherActivity"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                {/* Autre revenu d’activité (BIC/BNC) */}
                                Revenus d’activités (BIC/BNC)
                            </label>
                            <Input
                                name="otherActivity"
                                type="number"
                                placeholder="3200"
                                change={handleChangeRevenu}
                                value={values.otherActivity}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="revenuFoncier"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Revenus fonciers
                            </label>
                            <Input
                                name="revenuFoncier"
                                type="number"
                                placeholder="3200"
                                change={handleChangeRevenu}
                                value={values.revenuFoncier}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="revenuMobilier"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Revenus mobiliers
                            </label>
                            <Input
                                name="revenuMobilier"
                                type="number"
                                placeholder="3200"
                                change={handleChangeRevenu}
                                value={values.revenuMobilier}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="revenuLMNP"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Revenus LMNP
                            </label>
                            <Input
                                name="revenuLMNP"
                                type="number"
                                placeholder="3200"
                                change={handleChangeRevenu}
                                value={values.revenuLMNP}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="otherBecome"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Autres revenus
                            </label>
                            <Input
                                name="otherBecome"
                                type="number"
                                placeholder="3200"
                                change={handleChangeRevenu}
                                value={values.otherBecome}
                            />
                        </div>
                    </div>
                </div>
            </div>

            {/* 
				CHARGES
			*/}

            <div className="w-full rounded-lg bg-[#071f4f6a] p-2 md:p-10">
                <div className="mb-5 w-full border-b-[1px] border-b-white ">
                    <h1 className="mb-5 text-center text-xl text-white sm:text-left md:text-2xl">
                        CHARGES <span className="text-sm">(Annuelles)</span>
                    </h1>
                </div>
                <div className="flex w-full flex-col items-center justify-center gap-5 lg:flex-row">
                    <div className="col-span-12 flex justify-center">
                        <Chart data={dataCharge} />
                    </div>
                    <div className="flex-1 w-full grid  grid-cols-12 gap-4 rounded-lg p-2">
                        <div className="col-span-6 ">
                            <label
                                htmlFor="loyer"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Loyer(s)
                            </label>
                            <Input
                                name="loyer"
                                type="number"
                                placeholder="3200"
                                change={handleChangeCharge}
                                value={valuesCharge.loyer}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="chargeActif"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Charges liés aux actifs
                            </label>
                            <Input
                                name="chargeActif"
                                type="number"
                                placeholder="3200"
                                change={handleChangeCharge}
                                value={valuesCharge.chargeActif}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="ir"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                IR
                            </label>
                            <Input
                                name="ir"
                                type="number"
                                placeholder="3200"
                                change={handleChangeCharge}
                                value={valuesCharge.ir}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="otherCharge"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Autre(s) charge(s)
                            </label>
                            <Input
                                name="otherCharge"
                                type="number"
                                placeholder="3200"
                                change={handleChangeCharge}
                                value={valuesCharge.otherCharge}
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="ifi"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                IFI
                            </label>
                            <Input
                                name="ifi"
                                type="number"
                                placeholder="3200"
                                change={handleChangeCharge}
                                value={valuesCharge.ifi}
                            />
                        </div>
                        <div className="col-span-12 lg:col-span-6">
                            <label
                                htmlFor="capacity"
                                className="block text-xs font-medium text-gray-400 sm:text-sm md:text-base"
                            >
                                Quelle est votre capactié d’épargne ?
                                (mensuelle)
                            </label>
                            <Input
                                type="number"
                                name="capacity"
                                change={handleChangeCharge}
                                value={valuesCharge.capacity}
                            />
                            {capacityError ? (
                                <p className="text-red-500">
                                    Ce champs doit être rempli
                                </p>
                            ) : null}
                        </div>
                    </div>
                </div>
            </div>

            <div className="mb-4 flex w-full max-w-2xl flex-shrink-0 justify-end">
                <button
                    type="submit"
                    className="inline-flex justify-center rounded-md bg-[#1C4699] px-6 py-4 text-sm font-medium text-white md:text-xl"
                    onClick={() => handleSubmit()}
                >
                    Suivant
                </button>
            </div>
        </div>
    );
};

export default FormStep2;
