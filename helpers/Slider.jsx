import { useKeenSlider } from "keen-slider/react";
import { useState } from "react";
import CardArrow from "../components/Marches-Financier/CardArrow";



const Slider = (props) => {

    const {cardDatas, Component, loopTime} = props
    const [currentSlide, setCurrentSlide] = useState(1);
    const [loaded, setLoaded] = useState(false);

    const autoLoop = (slider) => {
        let timeout;
        let mouseOver = false;
        function clearNextTimeout() {
            clearTimeout(timeout);
        }
        function nextTimeout() {
            clearTimeout(timeout);
            if (mouseOver) return;
            timeout = setTimeout(() => {
                slider.next();
            }, loopTime ? loopTime : 4000);
        }
        slider.on("created", () => {
            slider.container.addEventListener("mouseover", () => {
                mouseOver = true;
                clearNextTimeout();
            });
            slider.container.addEventListener("mouseout", () => {
                mouseOver = false;
                nextTimeout();
            });
            nextTimeout();
        });
        slider.on("dragStarted", clearNextTimeout);
        slider.on("animationEnded", nextTimeout);
        slider.on("updated", nextTimeout);
    };
    
    const [sliderRef, instanceRef] = useKeenSlider({
        mode: "free-snap",
        slides: {
            origin: "center",
            perView: 1.5,
            spacing: 12,
            size: 0.9
        },
        initial: 1,
        loop: true,
        breakpoints: {
            '(min-width: 768px)': {
                slides: {
                    origin: "center",
                    perView: 2,
                    spacing: 28,
                    size: 0.8
                },
            },
        },
        created() {
            setLoaded(true)
        },
        slideChanged(slider) {
            setCurrentSlide(slider.track.details.rel)
        },
    }, [autoLoop])

    return (
        <div className="relative mt-10 w-full">
            <div ref={sliderRef} className="keen-slider">
                {cardDatas.map((card, idx) => (
                    <Component {...card} key={idx} />
                ))}
            </div>
            {loaded && instanceRef.current && (
                <>
                    <CardArrow
                        left
                        onClick={(e) =>
                            e.stopPropagation() || instanceRef.current?.prev()
                        }
                    />

                    <CardArrow
                        onClick={(e) =>
                            e.stopPropagation() || instanceRef.current?.next()
                        }
                    />
                </>
            )}
        </div>
    );
};
export default Slider;
